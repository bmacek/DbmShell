cmake_minimum_required(VERSION 2.8)

project("DbmShell")

OPTION(COMPILE_SLINK_PLUGIN "Build Plugin for S-link communication." ON)
OPTION(COMPILE_BOCLIB_PLUGIN "Build Plugin using 'boclib' for BOC communication." ON)

if(EXISTS "$ENV{CATSHELL}")
        message("CatShell searched for in " $ENV{CATSHELL})
        include_directories($ENV{CATSHELL}/include)
        LINK_DIRECTORIES(${LINK_DIRECTORIES} $ENV{CATSHELL}/lib)
else()
endif(EXISTS "$ENV{CATSHELL}")
if(COMPILE_BOCLIB_PLUGIN)
		include_directories($ENV{BOCLIB_INST_PATH}/inc)
		message("Boclib searched for in " $ENV{BOCLIB_INST_PATH})
		LINK_DIRECTORIES(${LINK_DIRECTORIES} $ENV{BOCLIB_INST_PATH}/bin)
else()
endif(COMPILE_BOCLIB_PLUGIN)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread -std=c++11 -fPIC")

if(COMPILE_SLINK_PLUGIN)
	add_subdirectory(slink)
endif()
add_subdirectory(hardware)
add_subdirectory(lumi)
if(COMPILE_BOCLIB_PLUGIN)
	add_subdirectory(boclib)
endif()
add_subdirectory(simulation)
add_subdirectory(config)


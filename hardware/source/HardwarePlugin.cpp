#include "CatShell/core/Plugin.h"
#include "DbmShell/hardware/LumiAlgoLogic.h"
#include "DbmShell/hardware/LumiAlgoCreator.h"
#include "DbmShell/hardware/BocRegisterCommand.h"
#include "DbmShell/hardware/LumiAlgoConfigurator.h"
#include "DbmShell/hardware/RegisterToBocLib.h"
#include "DbmShell/hardware/PixCoord.h"
#include "DbmShell/hardware/PixCoordParser.h"
#include "DbmShell/hardware/FEI4Command.h"
#include "DbmShell/hardware/FEI4Configurator.h"
#include "DbmShell/hardware/FEI4ToBocRegister.h"
#include "DbmShell/hardware/FEI4Package.h"
#include "DbmShell/hardware/FEI4Message.h"
#include "DbmShell/hardware/FEI4Mask.h"
//#ifdef BOCLIB_COMPILE
#include "DbmShell/hardware/DbmConsole.h"
//#endif

using namespace std;

namespace DbmShell {

EXPORT_PLUGIN(CatShell::Plugin, DbmHardwarePlugin)
START_EXPORT_CLASSES(DbmHardwarePlugin)
EXPORT_CLASS(LumiAlgoLogic, CAT_NAME_LUMIALGOLOGIC, DbmHardwarePlugin)
EXPORT_CLASS(LumiAlgoCreator, CAT_NAME_LUMIALGOCREATOR, DbmHardwarePlugin)
EXPORT_CLASS(BocRegisterCommand, CAT_NAME_BOCREGISTERCOMMAND, DbmHardwarePlugin)
EXPORT_CLASS(LumiAlgoConfigurator, CAT_NAME_LUMIALGOCONFIGURATOR, DbmHardwarePlugin)
EXPORT_CLASS(RegisterToBocLib, CAT_NAME_REGISTERTOBOCLIB, DbmHardwarePlugin)
EXPORT_CLASS(PixCoord, CAT_NAME_PIXCOORD, DbmHardwarePlugin)
EXPORT_CLASS(PixCoordParser, CAT_NAME_PIXCOORDPARSER, DbmHardwarePlugin)
EXPORT_CLASS(FEI4Command, CAT_NAME_FEI4COMMAND, DbmHardwarePlugin)
EXPORT_CLASS(FEI4Configurator, CAT_NAME_FEI4CONFIGURATOR, DbmHardwarePlugin)
EXPORT_CLASS(FEI4ToBocRegister, CAT_NAME_FEI4TOBOCREGISTER, DbmHardwarePlugin)
EXPORT_CLASS(FEI4Package, CAT_NAME_FEI4PACKAGE, DbmHardwarePlugin)
EXPORT_CLASS(FEI4Message, CAT_NAME_FEI4MESSAGE, DbmHardwarePlugin)
EXPORT_CLASS(FEI4Mask, CAT_NAME_FEI4MASK, DbmHardwarePlugin)
//#ifdef BOCLIB_COMPILE
EXPORT_CLASS(DbmConsole, CAT_NAME_DBMCONSOLE, DbmHardwarePlugin)
//#endif
END_EXPORT_CLASSES(DbmHardwarePlugin)

} // namespace DbmShell

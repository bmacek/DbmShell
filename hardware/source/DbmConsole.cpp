#include "DbmShell/hardware/defines.h"
#include "DbmShell/hardware/DbmConsole.h"
#include "DbmShell/hardware/FEI4Package.h"
#include "DbmShell/hardware/FEI4Message.h"
#include "DbmShell/hardware/LumiAlgoLogic.h"

#include "CatShell/core/PluginDeamon.h"
#include "CatShell/core/CatService.h"

#include "Bcf.h"

#include <sstream>
#include <stdio.h>
#include <string.h>
#include <chrono>

using namespace std;
using namespace CatShell;
using namespace Boclib;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(DbmConsole, CAT_NAME_DBMCONSOLE, CAT_TYPE_DBMCONSOLE);

DbmConsole::DbmConsole()
{
}

DbmConsole::~DbmConsole()
{
}

void DbmConsole::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
	// processing input data
	if(object.isSomething() && port == _in_boc_read)
	{
		CatPointer<BocRegisterCommand> cmd = object.unsafeCast<BocRegisterCommand>();

		if(_reply == reply_t::read_register)
		{
			std::stringstream text;
			text << "Register read,";

			if(cmd->get_fpga() == BocFpga::North)
				text << " north fpga,";
			else if(cmd->get_fpga() == BocFpga::South)
				text << " south fpga,";
			else
			{
				ERROR(logger, "Unknown target FPGA. Skipping.");
				Algorithm::process_data(port, object, flush, logger);
				return;
			}

			if(cmd->get_bank() == BmfRegisterBank::General)
				text << " general register 0x";
			else if(cmd->get_bank() == BmfRegisterBank::Rx)
				text << " rx register 0x";
			else if(cmd->get_bank() == BmfRegisterBank::Tx)
				text << " tx register 0x";
			else if(cmd->get_bank() == BmfRegisterBank::Dbm)
				text << " dbm register 0x";
			else
			{
				ERROR(logger, "Error: unrecognised register bank. Skipping.");
				Algorithm::process_data(port, object, flush, logger);
				return;
			}

			text << hex << cmd->get_register() << dec << ", value 0x" << hex << (int)cmd->getData() << "." << endl;

			cout << text.str();			
		}
		else if(_reply == reply_t::laser_satus)
		{
			// no checking that the source of the data is correct
			if((Bcf::InterlockState)cmd->getData() == Bcf::Interlock_Nothing)
				cout << "BOC laser is ON." << endl;
			else
				cout << "BOC laser is OFF." << endl;
		}
		else if(_reply == reply_t::h_stream_enable)
		{
			// setting the H-stream flags
			std::uint8_t value;

			// get the current register value
			value = cmd->getData();

			if(_hold_h_on)
			{
				// turning it ON
				value |= (BOC_HSTREAM_ENABLE_0_FLAG << _hold_h_ch);
			}
			else
			{
				// turning it OFF
				value &= (~(BOC_HSTREAM_ENABLE_0_FLAG << _hold_h_ch));
			}

			// write the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Write, cmd->get_fpga(), BmfRegisterBank::Dbm, BOC_DBM_REGISTER_HSTREAM_CONTROL, value);

			// write the corrected register value
			publish_data(_out_boc_command, cmd1, logger);
		}
		else if(_reply == reply_t::h_stream_reset)
		{
			// setting the H-stream flags
			std::uint8_t value;

			// get the current register value
			value = cmd->getData();

			if(_hold_h_on)
			{
				// turning it ON
				value |= (BOC_HSTREAM_RESET_0_FLAG << _hold_h_ch);
			}
			else
			{
				// turning it OFF
				value &= (~(BOC_HSTREAM_RESET_0_FLAG << _hold_h_ch));
			}

			// write the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Write, cmd->get_fpga(), BmfRegisterBank::Dbm, BOC_DBM_REGISTER_HSTREAM_CONTROL, value);

			// write the corrected register value
			publish_data(_out_boc_command, cmd1, logger);
		}
		else if(_reply == reply_t::dbm_slink_reset)
		{
			// setting the H-stream flags
			std::uint8_t value;

			// get the current register value
			value = cmd->getData();

			if(_hold_h_on)
			{
				// turning it ON
				value |= (BOC_SLINK_0_FLAG << _hold_h_ch);
			}
			else
			{
				// turning it OFF
				value &= (~(BOC_SLINK_0_FLAG << _hold_h_ch));
			}

			// write the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Write, cmd->get_fpga(), BmfRegisterBank::Dbm, BOC_DBM_REGISTER_DBM_CONTROL, value);

			// write the corrected register value
			publish_data(_out_boc_command, cmd1, logger);
		}
		else if(_reply == reply_t::enable_h_rx)
		{
			// setting the H-stream flags
			std::uint8_t value;

			// get the current register value
			value = cmd->getData();

			if(_hold_h_on)
			{
				// turning it ON
				value |= (BOC_H_RX_ENABLE_0_FLAG << _hold_h_ch);
			}
			else
			{
				// turning it OFF
				value &= (~(BOC_H_RX_ENABLE_0_FLAG << _hold_h_ch));
			}

			// write the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Write, cmd->get_fpga(), BmfRegisterBank::Dbm, BOC_DBM_REGISTER_HSTREAM_CONTROL, value);

			// write the corrected register value
			publish_data(_out_boc_command, cmd1, logger);
		}
		else if(_reply == reply_t::enable_h_generator)
		{
			// setting the H-stream flags
			std::uint8_t value;

			// get the current register value
			value = cmd->getData();

			if(_hold_h_on)
			{
				// turning it ON
				value |= (BOC_H_GENERATOR_FLAG);
			}
			else
			{
				// turning it OFF
				value &= (~(BOC_H_GENERATOR_FLAG));
			}

			// write the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Write, cmd->get_fpga(), BmfRegisterBank::Dbm, BOC_DBM_REGISTER_HSTREAM_CONTROL, value);

			// write the corrected register value
			publish_data(_out_boc_command, cmd1, logger);
		}
		else if(_reply == reply_t::hb_pattern)
		{
			// setting the H-stream flags
			std::uint8_t value;

			// get the current register value
			value = cmd->getData();

			if(_hold_h_on)
			{
				// pattern should be A (0-bit)
				value &= (~(BOC_H_PATTERN_MASK << _hold_h_ch));
			}
			else
			{
				// pattern should be B (1-bit)
				value |= (BOC_H_PATTERN_MASK << _hold_h_ch);
			}

			// write the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Write, cmd->get_fpga(), BmfRegisterBank::Dbm, BOC_DBM_REGISTER_DBM_CONTROL, value);

			// write the corrected register value
			publish_data(_out_boc_command, cmd1, logger);
		}
		else if(_reply == reply_t::ibl_slink_enable)
		{
			// setting the slink flags
			std::uint8_t value;

			// get the current register value
			value = cmd->getData();

			if(_hold_h_on)
			{
				// turning it ON -> drop reset
				value &= (~BOC_IBL_SLINK_ENABLE_FLAG);
			}
			else
			{
				// turning it OFF -> rise reset
				value |= BOC_IBL_SLINK_ENABLE_FLAG;
			}

			// write the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Write, cmd->get_fpga(), BmfRegisterBank::General, _hold_h_ch, value);

			// write the corrected register value
			publish_data(_out_boc_command, cmd1, logger);
		}
		else
		{
			cout << "Too much info!" << endl;
		}
	}
	else if(object.isSomething() && (port >= _in_fe_read) && (port < _in_fe_read + FEI4_PER_BOC))
	{
		if(_reply == reply_t::read_fe_register)
		{
			std::uint8_t channel = (std::uint8_t)(port-_in_fe_read);

			CatPointer<FEI4Package> pkg = object.unsafeCast<FEI4Package>();

			if(pkg->get_package_type() == FEI4Package::type::reg_read)
			{
				cout << "FE on channel " << dec << (int)_hold_channel << " register " << pkg->rr_get_address() << " read , value 0x" << hex << pkg->rr_get_value() << "." << endl;
			}
			else
			{
				ERROR(logger, "Error: unrecognised package type as a response from FE.");
			}
		}
		else
		{
			cout << "Too much info!" << endl;
		}
	}

	Algorithm::process_data(port, object, flush, logger);
}

void DbmConsole::algorithm_init()
{
	Process::algorithm_init();

	_reply = reply_t::none;

	// get the memory pool
    _pool_boc_command = PluginDeamon::get_pool<BocRegisterCommand>();
    _pool_fe_command = PluginDeamon::get_pool<FEI4Message>();
    _pool_fe_mask = PluginDeamon::get_pool<FEI4Mask>();

    // declare the input ports
    _in_boc_read = declare_input_port("in_read", CAT_NAME_BOCREGISTERCOMMAND);
    _in_fe_read = declare_input_port("in_fe_read", CAT_NAME_FEI4PACKAGE, FEI4_PER_BOC);

	// define the output port
	_out_boc_command = declare_output_port("out_boc_cmd", CAT_NAME_BOCREGISTERCOMMAND);
	_out_port_fe = declare_output_port("out_fe", CAT_NAME_FEI4MESSAGE);
	_out_port_fe_broadcast = declare_output_port("out_fe_broadcast", CAT_NAME_FEI4COMMAND);
	
	// check if in silent mode
	_silent = algo_settings().get_setting<bool>("silent", false);

	// create FE handlers
	char name[100];
	std::string tmp;
	for(std::uint8_t i; i<FEI4_PER_BOC; ++i)
	{
		_fe[i] = new FEI4;
		// configure
		sprintf(name, "links/link_%u/fei4", i);
		if(algo_settings().has_element(name))
			_fe[i]->load_configuration(algo_settings()[name]);
		else
			_fe[i]->default_configuration();
		// mapping
		_mapping[i].fpga = BocFpga::Bcf;
		_mapping[i].tx = 0;
		_mapping[i].rx = 0;

		sprintf(name, "links/link_%u/mapping/fpga", i);
		tmp = algo_settings().get_setting<std::string>(name, "");
		if(tmp == "north")
			_mapping[i].fpga = BocFpga::North;
		else if(tmp == "south")
			_mapping[i].fpga = BocFpga::South;
		else
			_mapping[i].fpga = BocFpga::Bcf;
		sprintf(name, "links/link_%u/mapping/tx", i);
		_mapping[i].tx = algo_settings().get_setting<std::uint8_t>(name, 0);
		sprintf(name, "links/link_%u/mapping/rx", i);
		_mapping[i].rx = algo_settings().get_setting<std::uint8_t>(name, 0);
	}
}

void DbmConsole::algorithm_deinit()
{
	// destroy the FE handlers
	// create FE handlers
	for(std::uint8_t i=0; i<FEI4_PER_BOC; ++i)
	{
		if(_fe[i])
		{
			delete _fe[i];
			_fe[i] = nullptr;
		}
	}

	Process::algorithm_deinit();
}

void DbmConsole::work_main()
{
}

void DbmConsole::display_help()
{
	cout << endl
	     << "? Avalilable commands are listed below. Letters in brackets are single letter abbreviations." << endl
	     << "? " << endl
	     << "? (q)uit  .............................................................................. exit the program" << endl
	     << "? (h)help .............................................................................. display this help" << endl
	     << "?" << endl
	     << "? boc (r)ead <(b)cf/(n)orth/(s)outh> <(g)eneral/(r)x/(t)x/(d)bm> [register] ............ read BOC register" << endl
	     << "? boc (w)rite <(b)cf/(n)orth/(s)outh> <(g)eneral/(r)x/(t)x/(d)bm> [register] [value] ... write BOC register" << endl
	     << "? boc (l)aser <on/off/status> .......................................................... control BOC laser" << endl
	     << "? " << endl
	     << "? fe (w)rite [channel] [register] [value] {b} .......................................... write FE register" << endl
	     << "? fe (r)ead [channel] [register] ....................................................... read FE register" << endl
	     << "? fe (m)ode [channel] <config/run> {b} ................................................. change FE run mode" << endl
	     << "? fe re(s)et [channel] {b} ............................................................. reset FE" << endl
	     << "? fe (p)ulse [channel] [length] {b} .................................................... produce global pulse on FE" << endl
	     << "? fe (h)itor [channel] [file] {b} ...................................................... set hitor mask" << endl
	     << "? fe (e)nable [channel] [file] {b} ..................................................... set enable mask" << endl
	     << "? fe (d)igital [channel] [file] [length] {b} ........................................... produces digital pulse" << endl
	     << "? fe (f)rontend [channel] [pattern] {b} ................................................ write shift-register" << endl
	     << "? " << endl
	     << "? hb (w)rite <(n)orth/(s)outh> [channel] [register] [value] ............................ write hitbus register" << endl
	     << "? hb (r)eceive <(n)orth/(s)outh> <on/off> [channel] .................................... turns on/off Hitbus rx" << endl
	     << "? hb (g)enerator <(n)orth/(s)outh> <on/off> ............................................ turns on/off fixed data" << endl
	     << "? hb (p)attern <(n)orth/(s)outh> <A/B> [channel] ....................................... select the alignment pattern" << endl
	     << "? " << endl
	     << "? lumi (s)link <(n)orth/(s)outh> <on/off> [stream #] ................................... turns on/off the lumi S-link" << endl
	     << "? lumi (h)stream <(n)orth/(s)outh> <on/off> [stream #] ................................. turns on/off the H-stream" << endl
	     << "? lumi h(r)eset <(n)orth/(s)outh> <on/off> [stream #] .................................. sets reset on/off for H-stream" << endl	     
	     << "? lumi (m)ap <(n)orth/(s)outh> [file] .................................................. configures lumi maps" << endl		
	     << "? lumi (i)nterval <(n)orth/(s)outh> [length] ........................................... length of lumi-block" << endl		
	     << "? " << endl
	     << "? ibl (s)link <(n)orth/(s)outh> <on/off> [stream #] .................................... enables on/off the IBL S-link" << endl
	     << endl;
}

void DbmConsole::cmd_to_vector(const char* command, std::vector<std::string>& list)
{
	char input_line[100];
	strcpy(input_line, command);

	list.clear();
	char* pch = strtok (input_line," ");
	while (pch != NULL)
	{
		list.push_back(pch);
		pch = strtok (NULL, " ");
	}
}

bool DbmConsole::execute_command(const char* command)
{
	std::vector<std::string> input_vector;

	// which group of commmands
	cmd_to_vector(command, input_vector);

	if(input_vector.size() == 0)
		return true;

	if(input_vector[0] == "boc")
	{
		if(input_vector.size() < 3)
		{
			cout << "! Not enough parameters for any of the BOC commands." << endl;
			return true;
		}

		if(input_vector[1] == "laser" || input_vector[1] == "l")
		{
			// do the laser status
			if(input_vector[2] == "on")
			{
				CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
				CatPointer<BocRegisterCommand> cmd2 = _pool_boc_command->get_element();
				cmd1->set_command(BocCommand::Write, BocFpga::Bcf, BmfRegisterBank::General, Bcf::Register::SecureReg, 0x42);
				cmd2->set_command(BocCommand::Write, BocFpga::Bcf, BmfRegisterBank::General, Bcf::Register::LaserEnable, 0xab);
				
				publish_data(_out_boc_command, cmd1, logger());
				publish_data(_out_boc_command, cmd2, logger());
			}
			else if(input_vector[2] == "off")
			{
				CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
				CatPointer<BocRegisterCommand> cmd2 = _pool_boc_command->get_element();
				cmd1->set_command(BocCommand::Write, BocFpga::Bcf, BmfRegisterBank::General, Bcf::Register::SecureReg, 0x42);
				cmd2->set_command(BocCommand::Write, BocFpga::Bcf, BmfRegisterBank::General, Bcf::Register::LaserEnable, 0x00);
				
				publish_data(_out_boc_command, cmd1, logger());
				publish_data(_out_boc_command, cmd2, logger());
			}
			else if(input_vector[2] == "status")
			{
				CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
				cmd1->set_command(BocCommand::Read, BocFpga::Bcf, BmfRegisterBank::General, Bcf::Register::LaserEnable);

				_reply = reply_t::laser_satus;	
				publish_data(_out_boc_command, cmd1, logger());
			}
			else
			{
				cout << "! '" << input_vector[2] << "' is not valid BOC laser command. Options: on, off, status." << endl;
			}

			return true;
		}

		if(input_vector.size() < 5)
		{
			cout << "! Not enough parameters for any of the BOC commands." << endl;
			return true;
		}

		// decode the command
		BocCommand cmd;
		if(input_vector[1] == "read" || input_vector[1] == "r")
			cmd = BocCommand::Read;
		else if(input_vector[1] == "write" || input_vector[1] == "w")
			cmd = BocCommand::Write;
		else
		{
			cout << "! '" << input_vector[1] << "' is not valid BOC command. Options: read, write." << endl;
			return true;
		}

		// decode the fpga
		BocFpga fpga;
		if(input_vector[2] == "bcf" || input_vector[2] == "b")
			fpga = BocFpga::Bcf;
		else if(input_vector[2] == "north" || input_vector[2] == "n")
			fpga = BocFpga::North;
		else if(input_vector[2] == "south" || input_vector[2] == "s")
			fpga = BocFpga::South;
		else
		{
			cout << "! '" << input_vector[2] << "' is not valid BOC fpga name. Options: bcf, north, south." << endl;
			return true;
		}

		// decode the registry bank
		BmfRegisterBank bank;
		if(input_vector[3] == "general" || input_vector[3] == "g")
			bank = BmfRegisterBank::General;
		else if(input_vector[3] == "rx" || input_vector[3] == "r")
			bank = BmfRegisterBank::Rx;
		else if(input_vector[3] == "tx" || input_vector[3] == "t")
			bank = BmfRegisterBank::Tx;
		else if(input_vector[3] == "dbm" || input_vector[3] == "d")
			bank = BmfRegisterBank::Dbm;
		else
		{
			cout << "! '" << input_vector[3] << "' is not valid BOC registry bank name. Options: general, rx, tx, dbm." << endl;
			return true;
		}

		// decode the register
		long int input;
		std::uint16_t reg;
		input = strtol(input_vector[4].c_str(), nullptr, 16);
		if(input < BOC_REGISTER_LOW || input > BOC_REGISTER_HIGH)
		{
			cout << "! Register number outside meaningful borders." << endl;
			return true;
		}
		reg = (std::uint16_t)input;

		// decode the data if it exist
		std::uint8_t data = 0x0;
		if(input_vector.size() >= 6)
		{
			input = strtol(input_vector[5].c_str(), nullptr, 16);
			if(input < 0 || input > 0xFF)
			{
				cout << "! Register value should be between 0x00 and 0xff." << endl;
				return true;
			}
			data = (std::uint8_t)input;				
		}

		// set the command and publish it
		CatPointer<BocRegisterCommand> boc_cmd = _pool_boc_command->get_element();
		boc_cmd->set_command(cmd, fpga, bank, reg, data);

		if(cmd == BocCommand::Read)
			_reply = reply_t::read_register;	

		publish_data(_out_boc_command, boc_cmd, logger());
	}
	else if(input_vector[0] == "fe")
	{
		if(input_vector.size() < 3)
		{
			cout << "! Not enough parameters for any of the FE commands." << endl;
			return true;
		}

		if(input_vector[1] == "write" || input_vector[1] == "w")
		{
			// writing to the frond end register

			if(input_vector.size() < 5)
			{
				cout << "! Not enough parameters for write-register FE command." << endl;
				return true;
			}

			int reg, value, channel;
			channel = strtol(input_vector[2].c_str(), nullptr, 0);
			reg = strtol(input_vector[3].c_str(), nullptr, 0);
			value = strtol(input_vector[4].c_str(), nullptr, 0);

			// validate inputs
			if(channel < 0 || channel >= FEI4_PER_BOC)
			{
				cout << "! TX channel can be between 0 and " << FEI4_PER_BOC << "." << endl;
				return true;
			}
			if(reg < 0 || reg >= FEI4_NUMER_OF_REGISTERS)
			{
				cout << "! Register number can be between 0 and " << FEI4_NUMER_OF_REGISTERS << "." << endl;
				return true;
			}
			if(value < 0 || value > 0xffff)
			{
				cout << "! Value can be between 0 and " << 0xffff << "." << endl;
				return true;
			}
			
			// set the FE command
			CatPointer<FEI4Message> fe_msg = _pool_fe_command->get_element();
			CatPointer<FEI4Command> fe_cmd = fe_msg.cast<FEI4Command>();
			_fe[channel]->create_cmd_write(reg, value, fe_cmd);
			fe_msg->set_address(_mapping[channel].fpga, _mapping[channel].tx, _mapping[channel].rx);

			// publish
			if(input_vector.size() == 6 && input_vector[5] == "b")
				publish_data(_out_port_fe_broadcast, fe_msg, logger());
			else
				publish_data(_out_port_fe, fe_msg, logger());
		}
		else if(input_vector[1] == "read" || input_vector[1] == "r")
		{
			// reading frond-end register

			if(input_vector.size() < 4)
			{
				cout << "! Not enough parameters for read-register FE command." << endl;
				return true;
			}

			int reg, value, channel;
			channel = strtol(input_vector[2].c_str(), nullptr, 0);
			reg = strtol(input_vector[3].c_str(), nullptr, 0);

			// validate inputs
			if(channel < 0 || channel >= FEI4_PER_BOC)
			{
				cout << "! TX channel can be between 0 and " << FEI4_PER_BOC << "." << endl;
				return true;
			}
			if(reg < 0 || reg >= FEI4_NUMER_OF_REGISTERS)
			{
				cout << "! Register number can be between 0 and " << FEI4_NUMER_OF_REGISTERS << "." << endl;
				return true;
			}
			
			// set the FE command
			CatPointer<FEI4Message> fe_msg = _pool_fe_command->get_element();
			CatPointer<FEI4Command> fe_cmd = fe_msg.cast<FEI4Command>();
			_fe[channel]->create_cmd_read(reg, fe_cmd);
			fe_msg->set_address(_mapping[channel].fpga, _mapping[channel].tx, _mapping[channel].rx);

			_reply = reply_t::read_fe_register;	
			_hold_channel = channel;

			// publish
			publish_data(_out_port_fe, fe_msg, logger());
		}
		else if(input_vector[1] == "mode" || input_vector[1] == "m")
		{
			// changing the run mode

			if(input_vector.size() < 4)
			{
				cout << "! Not enough parameters for change-mode FE command." << endl;
				return true;
			}

			int channel;
			uint8_t mode;
			channel = strtol(input_vector[2].c_str(), nullptr, 0);
			if(input_vector[3] == "config")
				mode = 0x07;
			else if(input_vector[3] == "run")
				mode = 0x38;
			else
				mode = 0;

			// validate inputs
			if(channel < 0 || channel >= FEI4_PER_BOC)
			{
				cout << "! TX channel can be between 0 and " << FEI4_PER_BOC << "." << endl;
				return true;
			}
			if(mode == 0)
			{
				cout << "! Valid modes are: config, run." << endl;
				return true;
			}
			
			// set the FE command
			CatPointer<FEI4Message> fe_msg = _pool_fe_command->get_element();
			CatPointer<FEI4Command> fe_cmd = fe_msg.cast<FEI4Command>();
			_fe[channel]->create_cmd_mode(mode, fe_cmd);
			fe_msg->set_address(_mapping[channel].fpga, _mapping[channel].tx, _mapping[channel].rx);

			// publish
			if(input_vector.size() == 5 && input_vector[4] == "b")
				publish_data(_out_port_fe_broadcast, fe_msg, logger());
			else
				publish_data(_out_port_fe, fe_msg, logger());
		}
		else if(input_vector[1] == "reset" || input_vector[1] == "s")
		{
			// changing the run mode

			if(input_vector.size() < 3)
			{
				cout << "! Not enough parameters for reset FE command." << endl;
				return true;
			}

			int channel;
			channel = strtol(input_vector[2].c_str(), nullptr, 0);

			// validate inputs
			if(channel < 0 || channel >= FEI4_PER_BOC)
			{
				cout << "! TX channel can be between 0 and " << FEI4_PER_BOC << "." << endl;
				return true;
			}
			
			// set the FE command
			CatPointer<FEI4Message> fe_msg = _pool_fe_command->get_element();
			CatPointer<FEI4Command> fe_cmd = fe_msg.cast<FEI4Command>();
			_fe[channel]->create_cmd_reset(fe_cmd);
			fe_msg->set_address(_mapping[channel].fpga, _mapping[channel].tx, _mapping[channel].rx);

			// publish
			if(input_vector.size() == 4 && input_vector[3] == "b")
				publish_data(_out_port_fe_broadcast, fe_msg, logger());
			else
				publish_data(_out_port_fe, fe_msg, logger());
		}
		else if(input_vector[1] == "frontend" || input_vector[1] == "f")
		{
			// send the frondend shift-register

			if(input_vector.size() < 4)
			{
				cout << "! Not enough parameters for front-end FE command." << endl;
				return true;
			}

			int channel;
			std::uint8_t pattern;
			channel = strtol(input_vector[2].c_str(), nullptr, 0);
			pattern = (std::uint8_t)strtol(input_vector[3].c_str(), nullptr, 0);

			// validate inputs
			if(channel < 0 || channel >= FEI4_PER_BOC)
			{
				cout << "! TX channel can be between 0 and " << FEI4_PER_BOC << "." << endl;
				return true;
			}

			// create the shift-register values
			std::uint8_t raw_data[2*FEI4_ROWS/8];
			for(std::uint16_t i=0; i<2*FEI4_ROWS/8; ++i)
				raw_data[i] = pattern;

			// create the command
			CatPointer<FEI4Message> fe_msg = _pool_fe_command->get_element();
			CatPointer<FEI4Command> fe_cmd = fe_msg.cast<FEI4Command>();
			fe_msg->set_address(_mapping[channel].fpga, _mapping[channel].tx, _mapping[channel].rx);

			_fe[channel]->create_cmd_write_fe(raw_data, fe_cmd);
			if(input_vector.size() == 5 && input_vector[4] == "b")
				publish_data(_out_port_fe_broadcast, fe_msg, logger());
			else
				publish_data(_out_port_fe, fe_msg, logger());
		}
		else if(input_vector[1] == "pulse" || input_vector[1] == "p")
		{
			// changing the run mode

			if(input_vector.size() < 4)
			{
				cout << "! Not enough parameters for pulse FE command." << endl;
				return true;
			}

			int channel, length;
			channel = strtol(input_vector[2].c_str(), nullptr, 0);
			length = strtol(input_vector[3].c_str(), nullptr, 0);

			// validate inputs
			if(channel < 0 || channel >= FEI4_PER_BOC)
			{
				cout << "! TX channel can be between 0 and " << FEI4_PER_BOC << "." << endl;
				return true;
			}
			if(length <= 0)
			{
				cout << "! Pulse length must be at least 1." << endl;
				return true;
			}
			else if(length >= 64)
			{
				cout << "! Pulse must be shorter than 64." << endl;
				return true;
			}
			
			// set the FE command
			CatPointer<FEI4Message> fe_msg = _pool_fe_command->get_element();
			CatPointer<FEI4Command> fe_cmd = fe_msg.cast<FEI4Command>();
			_fe[channel]->create_cmd_pulse((std::uint8_t)length, fe_cmd);
			fe_msg->set_address(_mapping[channel].fpga, _mapping[channel].tx, _mapping[channel].rx);

			// publish
			if(input_vector.size() == 5 && input_vector[4] == "b")
				publish_data(_out_port_fe_broadcast, fe_msg, logger());
			else
				publish_data(_out_port_fe, fe_msg, logger());
		}
		else if(input_vector[1] == "hitor" || input_vector[1] == "h")
		{
			// configuring the hitor mask
			if(input_vector.size() < 4)
			{
				cout << "! Not enough parameters for configuring HitOr mask." << endl;
				return true;
			}

			int channel;
			channel = strtol(input_vector[2].c_str(), nullptr, 0);

			// validate inputs
			if(channel < 0 || channel >= FEI4_PER_BOC)
			{
				cout << "! TX channel can be between 0 and " << FEI4_PER_BOC << "." << endl;
				return true;
			}
			
			// set the FE command
			CatPointer<FEI4Mask> mask = _pool_fe_mask->get_element();
			if(mask->load_from_file(input_vector[3]))
			{
				// mask is loaded
				std::uint8_t raw_data[2*FEI4_ROWS/8];

				// invert the mast to have HitOr
				mask->invert();

				CatPointer<FEI4Message> fe_msg = _pool_fe_command->get_element();
				CatPointer<FEI4Command> fe_cmd = fe_msg.cast<FEI4Command>();
				fe_msg->set_address(_mapping[channel].fpga, _mapping[channel].tx, _mapping[channel].rx);

				for(std::uint8_t col=1; col<80; col+=2)
				{
					// select double column
					_fe[channel]->create_cmd_write(22, (uint16_t)(CatService::reverse_bits((col-1)/2) << 2), fe_cmd);
					if(input_vector.size() == 5 && input_vector[4] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
					// set SR in normal mode and set the HitOr latch bit
					_fe[channel]->create_cmd_write(13, FEI4_PIXEL_STROBE_HITOR, fe_cmd);
					if(input_vector.size() == 5 && input_vector[4] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
					// write FE
					mask->fill_fe_stream(col, raw_data);
					_fe[channel]->create_cmd_write_fe(raw_data, fe_cmd);
					if(input_vector.size() == 5 && input_vector[4] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
					// load the latches
					// latch enable signal
					_fe[channel]->create_cmd_write(27, FEI4_PULSER_MASK_LATCH_ENABLE, FEI4_PULSER_MASK_LATCH_ENABLE, fe_cmd);
					if(input_vector.size() == 5 && input_vector[4] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
					// global pulse
					_fe[channel]->create_cmd_pulse((std::uint8_t)2, fe_cmd);
					if(input_vector.size() == 5 && input_vector[4] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
					// remove latch enable signal
					_fe[channel]->create_cmd_write(27, 0, FEI4_PULSER_MASK_LATCH_ENABLE, fe_cmd);
					if(input_vector.size() == 5 && input_vector[4] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());

					// slow down
					std::this_thread::sleep_for(std::chrono::milliseconds(500));
				}
				return true;
			}
			else
			{
				cout << "! Failed to load the file." << endl;
				return true;
			}
		}
		else if(input_vector[1] == "enable" || input_vector[1] == "e")
		{
			// configuring the hitor mask
			if(input_vector.size() < 4)
			{
				cout << "! Not enough parameters for configuring Enable mask." << endl;
				return true;
			}

			int channel;
			channel = strtol(input_vector[2].c_str(), nullptr, 0);

			// validate inputs
			if(channel < 0 || channel >= FEI4_PER_BOC)
			{
				cout << "! TX channel can be between 0 and " << FEI4_PER_BOC << "." << endl;
				return true;
			}
			
			// set the FE command
			CatPointer<FEI4Mask> mask = _pool_fe_mask->get_element();
			if(mask->load_from_file(input_vector[3]))
			{
				// mask is loaded
				std::uint8_t raw_data[2*FEI4_ROWS/8];

				CatPointer<FEI4Message> fe_msg = _pool_fe_command->get_element();
				CatPointer<FEI4Command> fe_cmd = fe_msg.cast<FEI4Command>();
				fe_msg->set_address(_mapping[channel].fpga, _mapping[channel].tx, _mapping[channel].rx);

				for(std::uint8_t col=1; col<80; col+=2)
				{
					// select double column
					_fe[channel]->create_cmd_write(22, (uint16_t)(CatService::reverse_bits((col-1)/2) << 2), fe_cmd);
					if(input_vector.size() == 5 && input_vector[4] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
					// set SR in normal mode and set the HitOr latch bit
					_fe[channel]->create_cmd_write(13, FEI4_PIXEL_STROBE_ENABLE, fe_cmd);
					if(input_vector.size() == 5 && input_vector[4] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
					// write FE
					mask->fill_fe_stream(col, raw_data);
					_fe[channel]->create_cmd_write_fe(raw_data, fe_cmd);
					if(input_vector.size() == 5 && input_vector[4] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
					// load the latches
					// latch enable signal
					_fe[channel]->create_cmd_write(27, FEI4_PULSER_MASK_LATCH_ENABLE, FEI4_PULSER_MASK_LATCH_ENABLE, fe_cmd);
					if(input_vector.size() == 5 && input_vector[4] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
					// global pulse
					_fe[channel]->create_cmd_pulse((std::uint8_t)2, fe_cmd);
					if(input_vector.size() == 5 && input_vector[4] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
					// remove latch enable signal
					_fe[channel]->create_cmd_write(27, 0, FEI4_PULSER_MASK_LATCH_ENABLE, fe_cmd);
					if(input_vector.size() == 5 && input_vector[4] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());

				}
				return true;
			}
			else
			{
				cout << "! Failed to load the file." << endl;
				return true;
			}
		}
		else if(input_vector[1] == "digital" || input_vector[1] == "d")
		{
			// configuring the hitor mask
			if(input_vector.size() < 5)
			{
				cout << "! Not enough parameters for Digital pulse." << endl;
				return true;
			}

			int channel;
			uint16_t length;
			channel = strtol(input_vector[2].c_str(), nullptr, 0);
			length = strtol(input_vector[4].c_str(), nullptr, 0);

			// validate inputs
			if(channel < 0 || channel >= FEI4_PER_BOC)
			{
				cout << "! TX channel can be between 0 and " << FEI4_PER_BOC << "." << endl;
				return true;
			}
			if(length < 1 || length > 15)
			{
				cout << "! Length must be between 1 and 15." << endl;
				return true;
			}
			
			// set the FE command
			CatPointer<FEI4Mask> mask = _pool_fe_mask->get_element();
			if(mask->load_from_file(input_vector[3]))
			{
				// mask is loaded
				std::uint8_t raw_data[2*FEI4_ROWS/8];

				CatPointer<FEI4Message> fe_msg = _pool_fe_command->get_element();
				CatPointer<FEI4Command> fe_cmd = fe_msg.cast<FEI4Command>();
				fe_msg->set_address(_mapping[channel].fpga, _mapping[channel].tx, _mapping[channel].rx);

				// enable digital injection
				_fe[channel]->create_cmd_write(21, FEI4_ENABLE_DIGITAL_INJECTION, FEI4_ENABLE_DIGITAL_INJECTION, fe_cmd);
				if(input_vector.size() == 6 && input_vector[5] == "b")
					publish_data(_out_port_fe_broadcast, fe_msg, logger());
				else
					publish_data(_out_port_fe, fe_msg, logger());
				// enable pulser flag
				_fe[channel]->create_cmd_write(27, FEI4_PULSER_MASK_DIGITAL_ENABLE, FEI4_PULSER_MASK_DIGITAL_ENABLE, fe_cmd);
				if(input_vector.size() == 6 && input_vector[5] == "b")
					publish_data(_out_port_fe_broadcast, fe_msg, logger());
				else
					publish_data(_out_port_fe, fe_msg, logger());

				for(std::uint8_t col=1; col<80; col+=2)
				{
					// select double column
					_fe[channel]->create_cmd_write(22, (uint16_t)(CatService::reverse_bits((col-1)/2) << 2), fe_cmd);
					if(input_vector.size() == 6 && input_vector[5] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
					// set SR in normal mode and set the HitOr latch bit
					_fe[channel]->create_cmd_write(13, FEI4_PIXEL_STROBE_HITOR, fe_cmd);
					if(input_vector.size() == 6 && input_vector[5] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
					// write FE
					mask->fill_fe_stream(col, raw_data);
					_fe[channel]->create_cmd_write_fe(raw_data, fe_cmd);
					if(input_vector.size() == 6 && input_vector[5] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
					// global pulse
					_fe[channel]->create_cmd_pulse((std::uint8_t)length, fe_cmd);
					if(input_vector.size() == 6 && input_vector[5] == "b")
						publish_data(_out_port_fe_broadcast, fe_msg, logger());
					else
						publish_data(_out_port_fe, fe_msg, logger());
				}
				// disable digital injection
				_fe[channel]->create_cmd_write(21, 0, FEI4_ENABLE_DIGITAL_INJECTION, fe_cmd);
				if(input_vector.size() == 6 && input_vector[5] == "b")
					publish_data(_out_port_fe_broadcast, fe_msg, logger());
				else
					publish_data(_out_port_fe, fe_msg, logger());
				// disable pulser flag
				_fe[channel]->create_cmd_write(27, 0, FEI4_PULSER_MASK_DIGITAL_ENABLE, fe_cmd);
				if(input_vector.size() == 6 && input_vector[5] == "b")
					publish_data(_out_port_fe_broadcast, fe_msg, logger());
				else
					publish_data(_out_port_fe, fe_msg, logger());

				return true;
			}
			else
			{
				cout << "! Failed to load the file." << endl;
				return true;
			}
		}
	}
	else if(input_vector[0] == "hb")
	{
		if(input_vector.size() < 4)
		{
			cout << "! Not enough parameters for any of the HitBus commands." << endl;
			return true;
		}

		if(input_vector[1] == "write" || input_vector[1] == "w")
		{
			// decode the fpga
			BocFpga fpga;
			if(input_vector[2] == "north" || input_vector[2] == "n")
				fpga = BocFpga::North;
			else if(input_vector[2] == "south" || input_vector[2] == "s")
				fpga = BocFpga::South;
			else
			{
				cout << "! '" << input_vector[2] << "' is not valid BOC fpga name. Options: north, south." << endl;
				return true;
			}

			// decode the channel
			long int input;
			std::uint8_t channel;
			input = strtol(input_vector[3].c_str(), nullptr, 0);
			if(!(input == HITBUS_CHANNEL_0 || input == HITBUS_CHANNEL_1))
			{
				cout << "! HitBus channel must be either " << HITBUS_CHANNEL_0 << " or " << HITBUS_CHANNEL_1 << "." << endl;
				return true;
			}
			channel = (std::uint8_t)input;

			// decode the register
			std::uint8_t reg;
			input = strtol(input_vector[4].c_str(), nullptr, 0);
			if(input < 0 || input > 0xFF)
			{
				cout << "! Register address should be between 0x00 and 0xff." << endl;
				return true;
			}
			reg = (std::uint8_t)input;				

			// decode the data if it exist
			std::uint8_t data = 0x0;
			if(input_vector.size() >= 6)
			{
				input = strtol(input_vector[5].c_str(), nullptr, 0);
				if(input < 0 || input > 0xFF)
				{
					cout << "! Register value should be between 0x00 and 0xff." << endl;
					return true;
				}
				data = (std::uint8_t)input;				
			}		

			// send the appropriate commands
			CatPointer<BocRegisterCommand> boc_cmd = _pool_boc_command->get_element();
			// set the address
			boc_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, channel == HITBUS_CHANNEL_0 ? BOC_DBM_REGISTER_HITBUS_0_ADDR : BOC_DBM_REGISTER_HITBUS_1_ADDR, reg);
			publish_data(_out_boc_command, boc_cmd, logger());
			// send data
			boc_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, channel == HITBUS_CHANNEL_0 ? BOC_DBM_REGISTER_HITBUS_0_DATA : BOC_DBM_REGISTER_HITBUS_1_DATA, data);
			publish_data(_out_boc_command, boc_cmd, logger());
		}
		else if(input_vector[1] == "receive" || input_vector[1] == "r")
		{
			if(input_vector.size() < 5)
			{
				cout << "! Not enough parameters for hb receive command." << endl;
				return true;
			}

			BocFpga fpga;
			if(input_vector[2] == "north" || input_vector[2] == "n")
				fpga = BocFpga::North;
			else if(input_vector[2] == "south" || input_vector[2] == "s")
				fpga = BocFpga::South;
			else
			{
				cout << "! '" << input_vector[2] << "' is not valid BOC fpga name. Options: north, south." << endl;
				return true;
			}

			bool on;
			if(input_vector[3] == "on")
			{
				on = true;
			}
			else if(input_vector[3] == "off")
			{
				on = false;
			}
			else
			{
				cout << "! '" << input_vector[3] << "' is not valid hb receive slink command. Options: on, off." << endl;
				return true;
			}

			std::uint8_t ch;
			ch = (std::uint8_t)atoi(input_vector[4].c_str());
			if(!(ch == 0 || ch == 1))
			{
				cout << "! '" << input_vector[4] << "' is not valid slink channel number. Options: 0, 1." << endl;
				return true;
			}

			// remember the variables
			_hold_h_on = on;
        	_hold_h_ch = ch;

			// read the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Read, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_HSTREAM_CONTROL);

			_reply = reply_t::enable_h_rx;	
			publish_data(_out_boc_command, cmd1, logger());
		}
		else if(input_vector[1] == "pattern" || input_vector[1] == "p")
		{
			if(input_vector.size() < 5)
			{
				cout << "! Not enough parameters for hb pattern command." << endl;
				return true;
			}

			BocFpga fpga;
			if(input_vector[2] == "north" || input_vector[2] == "n")
				fpga = BocFpga::North;
			else if(input_vector[2] == "south" || input_vector[2] == "s")
				fpga = BocFpga::South;
			else
			{
				cout << "! '" << input_vector[2] << "' is not valid BOC fpga name. Options: north, south." << endl;
				return true;
			}

			bool pattern_a;
			if(input_vector[3] == "A")
			{
				pattern_a = true;
			}
			else if(input_vector[3] == "B")
			{
				pattern_a = false;
			}
			else
			{
				cout << "! '" << input_vector[3] << "' is not valid hb pattern. Options: A, B." << endl;
				return true;
			}

			std::uint8_t ch;
			ch = (std::uint8_t)atoi(input_vector[4].c_str());
			if(!(ch == 0 || ch == 1 || ch == 2 || ch == 3))
			{
				cout << "! '" << input_vector[4] << "' is not valid slink channel number. Options: 0, 1, 2, 3." << endl;
				return true;
			}

			// remember the variables
			_hold_h_on = pattern_a;
        	_hold_h_ch = ch;

			// read the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Read, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_DBM_CONTROL);

			_reply = reply_t::hb_pattern;	
			publish_data(_out_boc_command, cmd1, logger());
		}
		else if(input_vector[1] == "generator" || input_vector[1] == "g")
		{
			if(input_vector.size() < 4)
			{
				cout << "! Not enough parameters for hb generator command." << endl;
				return true;
			}

			BocFpga fpga;
			if(input_vector[2] == "north" || input_vector[2] == "n")
				fpga = BocFpga::North;
			else if(input_vector[2] == "south" || input_vector[2] == "s")
				fpga = BocFpga::South;
			else
			{
				cout << "! '" << input_vector[2] << "' is not valid BOC fpga name. Options: north, south." << endl;
				return true;
			}

			bool on;
			if(input_vector[3] == "on")
			{
				on = true;
			}
			else if(input_vector[3] == "off")
			{
				on = false;
			}
			else
			{
				cout << "! '" << input_vector[3] << "' is not valid hb generator slink command. Options: on, off." << endl;
				return true;
			}

			// remember the variables
			_hold_h_on = on;

			// read the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Read, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_HSTREAM_CONTROL);

			_reply = reply_t::enable_h_generator;	
			publish_data(_out_boc_command, cmd1, logger());
		}
		else
		{
			cout << "Unknown HitBus command." << endl;
			return true;
		}
	}
	else if(input_vector[0] == "help" || input_vector[0] == "h")
	{
		display_help();
	}
	else if(input_vector[0] == "quit" || input_vector[0]== "q")
	{
		// ending the program
		return false;
	}
	else if(input_vector[0] == "lumi")
	{
		if(input_vector.size() < 4)
		{
			cout << "! Not enough parameters for any of the Lumi commands." << endl;
			return true;
		}

		if(input_vector[1] == "hstream" || input_vector[1] == "h")
		{
			if(input_vector.size() < 5)
			{
				cout << "! Not enough parameters for Lumi hstream command." << endl;
				return true;
			}

			BocFpga fpga;
			if(input_vector[2] == "north" || input_vector[2] == "n")
				fpga = BocFpga::North;
			else if(input_vector[2] == "south" || input_vector[2] == "s")
				fpga = BocFpga::South;
			else
			{
				cout << "! '" << input_vector[2] << "' is not valid BOC fpga name. Options: north, south." << endl;
				return true;
			}

			bool on;
			if(input_vector[3] == "on")
			{
				on = true;
			}
			else if(input_vector[3] == "off")
			{
				on = false;
			}
			else
			{
				cout << "! '" << input_vector[3] << "' is not valid lumi hstream command. Options: on, off." << endl;
				return true;
			}

			std::uint8_t ch;
			ch = (std::uint8_t)atoi(input_vector[4].c_str());
			if(!(ch == 0 || ch == 1))
			{
				cout << "! '" << input_vector[4] << "' is not valid hstream channel number. Options: 0, 1." << endl;
				return true;
			}

			// remember the variables
			_hold_h_on = on;
        	_hold_h_ch = ch;

			// read the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Read, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_HSTREAM_CONTROL);

			_reply = reply_t::h_stream_enable;	
			publish_data(_out_boc_command, cmd1, logger());
		}
		else if(input_vector[1] == "slink" || input_vector[1] == "s")
		{
			if(input_vector.size() < 5)
			{
				cout << "! Not enough parameters for Lumi slink command." << endl;
				return true;
			}

			BocFpga fpga;
			if(input_vector[2] == "north" || input_vector[2] == "n")
				fpga = BocFpga::North;
			else if(input_vector[2] == "south" || input_vector[2] == "s")
				fpga = BocFpga::South;
			else
			{
				cout << "! '" << input_vector[2] << "' is not valid BOC fpga name. Options: north, south." << endl;
				return true;
			}

			bool on;
			if(input_vector[3] == "on")
			{
				on = true;
			}
			else if(input_vector[3] == "off")
			{
				on = false;
			}
			else
			{
				cout << "! '" << input_vector[3] << "' is not valid lumi slink command. Options: on, off." << endl;
				return true;
			}

			std::uint8_t ch;
			ch = (std::uint8_t)atoi(input_vector[4].c_str());
			if(!(ch == 0 || ch == 1))
			{
				cout << "! '" << input_vector[4] << "' is not valid slink channel number. Options: 0, 1." << endl;
				return true;
			}

			// remember the variables
			_hold_h_on = on;
        	_hold_h_ch = ch;

			// read the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Read, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_DBM_CONTROL);

			_reply = reply_t::dbm_slink_reset;	
			publish_data(_out_boc_command, cmd1, logger());
		}
		else if(input_vector[1] == "hreset" || input_vector[1] == "r")
		{
			if(input_vector.size() < 5)
			{
				cout << "! Not enough parameters for Lumi hreset command." << endl;
				return true;
			}

			BocFpga fpga;
			if(input_vector[2] == "north" || input_vector[2] == "n")
				fpga = BocFpga::North;
			else if(input_vector[2] == "south" || input_vector[2] == "s")
				fpga = BocFpga::South;
			else
			{
				cout << "! '" << input_vector[2] << "' is not valid BOC fpga name. Options: north, south." << endl;
				return true;
			}

			bool on;
			if(input_vector[3] == "on")
			{
				on = true;
			}
			else if(input_vector[3] == "off")
			{
				on = false;
			}
			else
			{
				cout << "! '" << input_vector[3] << "' is not valid lumi hreset command. Options: on, off." << endl;
				return true;
			}

			std::uint8_t ch;
			ch = (std::uint8_t)atoi(input_vector[4].c_str());
			if(!(ch == 0 || ch == 1))
			{
				cout << "! '" << input_vector[4] << "' is not valid hstream channel number. Options: 0, 1." << endl;
				return true;
			}

			// remember the variables
			_hold_h_on = on;
        	_hold_h_ch = ch;

			// read the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Read, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_HSTREAM_CONTROL);

			_reply = reply_t::h_stream_reset;	
			publish_data(_out_boc_command, cmd1, logger());
		}
		else if(input_vector[1] == "map" || input_vector[1] == "m")
		{
			if(input_vector.size() < 4)
			{
				cout << "! Not enough parameters for Lumi map command." << endl;
				return true;
			}

			BocFpga fpga;
			if(input_vector[2] == "north" || input_vector[2] == "n")
				fpga = BocFpga::North;
			else if(input_vector[2] == "south" || input_vector[2] == "s")
				fpga = BocFpga::South;
			else
			{
				cout << "! '" << input_vector[2] << "' is not valid BOC fpga name. Options: north, south." << endl;
				return true;
			}
			
			CatFile f;
			if(f.open_file(input_vector[3],true,false))
			{
				// set the algorithm mask
				CatPointer<LumiAlgoLogic> logic = f.get_next_object().unsafeCast<LumiAlgoLogic>();
				CatPointer<BocRegisterCommand> reg_cmd = _pool_boc_command->get_element();
				uint16_t tmp16 = logic->algorithm_get_mask();
				if(tmp16 == 0)
				{
					// mask is empty so nothing to set
					// do nothing
					cout<< "No algorithm enabled. Nothing to do." << endl;
					return true;
				}
			
				reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_MASK_HIGH, (std::uint8_t)(tmp16 >> 8));
				publish_data(_out_boc_command, reg_cmd, logger());
				reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_MASK_LOW, (std::uint8_t)(tmp16));
				publish_data(_out_boc_command, reg_cmd, logger());

			
				// set the values for HIT LUT
				for(uint16_t addr=0; addr<LUMI_ALGORITHM_COMBINATIONS; ++addr)
				{
					// set the address
					if((addr & 0xFF) == 0)
					{
						// this is first addres in a batch
						reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_ADDR_HIGH, (std::uint8_t)((addr | LUMI_ALGORITHM_ADDRESS_HIT_FLAG) >> 8));
						publish_data(_out_boc_command, reg_cmd, logger());
					}
					reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_ADDR_LOW, (std::uint8_t)(addr));
					publish_data(_out_boc_command, reg_cmd, logger());

					// send the data
					tmp16 = logic->algorithm_get_logic_hit(addr);
					reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_DATA_HIGH, (std::uint8_t)(tmp16 >> 8) & 0x0F);
					publish_data(_out_boc_command, reg_cmd, logger());
					reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_DATA_LOW, (std::uint8_t)(tmp16));
					publish_data(_out_boc_command, reg_cmd, logger());
				}

			
				// set the values for LIVE LUT
				for(uint16_t addr=0; addr<LUMI_ALGORITHM_COMBINATIONS; ++addr)
				{
					// set the address
					if((addr & 0xFF) == 0)
					{
						// this is first addres in a batch
						reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_ADDR_HIGH, (std::uint8_t)(addr >> 8));
						publish_data(_out_boc_command, reg_cmd, logger());
					}
					reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_ADDR_LOW, (std::uint8_t)(addr));
					publish_data(_out_boc_command, reg_cmd, logger());

					// send the data
					tmp16 = logic->algorithm_get_logic_live(addr);
					reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_DATA_HIGH, (std::uint8_t)(tmp16 >> 8) & 0x0F);
					publish_data(_out_boc_command, reg_cmd, logger());
					reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_DATA_LOW, (std::uint8_t)(tmp16));
					publish_data(_out_boc_command, reg_cmd, logger());
				}
			}
			else
			{
				cout << "! Failed to load the file." << endl;
				return true;
			}

			
		}
		if(input_vector[1] == "interval" || input_vector[1] == "i")
		{
			if(input_vector.size() < 4)
			{
				cout << "! Not enough parameters for Lumi interval command." << endl;
				return true;
			}
			BocFpga fpga;
			if(input_vector[2] == "north" || input_vector[2] == "n")
				fpga = BocFpga::North;
			else if(input_vector[2] == "south" || input_vector[2] == "s")
				fpga = BocFpga::South;
			else
			{
				cout << "! '" << input_vector[2] << "' is not valid BOC fpga name. Options: north, south." << endl;
				return true;
			}

			uint32_t length;
			length = strtol(input_vector[3].c_str(), nullptr, 0);
			if(length <= 0)
			{
				cout << "! Length must be at least 1." << endl;
				return true;
			}
			else if(length >= 65535)
			{
				cout << "! Length must be shorter than 65535." << endl;
				return true;
			}
			else
			{
				
				CatPointer<BocRegisterCommand> boc_cmd = _pool_boc_command->get_element();
				
				boc_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, 3, (uint8_t)(length >> 8));
				
				publish_data(_out_boc_command, boc_cmd, logger());
				
				boc_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, 2, (uint8_t)(length));
				
				publish_data(_out_boc_command, boc_cmd, logger());
			}			
		}
	}
	else if(input_vector[0] == "ibl")
	{
		if(input_vector.size() < 4)
		{
			cout << "! Not enough parameters for any of the IBL commands." << endl;
			return true;
		}

		if(input_vector[1] == "slink" || input_vector[1] == "s")
		{
			if(input_vector.size() < 5)
			{
				cout << "! Not enough parameters for ibl slink command." << endl;
				return true;
			}

			BocFpga fpga;
			if(input_vector[2] == "north" || input_vector[2] == "n")
				fpga = BocFpga::North;
			else if(input_vector[2] == "south" || input_vector[2] == "s")
				fpga = BocFpga::South;
			else
			{
				cout << "! '" << input_vector[2] << "' is not valid BOC fpga name. Options: north, south." << endl;
				return true;
			}

			bool on;
			if(input_vector[3] == "on")
			{
				on = true;
			}
			else if(input_vector[3] == "off")
			{
				on = false;
			}
			else
			{
				cout << "! '" << input_vector[3] << "' is not valid ibl slink command. Options: on, off." << endl;
				return true;
			}

			std::uint8_t ch;
			ch = (std::uint8_t)atoi(input_vector[4].c_str());
			if(!(ch == 0 || ch == 1))
			{
				cout << "! '" << input_vector[4] << "' is not valid slink channel number. Options: 0, 1." << endl;
				return true;
			}

			// remember the variables
			_hold_h_on = on;
        	_hold_h_ch = ( ch == 0 ? BOC_IBL_REGISTER_SLINK_0 : BOC_IBL_REGISTER_SLINK_1);

			// read the current value of the register
			CatPointer<BocRegisterCommand> cmd1 = _pool_boc_command->get_element();
			cmd1->set_command(BocCommand::Read, fpga, BmfRegisterBank::General, _hold_h_ch);

			_reply = reply_t::ibl_slink_enable;	
			publish_data(_out_boc_command, cmd1, logger());
		}
	}
	else
	{
		cout << "! Unknown command." << endl;
	}

	return true;
}

void DbmConsole::work_init()
{
	char input_line[100];

	if(!_silent)
	{
		// greet the user
		cout << "***************" << endl
		     << "* DBM CONSOLE *" << endl
		     << "***************" << endl << endl;
	}

	// execute predefined commands
	for(auto& setting: algo_settings()["commands"])
	{
		if(setting.name() == "cmd")
		{
			if(!execute_command(setting.get_setting<std::string>("", "").c_str()))
			{
				return;
			}
		}
	}

	// preload FE registers
	char name[100];
	for(std::uint8_t i; i<FEI4_PER_BOC; ++i)
	{
		sprintf(name, "links/link_%u/preload", i);
		if(algo_settings().has_element(name) && algo_settings().get_setting<bool>(name, false))
		{
			// preload all the registers
			CatPointer<FEI4Command> fe_cmd = _pool_fe_command->get_element();

			for(std::uint8_t r=0; r<FEI4_NUMER_OF_WR_REGISTERS; ++r)
			{
				_fe[i]->create_cmd_write(r, fe_cmd);
				publish_data(_out_port_fe+i, fe_cmd, logger());				
			}
		}
	}

	if(!_silent)
	{
		// converse with user
		while(true)
		{
			// ask for input
			cout << "> ";
			cin.getline(input_line, 100);

			if(!execute_command(input_line))
				break;
		}
	}
}

void DbmConsole::work_deinit()
{
}

} // namespace DbmShell

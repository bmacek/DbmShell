#include "DbmShell/hardware/BocRegisterCommand.h"



using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(BocRegisterCommand, CAT_NAME_BOCREGISTERCOMMAND, CAT_TYPE_BOCREGISTERCOMMAND);

BocRegisterCommand::BocRegisterCommand()
{
}

BocRegisterCommand::~BocRegisterCommand()
{
}

void BocRegisterCommand::cat_stream_out(std::ostream& output)
{
        std::uint8_t tmp;
        // command type
        switch(_command)
        {
        case BocCommand::Read: tmp = 0x01; break;
        case BocCommand::Write: tmp = 0x02; break;
        default: tmp = 0x00; break;
        };
        output.write((char*)&tmp, sizeof(std::uint8_t));
        // fpga
        switch(_fpga)
        {
        case BocFpga::Bcf: tmp = 0x01; break;
        case BocFpga::North: tmp = 0x02; break;
        case BocFpga::South: tmp = 0x03; break;
        default: tmp = 0x00; break;
        };
        output.write((char*)&tmp, sizeof(std::uint8_t));
        // register bank
        switch(_bank)
        {
        case BmfRegisterBank::General: tmp = 0x01; break;
        case BmfRegisterBank::Rx: tmp = 0x02; break;
        case BmfRegisterBank::Tx: tmp = 0x03; break;
        case BmfRegisterBank::Dbm: tmp = 0x04; break;
        default: tmp = 0x00; break;
        };
        output.write((char*)&tmp, sizeof(std::uint8_t));
        //
        output.write((char*)&_register, sizeof(std::uint16_t));
        output.write((char*)&_data, sizeof(std::uint8_t));
}

void BocRegisterCommand::cat_stream_in(std::istream& input)
{
        std::uint8_t tmp;

        // command type
        input.read((char*)&tmp, sizeof(std::uint8_t));
        switch(tmp)
        {
        case 0x01: _command = BocCommand::Read; break;
        case 0x02: _command = BocCommand::Write; break;
        default: _command = BocCommand::Read; break;
        };
        // fpga
        input.read((char*)&tmp, sizeof(std::uint8_t));
        switch(tmp)
        {
        case 0x01: _fpga = BocFpga::Bcf; break;
        case 0x02: _fpga = BocFpga::North; break;
        case 0x03: _fpga = BocFpga::South; break;
        default: _fpga = BocFpga::Bcf; break;
        };
        // register bank
        input.read((char*)&tmp, sizeof(std::uint8_t));
        switch(tmp)
        {
        case 0x01: _bank = BmfRegisterBank::General; break;
        case 0x02: _bank = BmfRegisterBank::Rx; break;
        case 0x03: _bank = BmfRegisterBank::Tx; break;
        case 0x04: _bank = BmfRegisterBank::Dbm; break;
        default: _bank = BmfRegisterBank::General; break;
        };
        //
        input.read((char*)&_register, sizeof(std::uint16_t));
        input.read((char*)&_data, sizeof(std::uint8_t));
}

void BocRegisterCommand::cat_print(std::ostream& output, const std::string& padding)
{
	CatObject::cat_print(output, padding);
	output << " Register command: ";
        switch(_command)
        {
        case BocCommand::Read: output << "read from  "; break;
        case BocCommand::Write: output << "write to   "; break;
        default: output <<   "?? to/from "; break;
        };
        // fpga
        switch(_fpga)
        {
        case BocFpga::Bcf: output << "Bcf       "; break;
        case BocFpga::North: output << "Bmf north "; break;
        case BocFpga::South: output << "Bmf south "; break;
        default: output << "??        "; break;
        };
        // register bank
        switch(_bank)
        {
        case BmfRegisterBank::General: output << " in bank 'General' "; break;
        case BmfRegisterBank::Rx: output << " in bank 'Rx'      "; break;
        case BmfRegisterBank::Tx: output << " in bank 'Tx'      "; break;
        case BmfRegisterBank::Dbm: output << " in bank 'Dbm'     "; break;
        default: output << " in bank ??        "; break;
        };
        //
        output << " register 0x" << hex << _register << " with value 0x" << hex << (unsigned int)_data << dec;
}

} // namespace DbmShell

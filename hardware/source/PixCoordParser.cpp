#include "DbmShell/hardware/PixCoordParser.h"
#include "DbmShell/hardware/PixCoord.h"
#include "CatShell/core/PluginDeamon.h"


using namespace CatShell;
using namespace std;

namespace DbmShell{

CAT_OBJECT_IMPLEMENT(PixCoordParser, CAT_NAME_PIXCOORDPARSER,CAT_TYPE_PIXCOORDPARSER);

PixCoordParser::PixCoordParser()
{
}

PixCoordParser::~PixCoordParser()
{
}

void PixCoordParser::cat_stream_out(std::ostream& output)
{
      
}

void PixCoordParser::cat_stream_in(std::istream& input)
{

}

void PixCoordParser::cat_print(std::ostream& output, const std::string& padding)
{

}

void* PixCoordParser::cat_get_part(CatAddress& addr)
{
	return nullptr;
}

void PixCoordParser::algorithm_init()
{
	Process::algorithm_init();
	_pix_out = declare_output_port("pix_coordinates", CAT_NAME_PIXCOORD);
	_file_name = algo_settings().get_setting<std::string>("file_name", "pix_settings");
}

void PixCoordParser::algorithm_deinit()
{
	Process::algorithm_deinit();
}

void PixCoordParser::process_data(CatShell::Algorithm::DataPort port, CatShell::CatPointer<CatShell::CatObject>& object,bool flush, CatShell::Logger&logger)
{
	Algorithm::process_data(port, object, flush, logger);
}

void PixCoordParser::work_init()
{
    _myfile.open(_file_name, ios::in);
	INFO(logger(), "Open file -> "<< _file_name);
}

void PixCoordParser::work_deinit()
{
	_myfile.close();
	INFO(logger(), "Close file -> " << _file_name);	
}

void PixCoordParser::work_main()
{
	CatShell::MemoryPool<PixCoord>*  _pool = PluginDeamon::get_pool<PixCoord>();
	CatPointer<PixCoord> c;
		
	char line[100];
	int line_elements;
	int x,y;
	int i;
	i=0;

	while (!_myfile.eof())
	{
		if (!should_continue(false,0))
			break;	

		_myfile.getline(line,100);
		line_elements = sscanf (line,"%d %d" , &x, &y);

		if (line_elements >0)
		{		
			++i;
			c = _pool->get_element();		
			c->set(y,x);
			publish_data(_pix_out,c,logger());
		}	
	}
	flush_data_clients(_pix_out, logger());
	INFO(logger(), " Number of pixels read from the file = " << i );
}
}

#include "DbmShell/hardware/LumiAlgoCreator.h"

#include "CatShell/core/PluginDeamon.h"

using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(LumiAlgoCreator, CAT_NAME_LUMIALGOCREATOR, CAT_TYPE_LUMIALGOCREATOR);

LumiAlgoCreator::LumiAlgoCreator()
{
}

LumiAlgoCreator::~LumiAlgoCreator()
{
}

void LumiAlgoCreator::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
	// processing input data

	// no input data is used
}

void LumiAlgoCreator::algorithm_init()
{
	Process::algorithm_init();

	// define the output port
	_out_port = declare_output_port("out_logic", CAT_NAME_LUMIALGOLOGIC);
	
}

void LumiAlgoCreator::algorithm_deinit()
{
	Process::algorithm_deinit();
}

std::uint16_t LumiAlgoCreator::menu()
{
	std::uint16_t choice;
	cout	<< "############## Menu ############" << endl
			<< "0 - quit           6 - or" << endl
			<< "1 - new logic      7 - xor" << endl
			<< "2 - list logics    8 - copy" << endl
			<< "3 - print logic    9 - output" << endl
			<< "4 - invert logic" << endl
			<< "5 - and            11 - toggle mask" << endl
			<< "choice: "; cin >> choice;
	return choice;
}

void LumiAlgoCreator::work_main()
{
	// prepare the memory pools
	CatShell::MemoryPool<LumiAlgoLogic>*  _pool = PluginDeamon::get_pool<LumiAlgoLogic>(); // memory pool logics

	// do the work according to user wishes.
	std::uint16_t choice;
	while(true)
	{
		choice = menu();
		if(choice == 0)
		{
			break;
		}
		else if(choice == 1)
		{
			// creating a new logic

			std::uint16_t algorithm, input;
			std::string name;

			// ask user for data
			cout << "Algorithm number (0-"<< LUMI_ALGORITHM_MAPS/2-1 <<" are for stream-0 and "<< LUMI_ALGORITHM_MAPS/2 <<"-"<< LUMI_ALGORITHM_MAPS-1 <<" for stream-1): "; cin >> algorithm;
			cout << "Input signal (0-"<< LUMI_ALGORITHM_INPUTS-1 <<"): "; cin >> input;
			cout << "Logic name: "; cin >> name;

			CatPointer<LumiAlgoLogic> logic;

			auto result = _logics.find(name);
			if(result == _logics.end())
			{
				logic = _pool->get_element();
			}
			else
			{
				logic = result->second;
			}

			// logic
			logic->algorithm_fill_simple(algorithm, input);

			// insert it in the list of algorithms
			auto insert_result = _logics.emplace(name, logic);
			if(!insert_result.second)
			{
				cout << "Logic updated." << endl;
			}
			else
			{
				cout << "Logic '" << name << "' created :)" << endl;
			}
		}
		else if(choice == 2)
		{
			// listing all logics
			cout << "Available logics: ";
			for(auto& x: _logics)
			{
				cout << x.first << ",";
			}
			cout << endl;
		}
		else if(choice == 3)
		{
			std::string name;
			cout << "Logic name: "; cin >> name;

			auto result = _logics.find(name);
			if(result == _logics.end())
			{
				cout << "ERROR: no logic named '" << name << "' found!" << endl;
			}
			else
			{
				result->second->cat_print(std::cout, "   "); cout << endl;
			}
		}
		else if(choice == 4)
		{
			// inverting logic
			std::string name;
			std::uint16_t algorithm;
			cout << "Logic name: "; cin >> name;
			cout << "Algorithm number (0-"<< LUMI_ALGORITHM_MAPS/2-1 <<" are for stream-0 and "<< LUMI_ALGORITHM_MAPS/2 <<"-"<< LUMI_ALGORITHM_MAPS-1 <<" for stream-1): "; cin >> algorithm;
			auto result = _logics.find(name);
			if(result == _logics.end())
			{
				cout << "ERROR: no logic named '" << name << "' found!" << endl;
			}
			else
			{
				result->second->algorithm_invert(algorithm);
				cout << "Algorithm inverted." << endl;
			}
		}
		else if(choice >= 5 && choice <= 7)
		{
			// calcluating and
			std::string name_1, name_2, name_result;
			std::uint16_t algorithm_1, algorithm_2, algorithm_result;
			cout << "Input logic 1 name: "; cin >> name_1;
			cout << "Input logic 1 algorithm: "; cin >> algorithm_1;
			cout << "Input logic 2 name: "; cin >> name_2;
			cout << "Input logic 2 algorithm: "; cin >> algorithm_2;
			cout << "Output logic name: "; cin >> name_result;
			cout << "Output logic algorithm: "; cin >> algorithm_result;

			auto logic_1 = _logics.find(name_1);
			if(logic_1 == _logics.end())
			{
				cout << "ERROR: no logic named '" << name_1 << "' found!" << endl;
				continue;
			}
			auto logic_2 = _logics.find(name_2);
			if(logic_2 == _logics.end())
			{
				cout << "ERROR: no logic named '" << name_2 << "' found!" << endl;
				continue;
			}
			auto logic_result = _logics.find(name_result);
			if(logic_result == _logics.end())
			{
				cout << "ERROR: no logic named '" << name_result << "' found!" << endl;
				continue;
			}

			if(choice == 5)
				logic_result->second->algorithm_combine(algorithm_result, LumiAlgoLogic::Operation::op_and, logic_1->second, algorithm_1, logic_2->second, algorithm_2);
			else if(choice == 6)
				logic_result->second->algorithm_combine(algorithm_result, LumiAlgoLogic::Operation::op_or, logic_1->second, algorithm_1, logic_2->second, algorithm_2);
			else if(choice == 7)
				logic_result->second->algorithm_combine(algorithm_result, LumiAlgoLogic::Operation::op_xor, logic_1->second, algorithm_1, logic_2->second, algorithm_2);

			cout << "Logic merged." << endl;
		}
		else if(choice == 8)
		{
			// calcluating and
			std::string name_1, name_result;
			std::uint16_t algorithm_1, algorithm_result;
			cout << "Input logic name: "; cin >> name_1;
			cout << "Input logic algorithm: "; cin >> algorithm_1;
			cout << "Output logic name: "; cin >> name_result;
			cout << "Output logic algorithm: "; cin >> algorithm_result;

			auto logic_1 = _logics.find(name_1);
			if(logic_1 == _logics.end())
			{
				cout << "ERROR: no logic named '" << name_1 << "' found!" << endl;
				continue;
			}
			auto logic_result = _logics.find(name_result);
			if(logic_result == _logics.end())
			{
				cout << "ERROR: no logic named '" << name_result << "' found!" << endl;
				continue;
			}

			logic_result->second->algorithm_copy(algorithm_result, logic_1->second, algorithm_1);

			cout << "Algorithm copied." << endl;
		}
		else if(choice == 9)
		{
			std::string name;
			cout << "Logic name: "; cin >> name;

			auto logic = _logics.find(name);
			if(logic == _logics.end())
			{
				cout << "ERROR: no logic named '" << name << "' found!" << endl;
				continue;
			}

			// publish the map
			publish_data(_out_port, logic->second, logger());

			cout << "Logic was published." << endl;
		}
		else if(choice == 11)
		{
			std::string name;
			std::uint16_t algorithm;
			cout << "Logic name: "; cin >> name;
			cout << "Algorithm number (0-"<< LUMI_ALGORITHM_MAPS/2-1 <<" are for stream-0 and "<< LUMI_ALGORITHM_MAPS/2 <<"-"<< LUMI_ALGORITHM_MAPS-1 <<" for stream-1): "; cin >> algorithm;
			auto result = _logics.find(name);
			if(result == _logics.end())
			{
				cout << "ERROR: no logic named '" << name << "' found!" << endl;
			}
			else
			{
				result->second->algorithm_set_mask(algorithm, !(result->second->algorithm_get_mask(algorithm)));
				cout << "Algorithm mask inverted." << endl;
			}
		}
		else if(choice >= 12)
		{
			cout << "ERROR: Unknown menu option!" << endl;
		}
	}
}

void LumiAlgoCreator::work_init()
{

}

void LumiAlgoCreator::work_deinit()
{

}

} // namespace DbmShell

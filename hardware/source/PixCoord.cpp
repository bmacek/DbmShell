#include "DbmShell/hardware/PixCoord.h"

using namespace std;
using namespace CatShell;

namespace DbmShell{

CAT_OBJECT_IMPLEMENT(PixCoord, CAT_NAME_PIXCOORD,CAT_TYPE_PIXCOORD);

PixCoord::PixCoord()
{
}

PixCoord::~PixCoord()
{
}
	
void PixCoord::cat_stream_out(std::ostream& output)
{
	output.write((char*)&_col, sizeof(std::uint8_t));
	output.write((char*)&_row, sizeof(std::uint16_t));
}

void PixCoord::cat_stream_in(std::istream& input)
{
	input.read((char*)&_col, sizeof(std::uint8_t));
	input.read((char*)&_row, sizeof(std::uint16_t));
}

void PixCoord::cat_print(std::ostream& output, const std::string& padding)
{
	CatObject::cat_print(output, padding);
}

void PixCoord::cat_free() 
{

}

void* PixCoord::cat_get_part(CatAddress& addr)
{
	if(addr == "column")
	{
		return &_col;
	}
	else if (addr == "row")
	{
		return &_row;
	}
	else
	{
		return nullptr;
	}
	
}
}

#include "DbmShell/hardware/LumiAlgoConfigurator.h"
#include "DbmShell/hardware/LumiAlgoLogic.h"

#include "CatShell/core/PluginDeamon.h"

using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(LumiAlgoConfigurator, CAT_NAME_LUMIALGOCONFIGURATOR, CAT_TYPE_LUMIALGOCONFIGURATOR);

LumiAlgoConfigurator::LumiAlgoConfigurator()
{
}

LumiAlgoConfigurator::~LumiAlgoConfigurator()
{
}

void LumiAlgoConfigurator::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
	// processing input data
	if(_pool && object.isSomething())
	{
		CatPointer<LumiAlgoLogic> logic = object.unsafeCast<LumiAlgoLogic>();
		CatPointer<BocRegisterCommand> reg_cmd = _pool->get_element();
		std::uint16_t tmp16;

		// resolve which Bmf we should set
		BocFpga fpga;

		if(port == _in_port_north)
			fpga = BocFpga::North;
		else if(port == _in_port_south)
			fpga = BocFpga::South;
		else
		{
			// do nothing
			ERROR(logger, "Don't know who to send commands to ?!")
			Algorithm::process_data(port, object, flush, logger);
			return;
		}

		// set the algorithm mask
		tmp16 = logic->algorithm_get_mask();
		if(tmp16 == 0)
		{
			// mask is empty so nothing to set
			// do nothing
			WARNING(logger, "No algorithm enabled. Nothing to do.")
			Algorithm::process_data(port, object, flush, logger);
			return;
		}

		LOG(logger, "Setting the algorithm mask.")
		reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_MASK_HIGH, (std::uint8_t)(tmp16 >> 8));
		publish_data(_out_port, reg_cmd, logger);
		reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_MASK_LOW, (std::uint8_t)(tmp16));
		publish_data(_out_port, reg_cmd, logger);

		LOG(logger, "Writing hit-LUT.")
		// set the values for HIT LUT
		for(uint16_t addr=0; addr<LUMI_ALGORITHM_COMBINATIONS; ++addr)
		{
			// set the address
			if((addr & 0xFF) == 0)
			{
				// this is first addres in a batch
				reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_ADDR_HIGH, (std::uint8_t)((addr | LUMI_ALGORITHM_ADDRESS_HIT_FLAG) >> 8));
				publish_data(_out_port, reg_cmd, logger);
			}
			reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_ADDR_LOW, (std::uint8_t)(addr));
			publish_data(_out_port, reg_cmd, logger);

			// send the data
			tmp16 = logic->algorithm_get_logic_hit(addr);
			reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_DATA_HIGH, (std::uint8_t)(tmp16 >> 8) & 0x0F);
			publish_data(_out_port, reg_cmd, logger);
			reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_DATA_LOW, (std::uint8_t)(tmp16));
			publish_data(_out_port, reg_cmd, logger);
		}

		LOG(logger, "Writing live-LUT.")
		// set the values for LIVE LUT
		for(uint16_t addr=0; addr<LUMI_ALGORITHM_COMBINATIONS; ++addr)
		{
			// set the address
			if((addr & 0xFF) == 0)
			{
				// this is first addres in a batch
				reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_ADDR_HIGH, (std::uint8_t)(addr >> 8));
				publish_data(_out_port, reg_cmd, logger);
			}
			reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_ADDR_LOW, (std::uint8_t)(addr));
			publish_data(_out_port, reg_cmd, logger);

			// send the data
			tmp16 = logic->algorithm_get_logic_live(addr);
			reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_DATA_HIGH, (std::uint8_t)(tmp16 >> 8) & 0x0F);
			publish_data(_out_port, reg_cmd, logger);
			reg_cmd->set_command(BocCommand::Write, fpga, BmfRegisterBank::Dbm, BOC_DBM_REGISTER_H_ALGORITHM_DATA_LOW, (std::uint8_t)(tmp16));
			publish_data(_out_port, reg_cmd, logger);
		}

		LOG(logger, "Lumi algorithms configured.")
	}

	Algorithm::process_data(port, object, flush, logger);
}

void LumiAlgoConfigurator::algorithm_init()
{
	Algorithm::algorithm_init();

	// get the memory pool
    _pool = PluginDeamon::get_pool<BocRegisterCommand>();

    // declare the input ports
    _in_port_north = declare_input_port("in_logic_north", CAT_NAME_LUMIALGOLOGIC);
    _in_port_south = declare_input_port("in_logic_south", CAT_NAME_LUMIALGOLOGIC);

    // declare the output ports
    _out_port = declare_output_port("out_register", CAT_NAME_BOCREGISTERCOMMAND);
}

void LumiAlgoConfigurator::algorithm_deinit()
{
	Algorithm::algorithm_deinit();
}

} // namespace DbmShell

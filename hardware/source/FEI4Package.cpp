#include "DbmShell/hardware/FEI4Package.h"

#include <iostream>

using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(FEI4Package, CAT_NAME_FEI4PACKAGE, CAT_TYPE_FEI4PACKAGE);

FEI4Package::FEI4Package()
{
}
	
FEI4Package::~FEI4Package()
{
}
	
void FEI4Package::cat_stream_out(std::ostream& output)
{
	std::uint32_t n;

	// number of bytes
	n = (std::uint32_t)_data.size();
	output.write((char*)&n, sizeof(std::uint32_t));
	// bytes
	for(uint32_t i=0; i<n; ++i)
	{
		output.write((char*)&_data[i], sizeof(std::uint8_t));
	}
}

void FEI4Package::cat_stream_in(std::istream& input)
{
	std::uint32_t n;

	// get the number of bytes
	input.read((char*)&n, sizeof(std::uint32_t));
	_data.resize(n);
	// get bytes
	for(uint32_t i=0; i<n; ++i)
	{
		input.read((char*)&_data[i], sizeof(std::uint8_t));
	}
}

void FEI4Package::cat_print(std::ostream& output, const std::string& padding)
{
	CatObject::cat_print(output, padding); output << " FEI4 package: ";

	for(int i=0; i< _data.size(); i++)
	{
		output << endl << "0x" << hex << (int)(_data[i]) << endl;
	}
}

void* FEI4Package::cat_get_part(CatAddress& addr)
{
	return nullptr;
}

void FEI4Package::cat_free() 
{
	clear();
}	

FEI4Package::type FEI4Package::get_package_type() const
{
	if(_type == type::none)
	{
		// first need to resolve the type
		if(_data.size() == 6 && _data[0] == FEI4_PROTOCOL_ADDRESS_RECORD && _data[3] == FEI4_PROTOCOL_VALUE_RECORD)
			_type = type::reg_read;
		else
			_type = type::unknown; 
	}

	return _type;
}

} // namespace DbmShell



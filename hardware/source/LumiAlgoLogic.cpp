#include "DbmShell/hardware/LumiAlgoLogic.h"

using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(LumiAlgoLogic, CAT_NAME_LUMIALGOLOGIC, CAT_TYPE_LUMIALGOLOGIC);

LumiAlgoLogic::LumiAlgoLogic()
: _mask(0)
{
}

LumiAlgoLogic::~LumiAlgoLogic()
{
}

void LumiAlgoLogic::cat_stream_out(std::ostream& output)
{
        output.write((char*)&_mask, sizeof(std::uint16_t));
        for(uint16_t i=0; i<LUMI_ALGORITHM_COMBINATIONS; ++i)
        {
                output.write((char*)&(_logic[i].hit), sizeof(std::uint16_t));
                output.write((char*)&(_logic[i].live), sizeof(std::uint16_t));
        }
}

void LumiAlgoLogic::cat_stream_in(std::istream& input)
{
        input.read((char*)&_mask, sizeof(std::uint16_t));
        for(uint16_t i=0; i<LUMI_ALGORITHM_COMBINATIONS; ++i)
        {
                input.read((char*)&(_logic[i].hit), sizeof(std::uint16_t));
                input.read((char*)&(_logic[i].live), sizeof(std::uint16_t));
        }
}

void LumiAlgoLogic::cat_print(std::ostream& output, const std::string& padding)
{
        char text[100];

        CatObject::cat_print(output, padding);
        output << "Luminosity algorithm logic for DBM." << endl;

        output << padding << "Mask:           "; format_flags(output, _mask, LUMI_ALGORITHM_MAPS, '@', '.'); output << "  "; format_flags(output, _mask, LUMI_ALGORITHM_MAPS, '@', '.'); output << endl;
        output << padding << "Cell            Hit logic     Live logic";
        for(std::uint16_t i=0; i<LUMI_ALGORITHM_COMBINATIONS; ++i)
        {
                output << endl;
                output << padding; 
                format_flags(output, i, LUMI_ALGORITHM_INPUTS, '1', '0');
                output << " -> ";
                format_flags(output, _logic[i].hit, LUMI_ALGORITHM_MAPS, '#', '.');
                output << "  ";
                format_flags(output, _logic[i].live, LUMI_ALGORITHM_MAPS, '#', '.');
        }
        output << endl << padding << "Mask:           "; format_flags(output, _mask, LUMI_ALGORITHM_MAPS, '@', '.'); output << "  "; format_flags(output, _mask, LUMI_ALGORITHM_MAPS, '@', '.');
}

void LumiAlgoLogic::format_flags(std::ostream& output, std::uint16_t flags, std::uint8_t width, char yes, char no)
{
        for(std::uint8_t i=0; i<width; ++i)
        {
                if((((std::uint16_t)1) << (width-i-1)) & flags)
                        output << yes;  
                else
                        output << no;
        }
}

void LumiAlgoLogic::algorithm_fill_simple(std::uint16_t algorithm, std::uint16_t data_source)
{
		std::uint16_t flag_data_source = ((std::uint16_t)1) << data_source;
		std::uint16_t flag_algorithm = ((std::uint16_t)1) << algorithm;
		std::uint16_t mask_algorithm = ~flag_algorithm;

        for(std::uint16_t i=0; i<LUMI_ALGORITHM_COMBINATIONS; ++i)
        {
        		if(i & flag_data_source)
        		{
        			// this comination has the data-source signal
        			_logic[i].hit = (_logic[i].hit & mask_algorithm) | flag_algorithm;
        			_logic[i].live = (_logic[i].live & mask_algorithm) | flag_algorithm;
        		}
        		else
        		{
        			// this combination does not have the data-source signal
        			_logic[i].hit = (_logic[i].hit & mask_algorithm);
        			_logic[i].live = (_logic[i].live & mask_algorithm);
        		}
        }
}

void LumiAlgoLogic::algorithm_enable(std::uint16_t algorithm, bool enable)
{
	std::uint16_t flag_algorithm = ((std::uint16_t)1) << algorithm;
	std::uint16_t mask_algorithm = ~flag_algorithm;

	if(enable)
		_mask |= flag_algorithm;
	else
		_mask &= mask_algorithm;
}

void LumiAlgoLogic::algorithm_invert(std::uint16_t algorithm)
{
		std::uint16_t flag_algorithm = ((std::uint16_t)1) << algorithm;
		std::uint16_t mask_algorithm = ~flag_algorithm;

        for(std::uint16_t i=0; i<LUMI_ALGORITHM_COMBINATIONS; ++i)
        {
        	if(_logic[i].hit & flag_algorithm)
        	{
        		// turn off
        		_logic[i].hit &= mask_algorithm;
        	}
        	else
        	{
        		// turn on
        		_logic[i].hit |= flag_algorithm;
        	}
        }	
}

void LumiAlgoLogic::algorithm_combine(std::uint16_t algorithm, Operation op, CatShell::CatPointer<LumiAlgoLogic> src_1, std::uint16_t alg_1, CatShell::CatPointer<LumiAlgoLogic> src_2, std::uint16_t alg_2)
{
		std::uint16_t flag_algorithm = ((std::uint16_t)1) << algorithm;
		std::uint16_t mask_algorithm = ~flag_algorithm;

		std::uint16_t flag_algorithm_1 = ((std::uint16_t)1) << alg_1;
		std::uint16_t mask_algorithm_1 = ~flag_algorithm_1;

		std::uint16_t flag_algorithm_2 = ((std::uint16_t)1) << alg_2;
		std::uint16_t mask_algorithm_2 = ~flag_algorithm_2;

		bool logic_1, logic_2, result;
        for(std::uint16_t i=0; i<LUMI_ALGORITHM_COMBINATIONS; ++i)
        {
        	// do hits

        	// get values from base algorithms
        	logic_1 = src_1->_logic[i].hit & flag_algorithm_1;
        	logic_2 = src_2->_logic[i].hit & flag_algorithm_2;

        	// calculate logic
        	switch(op)
        	{
        	case Operation::op_or:
        		result = logic_1 || logic_2;
        		break;
        	case Operation::op_and:
        		result = logic_1 && logic_2;
        		break;
        	case Operation::op_xor:
        		result = (logic_1 && (!logic_2)) || ((!logic_1) && logic_2);
        		break;
        	default:
        		result = false;
        		break;
        	};

        	// set the algorithm
        	if(result)// set
        		_logic[i].hit |= flag_algorithm;
        	else// clear
        		_logic[i].hit &= mask_algorithm;

        	// do live	

        	// get values from base algorithms
        	logic_1 = src_1->_logic[i].live & flag_algorithm_1;
        	logic_2 = src_2->_logic[i].live & flag_algorithm_2;

        	// calculate the logic
        	result = logic_1 && logic_2;
        	//result = logic_1 || logic_2; // wrong for data analysis, but done for debugging

        	// set the algorithm
        	if(result)// set
        		_logic[i].live |= flag_algorithm;
        	else// clear
        		_logic[i].live &= mask_algorithm;
		}	
}

void LumiAlgoLogic::algorithm_copy(std::uint16_t algorithm, CatShell::CatPointer<LumiAlgoLogic> source, std::uint16_t source_algorithm)
{
		std::uint16_t flag_algorithm = ((std::uint16_t)1) << algorithm;
		std::uint16_t mask_algorithm = ~flag_algorithm;

		std::uint16_t flag_algorithm_source = ((std::uint16_t)1) << source_algorithm;
		std::uint16_t mask_algorithm_source = ~flag_algorithm_source;

        for(std::uint16_t i=0; i<LUMI_ALGORITHM_COMBINATIONS; ++i)
        {
        	// do hits
        	if(source->_logic[i].hit & flag_algorithm_source)
        		_logic[i].hit |= flag_algorithm;
        	else
        		_logic[i].hit &= mask_algorithm;

        	// do live
        	if(source->_logic[i].live & flag_algorithm_source)
        		_logic[i].live |= flag_algorithm;
        	else
        		_logic[i].live &= mask_algorithm;
		}	
}

bool LumiAlgoLogic::algorithm_get_mask(std::uint16_t algorithm) const
{
	std::uint16_t flag_algorithm = ((std::uint16_t)1) << algorithm;
	return _mask & flag_algorithm;
}

void LumiAlgoLogic::algorithm_set_mask(std::uint16_t algorithm, bool mask)
{
		std::uint16_t flag_algorithm = ((std::uint16_t)1) << algorithm;
		std::uint16_t mask_algorithm = ~flag_algorithm;

		if(mask)
			_mask |= flag_algorithm;
		else
			_mask &= mask_algorithm;
}

} // namespace DbmShell

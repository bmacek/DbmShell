#include "DbmShell/hardware/FEI4Message.h"

#include <iostream>
#include <fstream>
#include <bitset>

using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(FEI4Message, CAT_NAME_FEI4MESSAGE, CAT_TYPE_FEI4MESSAGE);

FEI4Message::FEI4Message()
{
}
	
FEI4Message::~FEI4Message()
{
}

void FEI4Message::cat_stream_out(std::ostream& output)
{
	std::uint8_t tmp;

	FEI4Command::cat_stream_out(output);

    // fpga
    switch(_fpga)
    {
    case BocFpga::Bcf: tmp = 0x01; break;
    case BocFpga::North: tmp = 0x02; break;
    case BocFpga::South: tmp = 0x03; break;
    default: tmp = 0x00; break;
    };
	output.write((char*)&tmp, sizeof(std::uint8_t));
    // rx & tx
	output.write((char*)&_tx, sizeof(std::uint8_t));
	output.write((char*)&_rx, sizeof(std::uint8_t));
}

void FEI4Message::cat_stream_in(std::istream& input)
{
	std::uint8_t tmp;

	FEI4Command::cat_stream_in(input);
	
	// fpga
    input.read((char*)&tmp, sizeof(std::uint8_t));
    switch(tmp)
    {
    case 0x01: _fpga = BocFpga::Bcf; break;
    case 0x02: _fpga = BocFpga::North; break;
    case 0x03: _fpga = BocFpga::South; break;
    default: _fpga = BocFpga::Bcf; break;
    };
    // rx & tx
	input.read((char*)&_tx, sizeof(std::uint8_t));
	input.read((char*)&_rx, sizeof(std::uint8_t));
}

void FEI4Message::cat_print(std::ostream& output, const std::string& padding)
{
	FEI4Command::cat_print(output, padding);

	output << "FPGA: ";
    // fpga
    switch(_fpga)
    {
    case BocFpga::Bcf: output << "Bcf       "; break;
    case BocFpga::North: output << "Bmf north "; break;
    case BocFpga::South: output << "Bmf south "; break;
    default: output << "??        "; break;
    };
	output << std::dec << "TX: " << ((int)_tx) << "  RX: " << ((int)_rx);
}

void* FEI4Message::cat_get_part(CatAddress& addr)
{
	return FEI4Command::cat_get_part(addr);
}

void FEI4Message::cat_free() 
{
	FEI4Command::cat_free();
}	

} // namespace DbmShell



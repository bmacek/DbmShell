#include "DbmShell/hardware/FEI4.h"
#include "DbmShell/hardware/FEI4Command.h"
#include "CatShell/core/PluginDeamon.h"

using namespace CatShell;
using namespace std;

namespace DbmShell{

FEI4::FEI4()
{
}

FEI4::~FEI4()
{
}

void FEI4::load_configuration(Setting& setting)
{
	// set all cregisters to 0
	for(std::uint8_t i=0; i<FEI4_NUMER_OF_REGISTERS; ++i)
	{
		_register[i] = 0x0000;
	}

	// go through all the settings and load register settings
	for(auto& s: setting["register_map"])
	{
		if(s.name() == "register")
		{
			_register[s.get_setting<std::uint8_t>("address", FEI4_NUMER_OF_REGISTERS)] = s.get_setting<std::uint16_t>("value", 0x0000);
		}
	}

	// chip ID
	_id = setting.get_setting<std::uint8_t>("chip_id", FEI4_ID_BRODCAST) & FEI4_ID_MASK;
}

void FEI4::default_configuration()
{
	// set all cregisters to 0
	for(std::uint8_t i=0; i<FEI4_NUMER_OF_REGISTERS; ++i)
	{
		_register[i] = 0x0000;
	}

	// chip ID
	_id = FEI4_ID_BRODCAST;

}

void FEI4::create_cmd_write(std::uint8_t reg, std::uint16_t value, CatShell::CatPointer<FEI4Command>& cmd)
{
	cmd->define_as_WrCommand(_id, reg, value);
	_register[reg] = value;
}

void FEI4::create_cmd_write(std::uint8_t reg, std::uint16_t value, std::uint16_t mask, CatShell::CatPointer<FEI4Command>& cmd)
{
	_register[reg] &= (~mask);
	_register[reg] |= (value & mask);
	cmd->define_as_WrCommand(_id, reg, _register[reg]);
	//cout << "WR: " << reg << " => " << _register[reg] << endl;
}

}

#include "DbmShell/hardware/FEI4ToBocRegister.h"
#include "CatShell/core/PluginDeamon.h"
#include "DbmShell/hardware/FEI4Command.h"
#include "DbmShell/hardware/BocRegisterCommand.h"
#include <bitset>


using namespace CatShell;
using namespace std;

namespace DbmShell{

CAT_OBJECT_IMPLEMENT(FEI4ToBocRegister, CAT_NAME_FEI4TOBOCREGISTER,CAT_TYPE_FEI4TOBOCREGISTER);

FEI4ToBocRegister::FEI4ToBocRegister()
{
}

FEI4ToBocRegister::~FEI4ToBocRegister()
{
}

void FEI4ToBocRegister::cat_stream_out(std::ostream& output)
{
      
}

void FEI4ToBocRegister::cat_stream_in(std::istream& input)
{

}

void FEI4ToBocRegister::cat_print(std::ostream& output, const std::string& padding)
{

}

void* FEI4ToBocRegister::cat_get_part(CatAddress& addr)
{
	return nullptr;
}

void FEI4ToBocRegister::cat_free() 
{
	
}

void FEI4ToBocRegister::algorithm_init()
{
	Algorithm::algorithm_init();
	_in_port_object = declare_input_port("InBocReg", CAT_NAME_FEI4COMMAND);	
	_pix_out = declare_output_port("ToBocReg", CAT_NAME_BOCREGISTERCOMMAND);
}

void FEI4ToBocRegister::algorithm_deinit()
{
	Algorithm::algorithm_deinit();
}

void FEI4ToBocRegister::process_data(CatShell::Algorithm::DataPort port, CatShell::CatPointer<CatShell::CatObject>& object,bool flush, CatShell::Logger&logger)
{
	CatShell::MemoryPool<BocRegisterCommand>*  _pool = PluginDeamon::get_pool<BocRegisterCommand>();
	CatPointer<BocRegisterCommand> command;

	if(object.isSomething())
	{
		CatPointer<FEI4Command> p = object.unsafeCast<FEI4Command>();
		
		_length = p->size();
		for(int i=0; i < _length; ++i)
		{
			command = _pool->get_element();
			command->set_command(BocCommand::Write, BocFpga::North , BmfRegisterBank::Tx, 3, p->data_element(i));	
			publish_data(_pix_out,command,logger);	
		}
	}
	Algorithm::process_data(port, object, flush, logger);
}

}

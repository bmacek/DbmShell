#include "DbmShell/hardware/FEI4Configurator.h"
#include "DbmShell/hardware/FEI4Command.h"
#include "DbmShell/hardware/PixCoord.h"
#include "CatShell/core/PluginDeamon.h"



using namespace CatShell;
using namespace std;

namespace DbmShell{

CAT_OBJECT_IMPLEMENT(FEI4Configurator, CAT_NAME_FEI4CONFIGURATOR,CAT_TYPE_FEI4CONFIGURATOR);

FEI4Configurator::FEI4Configurator()
{
}

FEI4Configurator::~FEI4Configurator()
{
}

void FEI4Configurator::cat_stream_out(std::ostream& output)
{
      
}

void FEI4Configurator::cat_stream_in(std::istream& input)
{

}

void FEI4Configurator::cat_print(std::ostream& output, const std::string& padding)
{

}

void* FEI4Configurator::cat_get_part(CatAddress& addr)
{
	return nullptr;
}

void FEI4Configurator::cat_free() 
{
	pixel_list.clear();
}

void FEI4Configurator::algorithm_init()
{
	Algorithm::algorithm_init();
	_in_port_object = declare_input_port("hitor_mask", CAT_NAME_PIXCOORD);
	_pix_out = declare_output_port("FE_Command", CAT_NAME_FEI4COMMAND);
}

void FEI4Configurator::algorithm_deinit()
{
	Algorithm::algorithm_deinit();
}

uint8_t FEI4Configurator::ReverseBits(int s,uint8_t input)
{
	uint8_t output = input;
	for (int i = sizeof(input) * s-1; i; --i)
	{
		output <<= 1;
		input  >>= 1;
		output |=  input & 1;
	}
	return output;
}

void FEI4Configurator::set_double_column(uint8_t chipID, std::vector<CatShell::CatPointer<PixCoord>>& pixel_list, int double_col, CatShell::Logger& logger )
{
	uint8_t Colpr_Addr;                             //address of double column, and Address is the address of register used
	uint8_t Addr_Colpr;
	int dc_bit,osem;                                // double column bit, from 0 to 672             
	int dc_number;                                  // double column number, from 0 to 39
	int rest;
	uint8_t double_column [40][84];
	uint8_t data;
	uint8_t data_c[84];

	uint8_t width;
	uint16_t config_table[36]={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

	// the variables for WrCommand function for WrRegister 22
	uint16_t CP0, CP1;
	CP0=0;
	CP1=256;
	///////////////////////////////////////////
	// the variables for WrCommand function for WrRegister 13               
	uint16_t S0,S1;
	uint16_t Pixel_latch_strobe;
	S0 = 0;
	S1 = 0;
	Pixel_latch_strobe = 0;         
	///////////////////////////////////////////
	// the variables for WrCommand function for WrRegister 21
	uint16_t HLD, DJO, DHS;
	uint16_t PlsrDAC;
	HLD =0;
	DJO = 0;
	DHS = 1024;
	PlsrDAC =0;                     
	///////////////////////////////////////////
	// the variables for WrCommand function for WrRegister 27
	uint16_t PLL, EFS, STP, RER, ADC, SRR, HOR, CAL, SRC, LEN, SRK, M13;
	PLL=0;
	EFS=0;
	STP=0;
	RER=0;
	ADC=0;
	SRR=0;
	HOR=0;
	CAL=0;
	SRC=0;
	LEN=0;
	SRK=0;
	M13=0;

	for (int c=0; c<40; ++c)
	{
	        for (int d=1; d<85; ++d)
	        {
	                double_column[c][d] =0;
	                data_c[d] =0;
	        }
	}

	for (std::vector<CatPointer<PixCoord>>::iterator ite=pixel_list.begin(); ite<pixel_list.end(); ++ite )
	{
		if(CatPointer<PixCoord>(*ite)->column() == 1 || CatPointer<PixCoord>(*ite)->column() ==2)
        	{
            		Colpr_Addr=0;
			dc_number =0;
		}
	
        	if(CatPointer<PixCoord>(*ite)->column()%2!=0 && CatPointer<PixCoord>(*ite)->column() != 1 && CatPointer<PixCoord>(*ite)->column() !=2)
        	{
        		Colpr_Addr= CatPointer<PixCoord>(*ite)->column()/2;
			dc_number = CatPointer<PixCoord>(*ite)->column()/2;
		}

        	if(CatPointer<PixCoord>(*ite)->column()%2 ==0 && CatPointer<PixCoord>(*ite)->column() != 2)
        	{
        		Colpr_Addr= CatPointer<PixCoord>(*ite)->column()/2-1;
			dc_number = CatPointer<PixCoord>(*ite)->column()/2-1;
		}
        
        	if(CatPointer<PixCoord>(*ite)->column() %2 != 0 )
        	{
        		dc_bit = 335+ CatPointer<PixCoord>(*ite)->row();
			osem = (dc_bit)/8;       
	 	}
        	else
        	{
        		dc_bit = 336- CatPointer<PixCoord>(*ite)->row();
			osem = (dc_bit)/8;        	
		}

		rest = (dc_bit)%8;
		uint8_t fill_bit[8]= {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};
		double_column [dc_number][osem] = fill_bit[rest] | double_column[dc_number][osem];

		if (dc_number == double_col)
		{
			data_c[osem] = double_column[dc_number][osem];
		}
	}

	// register 21
	config_table[21] = config_table[21] & 0xE000;
	HLD = HLD & 0x1000;
	DJO = DJO & 0x800;
	DHS = DHS & 0x400;
	PlsrDAC = ReverseBits(10,PlsrDAC);
	config_table[21] = HLD | DJO | DHS | PlsrDAC;

	// register 22
	config_table[22] = config_table[22] & 0xFC03;
	Addr_Colpr = ReverseBits(8,double_col);
	CP1 = CP1 & 0x100;
	Addr_Colpr = Addr_Colpr & 0xfc;
	CP0 = CP0 & 0x200;
	config_table[22] = CP0 | CP1 | Addr_Colpr;			
	//-------------------------------------------------------

	// register 27
	config_table[27] = config_table[27] & 0x9c0;
	PLL = PLL & 0x8000;
	EFS = EFS & 0x4000;
	STP = STP & 0x2000;
	RER = RER & 0x1000;
	ADC = ADC & 0x0400;
	SRR = SRR & 0x0200;
	HOR = HOR & 0x0020;
	CAL = CAL & 0x0010;
	SRC = SRC & 0x0008;
	LEN = LEN & 0x0004;
	SRK = SRK & 0x0002;
	M13 = M13 & 0x0001;
	config_table[27] = PLL | EFS | STP | RER | ADC | SRR | HOR | CAL | SRC | LEN | SRK | M13 ;
	//-------------------------------------------------------	
			
	// register 13
	config_table[13] = config_table[13] & 0x01;
	S1 = S1 & 0x8000;
	S0 = S0 & 0x4000;
	Pixel_latch_strobe = Pixel_latch_strobe & 0x3FFE;
	config_table[13] = S1 | S0 | Pixel_latch_strobe ;
	//-------------------------------------------------------

	CatShell::MemoryPool<FEI4Command>*  _pool = PluginDeamon::get_pool<FEI4Command>();
	CatPointer<FEI4Command> cmd;

	// commands to load an arbitrary bit stream to the shift register
	cmd = _pool->get_element();
	cmd->define_as_WrCommand(chipID,22,config_table[22]);
	publish_data(_pix_out,cmd,logger);

	cmd = _pool->get_element();
	cmd->define_as_WrCommand(chipID,13,config_table[13]);
	publish_data(_pix_out,cmd,logger);

	cmd = _pool->get_element();
	cmd->define_as_WrCommand(chipID,21,config_table[21]);
	publish_data(_pix_out,cmd,logger);

	cmd = _pool->get_element();
	cmd->define_as_WrCommand(chipID,27,config_table[27]);
	publish_data(_pix_out,cmd,logger);

	cmd = _pool->get_element();
	cmd->define_as_WrFrontEnd(chipID, &data_c[0]);
	publish_data(_pix_out,cmd,logger);				

	// commands to load the shift register content to the pixel latches
	cmd = _pool->get_element();
	cmd->define_as_WrCommand(chipID,22,config_table[22]);
	publish_data(_pix_out,cmd,logger);

	cmd = _pool->get_element();
	cmd->define_as_WrCommand(chipID,13,config_table[13]);
	publish_data(_pix_out,cmd,logger);

	cmd = _pool->get_element();
	cmd->define_as_WrCommand(chipID,21,config_table[21]);
	publish_data(_pix_out,cmd,logger);

	cmd = _pool->get_element();
	cmd->define_as_WrCommand(chipID,27,config_table[27]);
	publish_data(_pix_out,cmd,logger);

	cmd = _pool->get_element();
	cmd->define_as_GlobalPulse(chipID, width);
	publish_data(_pix_out,cmd,logger);

	cmd = _pool->get_element();
	cmd->define_as_WrCommand(chipID,13,config_table[13]);
	publish_data(_pix_out,cmd,logger);
}	

void FEI4Configurator::process_data(CatShell::Algorithm::DataPort port, CatShell::CatPointer<CatShell::CatObject>& object,bool flush, CatShell::Logger&logger)
{
	if(object.isSomething())
	{
		CatPointer<PixCoord> p = object.unsafeCast<PixCoord>();
		pixel_list.push_back(p);
	}	
	if(flush == true)
	{
		set_double_column(6, pixel_list,0,logger);
		set_double_column(6, pixel_list,5,logger);
		set_double_column(6, pixel_list,33,logger);
	}
	Algorithm::process_data(port, object, flush, logger);
}
}

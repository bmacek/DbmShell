#include "DbmShell/hardware/RegisterToBocLib.h"

#include <bitset>

#include <sstream>

using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(RegisterToBocLib, CAT_NAME_REGISTERTOBOCLIB, CAT_TYPE_REGISTERTOBOCLIB);

RegisterToBocLib::RegisterToBocLib()
: _file(nullptr)
{
}

RegisterToBocLib::~RegisterToBocLib()
{
}

void RegisterToBocLib::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
	// processing input data
	if(object.isSomething())
	{
		CatPointer<BocRegisterCommand> cmd = object.unsafeCast<BocRegisterCommand>();

		std::stringstream buff;

        switch(cmd->get_command_type())
        {
        case BocCommand::Read: buff << "R "; break;
        case BocCommand::Write: buff << "W "; break;
        default:
        	ERROR(logger, "Unknown BOC command.")
        	Algorithm::process_data(port, object, flush, logger);
        	return;
        }

        switch(cmd->get_fpga())
        {
        case BocFpga::Bcf: buff << "B "; break;
        case BocFpga::North: buff << "N "; break;
        case BocFpga::South: buff << "S "; break;
        default:
        	ERROR(logger, "Unknown FPGA.")
        	Algorithm::process_data(port, object, flush, logger);
        	return;
        }

        switch(cmd->get_bank())
        {
        case BmfRegisterBank::General: buff << "G "; break;
        case BmfRegisterBank::Rx: buff << "R "; break;
        case BmfRegisterBank::Tx: buff << "T "; break;
        case BmfRegisterBank::Dbm: buff << "D "; break;
        default:
        	ERROR(logger, "Unknown register banck.")
        	Algorithm::process_data(port, object, flush, logger);
        	return;
        }

        buff << "0x" << hex << cmd->get_register();
        if(cmd->get_command_type() == BocCommand::Write)
		{
			buff << " 0x" << hex << (unsigned int)cmd->getData();
		}

		// put it into file
		(*_file) << buff.str() << endl;
	}

	Algorithm::process_data(port, object, flush, logger);
}

void RegisterToBocLib::algorithm_init()
{
	Algorithm::algorithm_init();
   
	// declare the input ports
	_in_port = declare_input_port("in_command", CAT_NAME_BOCREGISTERCOMMAND);
	_file_name = algo_settings().get_setting<std::string>("file_name", "LumiAlgo.command");
	_file = new ofstream(_file_name.c_str());

	if(!_file->is_open())
	{
		delete _file;
		_file = nullptr;
	}
}

void RegisterToBocLib::algorithm_deinit()
{
	if(_file)
	{
		_file->close();
		delete _file;
		_file = nullptr;
	}

	Algorithm::algorithm_deinit();
}


} // namespace DbmShell

#include "DbmShell/hardware/FEI4Command.h"

#include <iostream>
#include <fstream>
#include <bitset>
#include <math.h>

using namespace std;
using namespace CatShell;

namespace DbmShell {

std::uint8_t FEI4Command::s_data_table [8][8]= 
{{0x00,0x01,0x03,0x07,0x0f,0x1f,0x3f,0x7f},
 {0x00,0x02,0x06,0x0e,0x1e,0x3e,0x7e,0xfe},
 {0x00,0x04,0x0c,0x1c,0x3c,0x7c,0xfc,0x00},
 {0x00,0x08,0x18,0x38,0x78,0xf8,0x00,0x00},
 {0x00,0x10,0x30,0x70,0xf8,0x00,0x00,0x00},
 {0x00,0x20,0x60,0xe0,0x00,0x00,0x00,0x00},
 {0x00,0x40,0xc0,0x00,0x00,0x00,0x00,0x00},
 {0x00,0x80,0x00,0x00,0x00,0x00,0x00,0x00}};
std::uint8_t FEI4Command::s_mask_vector[8] = {0xfe,0xfc,0xf8,0xf0,0xe0,0xc0,0x80,0x00};
std::uint8_t FEI4Command::s_mask_vector_2[9] = {0xff,0x01,0x03,0x07,0x1f,0x3f,0x7f,0xff,0xff};
//std::uint8_t FEI4Command::s_mask_vector_2[8] = {0x80,0xc0,0xe0,0xf0,0xf8,0xfc,0xfe,0xff};

CAT_OBJECT_IMPLEMENT(FEI4Command, CAT_NAME_FEI4COMMAND, CAT_TYPE_FEI4COMMAND);

FEI4Command::FEI4Command()
{
	_bit = -1;
	_type = type::None;
}
	
FEI4Command::~FEI4Command()
{
}
	
void FEI4Command::cat_stream_out(std::ostream& output)
{
	std::uint32_t n;

	// type
	output.write((char*)&_type, sizeof(std::uint8_t));
	// number of bytes
	n = (std::uint32_t)_data.size();
	output.write((char*)&n, sizeof(std::uint32_t));
	// bytes
	for(uint32_t i=0; i<n; ++i)
	{
		output.write((char*)&_data[i], sizeof(std::uint8_t));
	}
}

void FEI4Command::cat_stream_in(std::istream& input)
{
	std::uint32_t n;

	// type
	input.read((char*)&_type, sizeof(std::uint8_t));
	// get the number of bytes
	input.read((char*)&n, sizeof(std::uint32_t));
	_data.resize(n);
	// get bytes
	for(uint32_t i=0; i<n; ++i)
	{
		input.read((char*)&_data[i], sizeof(std::uint8_t));
	}
}

void FEI4Command::cat_print(std::ostream& output, const std::string& padding)
{
	CatObject::cat_print(output, padding);

	for(int i=0; i< _data.size(); i++)
	{
		output << bitset<8>(_data[i]) << endl; 
	}
}

void* FEI4Command::cat_get_part(CatAddress& addr)
{
	return nullptr;
}

void FEI4Command::cat_free() 
{
	_data.clear();
	_bit = -1;
	_type = type::None;
}	

void FEI4Command::add_bits(uint8_t data, int length)
{
	uint8_t a,b,f;

	//printf("* 0x%02x %u %i\n", data, length, _bit);

	if (_bit == -1)
	{
		_data.push_back(0);			
		_bit = 7;				
	}	

	if (_bit < length-1)
	{	
		a = data >> (length-_bit-1); 
		f = _data.back() & s_mask_vector[_bit];	
		if (a == 1)
		{
			a = a & 0x01;
		}
		else
		{
			a = a & s_data_table[(length-_bit-2)][(length-1)];   
		}
		_data.back() = f | a;							
		_data.push_back(0);
		length = length - _bit - 1;
		_bit = 7;
	}

	a = data << (_bit-length+1);
	a = a & s_mask_vector_2[_bit+1]; 
	b = _data.back() & s_mask_vector[_bit];	
	//printf(" 0x%02x\n", a);
	_data.back() = b | a;       		 				
	_bit = _bit - length;

	//cat_print(std::cout, "");
}
		
void FEI4Command::define_as_WrCommand(uint8_t chipID, uint8_t Address, uint16_t data)
{
	cat_free();
	add_bits(0x00, 8);
	add_bits(0x00, 1);
	add_bits(FEI4_CMD_SLOW_1);
	add_bits(FEI4_CMD_SLOW_2);
	add_bits(FEI4_CMD_WR_REG);	
	add_bits(chipID,4);
	add_bits(Address,6);
	add_bits((std::uint8_t)(data>>8),8);
	add_bits((std::uint8_t)(data),8);
	add_bits(0x00, 8);
	add_bits(0x00, 8);
	_type = type::WrReg;
}

void FEI4Command::define_as_RdCommand(uint8_t chipID, uint8_t Address)
{
	cat_free();
	add_bits(0x00, 8);
	add_bits(0x00, 1);
	add_bits(FEI4_CMD_SLOW_1);
	add_bits(FEI4_CMD_SLOW_2);
	add_bits(FEI4_CMD_RD_REG);	
	add_bits(chipID,4);
	add_bits(Address,6);
	add_bits(0x00, 8);
	add_bits(0x00, 8);
	_type = type::RdReg;
}

void FEI4Command::define_as_WrFrontEnd(uint8_t chipID, uint8_t *data_c)
{
	cat_free();
	add_bits(0x00, 8);
	add_bits(0x00, 1);
	add_bits(FEI4_CMD_SLOW_1);
	add_bits(FEI4_CMD_SLOW_2);
	add_bits(FEI4_CMD_WR_FE);
	add_bits(chipID,4);
	add_bits(0x00,6);
	for (int i=0; i <84; ++i)
	{
		add_bits(data_c[i],8);
	}		
	add_bits(0x00, 8);
	add_bits(0x00, 8);
	_type = type::WrFe;
}

void FEI4Command::define_as_GlobalPulse(uint8_t chipID, uint8_t width)
{
	cat_free();
	add_bits(0x00, 8);
	add_bits(0x00, 1);
	add_bits(FEI4_CMD_SLOW_1);
	add_bits(FEI4_CMD_SLOW_2);
	add_bits(FEI4_CMD_G_PULSE);
	add_bits(chipID,4);
	add_bits(width,6);
	add_bits(0x00, 8);
	add_bits(0x00, 8);
	_type = type::GlPulse;
}

void FEI4Command::define_as_Mode(std::uint8_t chipID, std::uint8_t mode)
{
	cat_free();
	add_bits(0x00, 8);
	add_bits(0x00, 1);
	add_bits(FEI4_CMD_SLOW_1);
	add_bits(FEI4_CMD_SLOW_2);
	add_bits(FEI4_CMD_MODE);
	add_bits(chipID,4);
	add_bits(mode,6);
	add_bits(0x00, 8);
	add_bits(0x00, 8);
	add_bits(0x00, 8);
	add_bits(0x00, 8);
	_type = type::Mode;
}

void FEI4Command::define_as_Reset(std::uint8_t chipID)
{
	cat_free();
	add_bits(0x00, 8);
	add_bits(0x00, 1);
	add_bits(FEI4_CMD_SLOW_1);
	add_bits(FEI4_CMD_SLOW_2);
	add_bits(FEI4_CMD_RESET);
	add_bits(chipID,4);
	add_bits(0x00, 8);
	add_bits(0x00, 8);
	add_bits(0x00, 8);
	add_bits(0x00, 8);
	_type = type::Reset;
}


std::uint32_t FEI4Command::get_bits(uint32_t start, uint32_t end) const
{
	if(end>=start && end<=start+32)
	{
		uint32_t j=(int) start/8;
		uint32_t k=(int) end/8;
		uint64_t value=0;
		uint64_t mask = pow(2,(k-j+1)*8)-1;
		
		for(uint32_t i=j; i<k+1; i++)
		{
			value += _data[i];
			
			if(i<k) value = value<<8;		
		}
		//printf("%d,%d\n\n",value,mask);
		mask = mask >> (1+k)*8-end-1+(start-8*j);
		value = value >> (1+k)*8-end-1;
		//printf("%d,%d,%d\n\n",value,mask,value&mask);
		return (uint32_t)(value & mask);
		
		
	}
	else{ return 0; }
}

std::uint32_t FEI4Command::get_first() const
{
	uint32_t a,b;
	uint32_t mask;
	uint32_t value;

	for(uint32_t j=0; j<_data.size(); j++)
	{
		for(uint32_t i=0; i<8; i++)
		{
			mask = 255;
			mask = mask >> 7-i;
			value = _data[j];
			value = value >> 7-i;

			if(value&mask != 0)
			{
				a=j;
				b=i+1;
				break;
			}
		}
		if(value&mask != 0) break;
	}
	
	return (8*a+b);
}

} // namespace DbmShell



#ifndef INCLUDE_FEI4CONFIGURATOR
#define INCLUDE_FEI4CONFIGURATOR

#include "CatShell/core/Algorithm.h"
#include "DbmShell/hardware/defines.h"
#include "CatShell/core/Logger.h"
#include "CatShell/core/WorkThread.h"
#include "DbmShell/hardware/PixCoord.h"
#include "DbmShell/hardware/FEI4Command.h"
#include <iostream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <string>
#include <bitset>

//using namespace CatShell;

namespace DbmShell{

class FEI4Configurator: public CatShell::Algorithm
{
public:
	FEI4Configurator();

	virtual ~FEI4Configurator();

	CAT_OBJECT_DECLARE(FEI4Configurator, CatShell::Algorithm, CAT_NAME_FEI4CONFIGURATOR,CAT_TYPE_FEI4CONFIGURATOR);

	//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
	//----------------------------------------------------------------------
	virtual void* cat_get_part(CatShell::CatAddress& addr);
		///Returns the pointer to the desired part(element).


	// ALGORITHM
	virtual void process_data(CatShell::Algorithm::DataPort port, CatShell::CatPointer<CatShell::CatObject>& object,bool flush, CatShell::Logger&logger);
		/// Process data.
	virtual void algorithm_init();
		/// Defines the process interface and initialises it.
	virtual void algorithm_deinit();
		/// Clears all the process interfaces.
	//virtual CatShell::CatPointer<CatShell::CatObject> produce_data(CatShell::Algorithm::DataPort port, bool& flush, CatShell::Logger&logger);
		/// Produces data on request.Second parameter that is returned if a flush flag.
		/// Object is taken from the first one that producess a result.

	//----------------------------------------------
	
private:
	DataPort	_in_port_object;
	void set_double_column(uint8_t chipID, std::vector<CatShell::CatPointer<PixCoord>>& pixel_list, int double_col, CatShell::Logger& logger);
	std::vector<CatShell::CatPointer<PixCoord>> pixel_list;
	static uint8_t ReverseBits(int s,uint8_t input);
	DataPort 	_pix_out;
};

inline StreamSize FEI4Configurator::cat_stream_get_size() const { return FEI4Configurator::cat_stream_get_size(); }

}
#endif

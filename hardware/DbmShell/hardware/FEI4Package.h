#ifndef INCLUDE_FEI4PACKAGE
#define INCLUDE_FEI4PACKAGE

#include "CatShell/core/CatObject.h"
#include "DbmShell/hardware/defines.h"

#include <vector>

namespace DbmShell{

class FEI4Package: public CatShell::CatObject
{
	CAT_OBJECT_DECLARE(FEI4Package, CatShell::CatObject, CAT_NAME_FEI4PACKAGE, CAT_TYPE_FEI4PACKAGE);

public:
        enum class type {none = 0, unknown = 1, reg_read = 2};
                /// Enum type, listing the type of package response.

        FEI4Package();
                /// Constructor: default.

        virtual ~FEI4Package();
                /// Destructor.

        //------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

        virtual void* cat_get_part(CatShell::CatAddress& addr);
                ///Returns the pointer to the desired part(element).
        //----------------------------------------------------------------------


        void clear();
                /// Removes all the data from the package.

        void add_data(const std::uint8_t x);
                /// Adds byte in the package.

        type get_package_type() const;
                /// Returns the type of data.

        // 
        // register read fnctions
        //

        std::uint16_t rr_get_address() const;
                /// Returns the address of the register read.

        std::uint16_t rr_get_value() const;
                /// Returns the value of the register read.

private:

        std::vector<std::uint8_t> _data;
        mutable type              _type;
};

inline StreamSize FEI4Package::cat_stream_get_size() const { return _data.size() + sizeof(std::uint32_t); }
inline void FEI4Package::clear() { _data.clear(); _type = type::none; }
inline void FEI4Package::add_data(const std::uint8_t x) { _data.push_back(x); }
inline std::uint16_t FEI4Package::rr_get_address() const { return (((std::uint16_t)(_data[1])) << 8) | (_data[2]); }
inline std::uint16_t FEI4Package::rr_get_value() const { return (((std::uint16_t)(_data[4])) << 8) | (_data[5]); }



} // namespace DbmShell

#endif

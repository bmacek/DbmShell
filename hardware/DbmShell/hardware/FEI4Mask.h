#ifndef INCLUDE_FEI4MASK
#define INCLUDE_FEI4MASK

#include "CatShell/core/CatObject.h"
#include "DbmShell/hardware/defines.h"


namespace DbmShell{

class FEI4Mask : public CatShell::CatObject
/// Starting address is col=1, row=1.
{
public:

	CAT_OBJECT_DECLARE(FEI4Mask, CatShell::CatObject, CAT_NAME_FEI4MASK, CAT_TYPE_FEI4MASK);

	FEI4Mask();
		/// Constructor: default.

	virtual ~FEI4Mask();
		/// Destructor.

	//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

		virtual void* cat_get_part(CatShell::CatAddress& addr);
			///Returns the pointer to the desired part(element).
	//----------------------------------------------------------------------

	void set_all(bool value);
		/// Sets all bits to the desired value.

	void set_bit(std::uint8_t col, std::uint16_t row, bool value);
		/// Sets the desired bit.

	void invert();
		/// Inverts all the bits in the mask.

	void fill_fe_stream(std::uint8_t col, std::uint8_t* mask);
		/// Fills the 'mask' stream with the bit for FE configuration for column 'col' and 'col+1'.

	bool load_from_file(const std::string& file_name);
		/// Loads the mask from the file. Returns success.

private:

	std::uint8_t _mask[FEI4_COLS][FEI4_ROWS/8]; // column, row

	// statics
	static std::uint8_t s_mask_table[8];
	static std::uint8_t s_inverse_table[256];
};

inline StreamSize FEI4Mask::cat_stream_get_size() const { return 80*42*sizeof(std::uint8_t); }

}

#endif

#ifndef INCLUDE_DBMDBMCONSOLE_INCLUDED
#define INCLUDE_DBMDBMCONSOLE_INCLUDED

#include "CatShell/core/Process.h"
#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/CatFile.h"

#include "DbmShell/hardware/defines.h"
#include "DbmShell/hardware/BocRegisterCommand.h"
#include "DbmShell/hardware/FEI4Message.h"
#include "DbmShell/hardware/FEI4.h"
#include "DbmShell/hardware/FEI4Mask.h"

namespace DbmShell {

class DbmConsole : public CatShell::Process
{
        CAT_OBJECT_DECLARE(DbmConsole, CatShell::Process, CAT_NAME_DBMCONSOLE, CAT_TYPE_DBMCONSOLE);
public:

        DbmConsole();
                /// Constructor: default.

        virtual ~DbmConsole();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main();
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init();
                /// Called before the thread starts executing.

        virtual void work_deinit();
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatShell::CatPointer<CatShell::WorkThread> child);
                /// Called when a subprocess finished.
//----------------------------------------------------------------------

protected:

private:

        bool execute_command(const char* command);
                /// Processes single command. Returns 'true' if program should continue.

        static void cmd_to_vector(const char* command, std::vector<std::string>& list);
                /// Returns the next parameter from command line and stores it into vector format.

        static void display_help();
                /// Prints the help information to the standard input.

private:

        typedef struct 
        {
                BocFpga fpga;
                std::uint8_t tx;
                std::uint8_t rx;
        } tMapping;

        enum class reply_t { none, read_register, laser_satus, read_fe_register, h_stream_enable, h_stream_reset, dbm_slink_reset, ibl_slink_enable, enable_h_rx, enable_h_generator, hb_pattern };

        reply_t _reply;

        CatShell::Algorithm::DataPort   _in_boc_read;
        CatShell::Algorithm::DataPort   _in_fe_read;
        CatShell::Algorithm::DataPort   _out_boc_command;
        CatShell::Algorithm::DataPort   _out_port_fe;
        CatShell::Algorithm::DataPort   _out_port_fe_broadcast;

        CatShell::MemoryPool<BocRegisterCommand>*  _pool_boc_command; 
        CatShell::MemoryPool<FEI4Message>*  _pool_fe_command; 
        CatShell::MemoryPool<FEI4Mask>*     _pool_fe_mask; 

        bool _silent;

        // FE entities
        FEI4*    _fe[FEI4_PER_BOC];
        tMapping _mapping[FEI4_PER_BOC];

        // hold data
        std::uint8_t _hold_channel;
        bool         _hold_h_on;
        std::uint8_t _hold_h_ch;
};

inline StreamSize DbmConsole::cat_stream_get_size() const { return CatShell::Process::cat_stream_get_size(); }
inline void DbmConsole::cat_stream_out(std::ostream& output) { CatShell::Process::cat_stream_out(output); }
inline void DbmConsole::cat_stream_in(std::istream& input) { CatShell::Process::cat_stream_in(input); }
inline void DbmConsole::cat_print(std::ostream& output, const std::string& padding) { CatShell::Process::cat_print(output, padding); }
inline void DbmConsole::work_subthread_finished(CatShell::CatPointer<CatShell::WorkThread> child) {}
inline void DbmConsole::cat_free() { }

} // namespace DbmShell

#endif

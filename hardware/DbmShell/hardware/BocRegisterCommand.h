#ifndef INCLUDE_BOCREGISTERCOMMAND
#define INCLUDE_BOCREGISTERCOMMAND

#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"


#include "DbmShell/hardware/defines.h"





namespace DbmShell {

class BocRegisterCommand : public CatShell::CatObject
{
        CAT_OBJECT_DECLARE(BocRegisterCommand, CatShell::CatObject, CAT_NAME_BOCREGISTERCOMMAND, CAT_TYPE_BOCREGISTERCOMMAND);
public:

        BocRegisterCommand();
                /// Constructor: default.

        virtual ~BocRegisterCommand();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        void set_command(BocCommand cmd, BocFpga fpga, BmfRegisterBank bank, std::uint16_t reg, std::uint8_t data = 0);
                /// Sets the command.

        BocCommand get_command_type() const;
                /// Returns the type of the command.

        BocFpga get_fpga() const;
                /// Returns FPGA to which the command is directed to.

        BmfRegisterBank get_bank() const;
                /// Returns the register bank within the FPGA.

        std::uint16_t get_register() const;
                /// Return the register number.

        std::uint8_t getData() const;
                /// Return the data;

protected:

private:

        BocCommand      _command;
        BocFpga         _fpga;
        BmfRegisterBank _bank;
        std::uint16_t   _register;
        std::uint8_t    _data;
};

inline StreamSize BocRegisterCommand::cat_stream_get_size() const { return 4*sizeof(std::uint16_t) + sizeof(std::uint16_t); }
inline void BocRegisterCommand::cat_free() {}
inline void BocRegisterCommand::set_command(BocCommand cmd, BocFpga fpga, BmfRegisterBank bank, std::uint16_t reg, std::uint8_t data) { _command = cmd; _fpga = fpga; _bank = bank; _register = reg; _data = data; } ;
inline BocCommand BocRegisterCommand::get_command_type() const { return _command; }
inline BocFpga BocRegisterCommand::get_fpga() const { return _fpga; }
inline BmfRegisterBank BocRegisterCommand::get_bank() const { return _bank; }
inline std::uint16_t BocRegisterCommand::get_register() const { return _register; }
inline std::uint8_t BocRegisterCommand::getData() const { return _data; }

} // namespace DbmShell

#endif


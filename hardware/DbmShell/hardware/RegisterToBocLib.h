#ifndef INCLUDE_DBMREGISTERTOBOCLIB_INCLUDED
#define INCLUDE_DBMREGISTERTOBOCLIB_INCLUDED

#include "CatShell/core/Algorithm.h"

#include "DbmShell/hardware/defines.h"
#include "DbmShell/hardware/BocRegisterCommand.h"

#include <iostream>
#include <fstream>

namespace DbmShell {

class RegisterToBocLib : public CatShell::Algorithm
{
        CAT_OBJECT_DECLARE(RegisterToBocLib, CatShell::Algorithm, CAT_NAME_REGISTERTOBOCLIB, CAT_TYPE_REGISTERTOBOCLIB);
public:

        RegisterToBocLib();
                /// Constructor: default.

        virtual ~RegisterToBocLib();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:

private:

        std::string     _file_name;
        std::ofstream*  _file;

        CatShell::Algorithm::DataPort    _in_port;
};

inline StreamSize RegisterToBocLib::cat_stream_get_size() const { return CatShell::Algorithm::cat_stream_get_size(); }
inline void RegisterToBocLib::cat_stream_out(std::ostream& output) { CatShell::Algorithm::cat_stream_out(output); }
inline void RegisterToBocLib::cat_stream_in(std::istream& input) { CatShell::Algorithm::cat_stream_in(input); }
inline void RegisterToBocLib::cat_print(std::ostream& output, const std::string& padding) { CatShell::Algorithm::cat_print(output, padding); }
inline void RegisterToBocLib::cat_free() {}

} // namespace DbmShell

#endif

#ifndef FEI4EMU_H__
#define FEI4EMU_H__

#include <vector>
#include "Bmf.h"

namespace Boclib
{
    class Fei4Emu
    {
        public:
            enum Register
            {
                Control         = 0x0,
                Status          = 0x1,
                ExtraFrames     = 0x2,
                Control2        = 0x3,
                HitInject       = 0x6,
                HitCount        = 0x7
            };

            enum Mode
            {
                ManualInject,
                AutomaticInject
            };

            struct Hit {
                uint16_t Row;
                uint16_t Col;
                uint8_t Tot1;
                uint8_t Tot2;
            };

            // constructor
            Fei4Emu(Bmf &bmf, int channel);

            // enable/disable
            void Enable();
            void Disable();
            bool IsEnabled();

            // switch mode
            void SetMode(Mode mode);
            Mode GetMode();

            // arm/disarm
            void SetArmed(bool on);
            bool GetArmed();

            // get/set ChipID
            void SetChipId(uint8_t ChipId);
            uint8_t GetChipId();

            // get/set TX link
            void SetTxLink(uint8_t Tx);
            uint8_t GetTxLink();

            // check RunMode
            bool GetRunMode();

            // ToT and extra frames
            uint8_t GetExtraFrames();
            void SetExtraFrames(uint8_t Value);
            uint8_t GetTotValue();
            void SetTotValue(uint8_t Value);
            uint8_t GetTot2Value();
            void SetTot2Value(uint8_t Value);

            // HitCounter
            uint8_t GetHitCount();
            void SetHitCount(uint8_t Count);

            // Inject Hit(s)
            void Inject(uint16_t Row, uint16_t Col, uint8_t Tot1, uint8_t Tot2 = 0xF);
            void Inject(Hit hit);
            void Inject(std::vector<Hit> hits);
            void Inject(uint8_t Raw);

        private:
            Bmf &bmf;
            unsigned int channel;
            unsigned int baseaddr;
    };

}

#endif // FEI4EMU_H__
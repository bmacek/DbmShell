#ifndef BOC_H__
#define BOC_H__

#include <cstdint>
#include <exception>

#include "Bcf.h"
#include "Bmf.h"

namespace Boclib
{
    class Boc;
    class BocException;
    class Bmf;
    class Bcf;

    class BocException : public std::exception
    {
    public:
        BocException(const std::string &ErrorMessage)
        {
            Error = ErrorMessage;
        }

        virtual const char* what() const throw()
        {
            return Error.c_str();
        }

    private:
        std::string Error;
    };

    class Boc
    {
    public:
        // enum FifoType {
        //     Fifo8,              // 8 bit FIFO
        //     Fifo16_LSB,         // 16 bit FIFO, LSB on base address
        //     Fifo16_MSB,         // 16 bit FIFO, MSB on base address
        //     Fifo32_LSB,         // 32 bit FIFO, LSB on base address
        //     Fifo32_MSB          // 32 bit FIFO, MSB on base address
        // };

        Boc(const std::string &host);
        Boc() : Boc("192.168.0.10") { };
        ~Boc();

        uint8_t SingleRead(uint16_t Addr);
        size_t MultipleRead(uint16_t Addr, uint8_t *Buffer, size_t Len, bool IncAddr);

        void SingleWrite(uint16_t Addr, uint8_t Value);
        size_t MultipleWrite(uint16_t Addr, uint8_t *Buffer, size_t Len, bool IncAddr);

        // FIFO read/write
        // size_t FifoRead(uint16_t FifoBase, uint16_t FifoStatusBase, uint8_t FifoStatusMask, FifoType type, uint8_t *Buffer, size_t Len);
        // size_t FifoWrite(uint16_t FifoBase, uint16_t FifoStatusBase, uint8_t FifoStatusMask, FifoType type, uint8_t *Buffer, size_t Len);

        Bcf GetBcf();
        Bmf GetBmf(const Bmf::Target target);

    private:
        int socketfd;
        uint32_t transaction_id;

        // IPBus header
        struct IPBusHeader {
            unsigned int vers     :  4;
            unsigned int trans_id : 11;
            unsigned int words    :  9;
            unsigned int type     :  5;
            unsigned int d        :  1;
            unsigned int res      :  2;
        };
        static void header2bytes(IPBusHeader *header, uint8_t *buf);
        static void bytes2header(IPBusHeader *header, uint8_t* buf);
        static void print_header(IPBusHeader *header);
    };

};


#endif // BOC_H__
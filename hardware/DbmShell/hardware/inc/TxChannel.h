#ifndef TXCHANNEL_H__
#define TXCHANNEL_H__

#include "Boc.h"
#include "Bmf.h"
#include "Bitstream.h"

namespace Boclib
{
    class Bmf;

    class TxChannel
    {
    public:
        enum Register
        {
            Control             = 0x0,
            Status              = 0x1,
            FifoControl         = 0x2,
            FifoData            = 0x3,
            Control2            = 0x4,
            CoarseDelay         = 0x5,
            MonCnt              = 0x6,
            MonFifoL            = 0x7,
            MonFifoH            = 0x8,
        };

        enum Configuration
        {
            Config_40M_ROD_BPM,            // use input from the ROD
            Config_40M_ROD_noBPM,        // use input from the ROD
            Config_40M_BOC_BPM,            // use input from the BOC
            Config_40M_BOC_noBPM,        // use input from the BOC
            Config_160M_BOC                // 8b10b 160 Mbit/s mode
        };

        enum MonitoringCounter {
            CntTrig             = 0x0,
            CntECR              = 0x1,
            CntBCR              = 0x2,
            CntCAL              = 0x3,
            CntSlow             = 0x4,
            CntCorrupt          = 0x5,
            CntSYNC             = 0x6
        };

        TxChannel(Bmf &bmf, int ch, bool broadcast = false);
        int GetChannel();

        // generic register interface
        uint8_t ReadRegister(uint8_t Addr);
        void WriteRegister(uint8_t Addr, uint8_t Value);
        size_t ReadRegister(uint8_t Addr, uint8_t *Value, size_t Len, bool Inc);
        size_t WriteRegister(uint8_t Addr, uint8_t *Value, size_t Len, bool Inc);

        // broadcasting control
        void EnableBroadcast();
        void DisableBroadcast();
        bool IsBroadcastEnabled();

        // configure TX channel
        void SetConfiguration(Configuration config);
        Configuration GetConfiguration();

        // FIFO operation
        void FifoPut(uint8_t data, bool kword = false);
        void FifoPut(Bitstream &bitstream, bool framed = false);
        bool FifoIsFull();
        bool FifoIsEmpty();
        void FifoSend();

        // set/get coarse delay setting
        void SetCoarse(int coarse);
        int GetCoarse();

        // mute
        void Mute();
        void UnMute();
        bool IsMuted();

        // monitoring
        void EnableMonitoring(bool EnableFifo = false);
        void DisableMonitoring();
        bool IsMonitoringEnabled();
        bool IsMonitoringFifoEnabled();
        uint32_t GetMonitoringCounter(MonitoringCounter cnt);
        uint16_t GetMonitoringFifo();

    private:
        int m_ch;
        bool m_broadcast;
        Bmf &m_bmf;
    };
}

#endif // TXCHANNEL_H__
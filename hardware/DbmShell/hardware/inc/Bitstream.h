#ifndef BITSTREAM_H__
#define BITSTREAM_H__

#include <cstdint>
#include <vector>

namespace Boclib
{
    class Bitstream
    {
    public:
        Bitstream()
        {
            len = 0;
            data_tmp = 0;
        }

        void AppendVar(uint32_t value, int length)
        {
            if(length == 32)
            {
                Append32(value);
            }
            else if(length == 16)
            {
                Append16(value & 0xFFFF);
            }
            else if(length == 8)
            {
                Append8(value & 0xFF);
            }
            else
            {
                for(int i = length-1; i >= 0; i--)
                {
                    if(value & (1 << i))
                    {
                        data_tmp = data_tmp | (1 << (7-(len % 8)));
                    }
                    len++;

                    if(len % 8 == 0)
                    {
                        data.push_back(data_tmp);
                        data_tmp = 0;
                    }
                }
            }
        }

        void Append8(uint8_t value)
        {
            if(len % 8 == 0)
            {
                data.push_back(value);
                data_tmp = 0;
                len = len + 8;
            }
            else
            {
                for(int i = 7; i >= 0; i--)
                {
                    if(value & (1 << i))
                    {
                        data_tmp = data_tmp | (1 << (7-(len % 8)));
                    }
                    len++;

                    if(len % 8 == 0)
                    {
                        data.push_back(data_tmp);
                        data_tmp = 0;
                    }
                }
            }
        }

        void Append16(uint16_t value)
        {
            Append8((value >> 8) & 0xFF);
            Append8((value >> 0) & 0xFF);
        }

        void Append32(uint32_t value)
        {
            Append8((value >> 24) & 0xFF);
            Append8((value >> 16) & 0xFF);
            Append8((value >> 8) & 0xFF);
            Append8((value >> 0) & 0xFF);
        }

        void AppendVec(std::vector<uint8_t> vec)
        {
            for(int i = 0; i < vec.size(); i++)
                Append8(vec[i]);
        }

        void AppendBit(Bitstream &bitstream)
        {
            for(int i = 0; i < bitstream.data.size(); i++)
            {
                Append8(bitstream.data[i]);
            }
        }

        std::vector<uint8_t>& GetData()
        {
            if(len % 8 != 0)
                data.push_back(data_tmp);
            
            return data;
        }

        void Clear()
        {
            len = 0;
            data.clear();
        }

        int Size()
        {
            return len;
        }

    private:
        std::vector<uint8_t> data;
        int len;
        uint8_t data_tmp;
    };
}

#endif // BITSTREAM_H__
#ifndef IBLRXCHANNEL_H__
#define IBLRXCHANNEL_H__

#include "Boc.h"
#include "Bmf.h"

namespace Boclib
{
    class Bmf;

    class IblRxChannel
    {
    public:
        enum Register
        {
            Control                 = 0x0,
            Status                  = 0x1,
            DataHigh                = 0x2,
            DataLow                 = 0x3,
            FrameCnt0               = 0x4,
            FrameCnt1               = 0x5,
            FrameCnt2               = 0x6,
            FrameCnt3               = 0x7,
            FrameErrCnt0            = 0x8,
            FrameErrCnt1            = 0x9,
            FrameErrCnt2            = 0xA,
            FrameErrCnt3            = 0xB,
            Control2                = 0xF,
            LinkOccMeanLow          = 0x18,
            LinkOccMeanHigh         = 0x19,
            LinkOccLiveLow          = 0x1A,
            LinkOccLiveHigh         = 0x1B,
            ErrHistoCtrl            = 0x1C,
            ErrHistoCount           = 0x1D,
            RodSaverThresh          = 0x1E,
        };

        enum Input
        {
            InputOptical                = 0x0,
            InputEmulator               = 0x1,
            InputSpare1                 = 0x2,
            InputSpare2                 = 0x3,
            InputOpticalSpareA          = 0x4,
            InputOpticalSpareB          = 0x5,
            InputOpticalSpareC          = 0x6,
            InputOpticalSpareD          = 0x7
        };

        enum ErrHisto
        {
            LockErr                  = 0x0,
            SyncErr                  = 0x1,
            DecErr                   = 0x2,
            DispErr                  = 0x3,
            FrameErr                 = 0x4
        };

        IblRxChannel(Bmf &bmf, int ch);
        int GetChannel();

        // generic register interface
        uint8_t ReadRegister(uint8_t Addr);
        void WriteRegister(uint8_t Addr, uint8_t Value);
        size_t ReadRegister(uint8_t Addr, uint8_t *Buffer, size_t Len, bool Inc);
        size_t WriteRegister(uint8_t Addr, uint8_t *Buffer, size_t Len, bool Inc);

        // enable and disable RX
        bool Enable(bool CheckStatus = false, int timeout = 10);
        void Disable();
        bool IsEnabled();

        // RX status
        bool IsSynced(bool reset = false);
        bool IsLocked(bool reset = false);
        bool HasDecodingErrors(bool reset = false);
        bool HasDisparityErrors(bool reset = false);
        bool HasFrameErrors(bool reset = false);
        bool RodSaverTriggered(bool reset = false);
        bool StatusIsGood(bool reset = false);
        void ResetStatus();

        // RX FIFO access
        uint16_t FifoGet();
        bool FifoIsFull();
        bool FifoIsEmpty();

        // configure ROD output
        void EnableRodOutput();
        void DisableRodOutput();
        bool IsRodOutputEnabled();

        // configure monitoring
        void EnableMonitorFifo(bool Raw = false, bool AllowIdle = false);
        void DisableMonitorFifo();
        bool IsMonitorFifoEnabled();
        bool IsMonitorFifoRaw();
        bool IsMonitorFifoAllowIdle();

        // get frame counter
        uint32_t GetFrameCount();
        uint32_t GetFrameErrCount();

        // select input
        void SetInput(Input in);
        Input GetInput();

        // invert input
        void EnableInvert();
        void DisableInvert();
        bool IsInvertEnabled();

        // rod saver mechanism
        void EnableRodSaver(uint8_t threshold);
        void DisableRodSaver();
        bool IsRodSaverEnabled();

        // link occupancy readout
        double GetLiveOccupancy();
        double GetMeanOccupancy();

        // error histogram counters
        void ErrHistoReset();
        void ErrHistoReset(ErrHisto Counter);
        uint32_t ErrHistoRead(ErrHisto Counter);

    private:
        int m_ch;
        Bmf &m_bmf;
    };
}

#endif // IBLRXCHANNEL_H__
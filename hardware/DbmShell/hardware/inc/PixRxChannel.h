#ifndef PIXRXCHANNEL_H__
#define PIXRXCHANNEL_H__

#include "Boc.h"
#include "Bmf.h"

namespace Boclib
{
    class Bmf;

    class PixRxChannel
    {
    public:
        enum Register
        {
            Control                 = 0x0,
            Status                  = 0x1,
            Data                    = 0x3,
            FibreMaster             = 0x4,
            FibreSlave              = 0x5,
            TrailerLengthLow        = 0x6,
            TrailerLengthHigh       = 0x7,
            CompareControl          = 0x8,
            ComparePattern          = 0x9,
            CompareMask             = 0xA,
            CompareCounters         = 0xB,
            FrameCounter            = 0xC,
            XCSel                   = 0xE,
            Control2                = 0xF,
            EmuConfig1              = 0x10,
            EmuConfig2              = 0x14,
            LinkOccMeanLow          = 0x18,
            LinkOccMeanHigh         = 0x19,
            LinkOccLiveLow          = 0x1A,
            LinkOccLiveHigh         = 0x1B,
        };

        PixRxChannel(Bmf &bmf, int ch, bool broadcast = false);
        int GetChannel();

        // generic register interface
        uint8_t ReadRegister(uint8_t Addr);
        void WriteRegister(uint8_t Addr, uint8_t Value);
        size_t ReadRegister(uint8_t Addr, uint8_t *Value, size_t Len, bool Inc);
        size_t WriteRegister(uint8_t Addr, uint8_t *Value, size_t Len, bool Inc);

        // broadcasting control
        void EnableBroadcast();
        void DisableBroadcast();
        bool IsBroadcastEnabled();

        // enable and disable RX
        bool Enable();
        void Disable();
        bool IsEnabled();

        // RX FIFO access
        uint8_t FifoGet();
        bool FifoIsFull();
        bool FifoIsEmpty();

        // configure ROD output
        void EnableRodOutput();
        void DisableRodOutput();
        bool IsRodOutputEnabled();

        // configure monitoring
        void EnableMonitorFifo(bool Raw = false, bool Slow = false);
        void DisableMonitorFifo();
        bool IsMonitorFifoEnabled();
        bool IsMonitorFifoRaw();
        bool IsMonitorFifoSlow();

        // invert input
        void EnableInvert();
        void DisableInvert();
        bool IsInvertEnabled();

        // emulator config
        void SetEmuConfig(uint32_t config1, uint32_t config2);
        void SetEmuConfig1(uint32_t value);
        void SetEmuConfig2(uint32_t value);
        uint32_t GetEmuConfig1();
        uint32_t GetEmuConfig2();

        // select fibres
        void SetFibre(int master, int slave);
        void SetFibreMaster(int fibre);
        void SetFibreSlave(int fibre);
        int GetFibreMaster();
        int GetFibreSlave();

        // threshold
        void SetThresholdMaster(uint16_t threshold);
        void SetThresholdSlave(uint16_t threshold);

        // delay
        void SetSamplingDelayMaster(uint8_t sampling, uint8_t delay = 0);
        void GetSamplingDelayMaster(uint8_t &sampling, uint8_t &delay);
        void SetSamplingDelaySlave(uint8_t sampling, uint8_t delay = 0);
        void GetSamplingDelaySlave(uint8_t &sampling, uint8_t &delay);
        bool GetDelayMasterOK();
        bool GetDelaySlaveOK();

        // frame counter
        uint32_t GetFrameCount();

        // configure trailer length
        void SetTrailerLength(uint16_t length);
        uint16_t GetTrailerLength();

        // compare unit
        void CompareEnable();
        void CompareDisable();
        bool CompareIsEnabled();
        void CompareClearCounters();
        void CompareGetCounters(uint32_t &bits, uint32_t &errors);
        void CompareLoad(Bitstream &Expected, uint8_t Mask = 0xFF);
        void CompareLoad(Bitstream &Expected, Bitstream &Mask);

        // link occupancy readout
        double GetLiveOccupancy();
        double GetMeanOccupancy();

        // check link activity
        bool HasActivity();

        // check if line is stuck at '1'
        bool IsStuck(bool reset);

    private:
        int m_ch;
        bool m_broadcast;
        Bmf &m_bmf;
    };
}

#endif // PIXRXCHANNEL_H__

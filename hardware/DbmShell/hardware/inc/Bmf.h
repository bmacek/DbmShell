#ifndef BMF_H__
#define BMF_H__

#include <sstream>
#include <cstdint>
#include "Boc.h"
#include "TxChannel.h"
#include "PixRxChannel.h"
#include "IblRxChannel.h"
#include "Fei4Emu.h"

namespace Boclib
{
    class Boc;
    class BocException;
    class Bmf;
    class Bcf;
    class TxChannel;
    class PixRxChannel;
    class IblRxChannel;
    class Fei4Emu;

    class Bmf
    {
    public:
        // register map
        enum Register
        {
            FirmwareVersion         = 0x0,
            BuildYear               = 0x1,
            BuildMonth              = 0x2,
            BuildDay                = 0x3,
            BuildHour               = 0x4,
            BuildMinute             = 0x5,
            BuildSecond             = 0x6,
            LockLossCnt             = 0x7,
            RxDebug                 = 0xA,
            SlinkCounters           = 0xB,
            Uptime                  = 0xC,
            QSFPControlStatus       = 0xD,
            QSFPRegAddr             = 0xE,
            QSFPRegData             = 0xF,
            Slink0Ctrl              = 0x10,
            Slink0Stat              = 0x11,
            Slink1Ctrl              = 0x12,
            Slink1Stat              = 0x13,
            FeatureLow              = 0x14,
            FeatureHigh             = 0x15,
            RxPlugin0DAC_Csr        = 0x16,
            RxPlugin0DAC_DataL      = 0x17,
            RxPlugin0DAC_DataH      = 0x18,
            RxPlugin1DAC_Csr        = 0x19,
            RxPlugin1DAC_DataL      = 0x1A,
            RxPlugin1DAC_DataH      = 0x1B,
            RxSpeed                 = 0x1F,
            GitHash                 = 0x20,
            TxBroadcastMask0        = 0x34,
            TxBroadcastMask1        = 0x35,
            RxBroadcastMask0        = 0x36,
            RxBroadcastMask1        = 0x37,
            SecureReg               = 0x4D,
            CdrBase                 = 0x50,
            Fei4EmuBase             = 0x80
        };

        // target FPGA
        enum Target
        {
            North,
            South,
            Both
        };

        // S-LINK channel
        enum SlinkChannel {
            Slink0 = 1,
            Slink1 = 2
        };

        // S-LINK HOLA
        enum SlinkHola {
            Hola0 = 1,
            Hola1 = 2
        };


        // BMF RX speed setting
        enum Speed {
            Rx_40M,
            Rx_80M,
            Rx_2x80M
        };

        // Register offsets
        static const uint16_t RegOffs_North   = 0x8000;
        static const uint16_t RegOffs_South   = 0x4000;
        static const uint16_t RegOffs_Tx      = 0x0C00;
        static const uint16_t RegOffs_Rx      = 0x0800;
        static const uint16_t RefOffs_General = 0x0000;
        static const uint16_t RefOffs_Dbm     = 0x0400;


        // get target from string or const char *
        static Target GetTarget(const std::string &target);

        // construct Bcf
        Bmf(Boc &boc, Target target);

        // get TX/RX channel
        TxChannel GetTxChannel(int ch);
        IblRxChannel GetIblRxChannel(int ch);
        PixRxChannel GetPixRxChannel(int ch);
        Fei4Emu GetFei4Emu(int ch);

        // generic register interface
        uint8_t ReadRegister(uint16_t Addr);
        void WriteRegister(uint16_t Addr, uint8_t Value);
        size_t ReadRegister(uint16_t Addr, uint8_t *Value, size_t Len, bool Inc);
        size_t WriteRegister(uint16_t Addr, uint8_t *Value, size_t Len, bool Inc);

        // get some version information from the BCF
        uint8_t GetVersion();
        std::string GetBuildDate();
        std::string GetGitHash();

        // read the BMF uptime
        uint32_t GetUptime();

        // SecureReg functionality
        void UnlockSecureReg();

        // get additional feature informations
        uint16_t GetFeatures();
        bool IsIbl();
        bool IsPix();

        // set/get RX speed for L1/L2
        void SetRxSpeed(Speed speed);
        Speed GetRxSpeed();

        // set/get threshold
        uint16_t GetRxThreshold(int Fibre);
        uint16_t GetRxRGS(uint8_t Plugin);
        void SetRxThreshold(int Fibre, uint16_t Value);
        void SetRxRGS(uint8_t Plugin, uint16_t Value);

        // L1/L2 sampling delay setting
        void SetSamplingDelay(int fibre, uint8_t sampling, uint8_t delay = 0);
        void GetSamplingDelay(int fibre, uint8_t &sampling, uint8_t &delay);
        bool GetDelayOk(int fibre);

        // IBL CDR settings
        void CDRTrain(int fibre);
        void CDRReset(int fibre, bool train);
        void CDRAutoPhase(int fibre);
        void CDRManPhase(int fibre, uint8_t phase);
        uint8_t CDRGetPhase(int fibre);
        bool CDRIsAutomatic(int fibre);
        void CDRGetCounters(int fibre, uint8_t &cnt0, uint8_t &cnt1, uint8_t &cnt2, uint8_t &cnt3);

        // PLL lock loss
        unsigned int GetLockLoss();

        // RX chipscope debugging
        unsigned int GetRxDebug();
        void SetRxDebug(unsigned int ch);

        // S-LINK control
        void SlinkEnable(SlinkChannel ch, SlinkHola hola);
        void SlinkDisable(SlinkChannel ch);
        void SlinkReset(SlinkChannel ch, bool HolaReset = true, bool MgtReset = false);
        bool SlinkLinkUp(SlinkChannel ch);
        bool SlinkLinkBusy(SlinkChannel ch);
        void SlinkGetCounters(uint32_t &sl0_lff, uint32_t &sl0_ldown, uint32_t &sl1_lff, uint32_t &sl1_ldown, bool reset = false);

        // QSFP control
        bool QSFPIsPresent();
        bool QSFPHasInterrupt();
        uint8_t QSFPReadRegister(uint8_t addr);
        void QSFPWriteRegister(uint8_t addr, uint8_t value);
        void QSFPReset();

        // TX/RX broadcast control
        bool HasTxBroadcast();
        bool HasRxBroadcast();
        void SetTxBroadcastMask(uint16_t mask);
        uint16_t GetTxBroadcastMask();
        void SetRxBroadcastMask(uint16_t mask);
        uint16_t GetRxBroadcastMask();

    private:
        Boc &m_boc;            // reference Boc class for low-level communication
        Target m_target;    // target FPGA

        const unsigned int fibre_dac_mapping[12] = {
            (0x2 << 2) | 0x2,       // DAC 2 (I2C 0x41), Channel C
            (0x2 << 2) | 0x3,       // DAC 2 (I2C 0x41), Channel D
            (0x2 << 2) | 0x1,       // DAC 2 (I2C 0x41), Channel B
            (0x2 << 2) | 0x0,       // DAC 2 (I2C 0x41), Channel A
            (0x1 << 2) | 0x2,       // DAC 1 (I2C 0x40), Channel C
            (0x1 << 2) | 0x3,       // DAC 1 (I2C 0x40), Channel D
            (0x1 << 2) | 0x1,       // DAC 1 (I2C 0x40), Channel B
            (0x1 << 2) | 0x0,       // DAC 1 (I2C 0x40), Channel A
            (0x0 << 2) | 0x2,       // DAC 0 (I2C 0x32), Channel C
            (0x0 << 2) | 0x3,       // DAC 0 (I2C 0x32), Channel D
            (0x0 << 2) | 0x1,       // DAC 0 (I2C 0x32), Channel B
            (0x0 << 2) | 0x0        // DAC 0 (I2C 0x32), Channel A
        };
    };
};

#endif // BCF_H__

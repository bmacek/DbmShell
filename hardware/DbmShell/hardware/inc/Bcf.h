#ifndef BCF_H__
#define BCF_H__

#include <cstdint>
#include "Boc.h"
#include "Bmf.h"

namespace Boclib {
    class Boc;
    class BocException;
    class Bmf;
    class Bcf;

    class Bcf
    {
    public:
        // register map
        enum Register
        {
            FirmwareVersion            = 0x0,
            BuildYear               = 0x1,
            BuildMonth              = 0x2,
            BuildDay                = 0x3,
            BuildHour               = 0x4,
            BuildMinute             = 0x5,
            BuildSecond             = 0x6,
            LaserEnable             = 0x7,
            JtagOverride            = 0x8,
            SerialNumber            = 0x9,
            PrimProcOp              = 0x10,
            PrimProcArg             = 0x11,
            PrimProcRet             = 0x15,
            VmeClkCsr               = 0x20,
            VmeFreq                 = 0x21,
            RebootReg               = 0x27,
            SecureReg               = 0x2A,
            InterlockLossCnt        = 0x2B,
            Uptime                  = 0x2C,
            PllIdelay               = 0x36,
            ProgNorthCrc0           = 0x38,
            ProgNorthCrc1           = 0x39,
            ProgNorthCrc2           = 0x3A,
            ProgNorthCrc3           = 0x3B,
            ProgSouthCrc0           = 0x3C,
            ProgSouthCrc1           = 0x3D,
            ProgSouthCrc2           = 0x3E,
            ProgSouthCrc3           = 0x3F,
            ProgNorthCtrl           = 0x40,
            ProgNorthStatus         = 0x41,
            ProgNorthDCNT           = 0x42,
            ProgNorthData           = 0x43,
            ProgNorthFladdr0        = 0x44,
            ProgNorthFladdr1        = 0x45,
            ProgNorthFladdr2        = 0x46,
            ProgSouthCtrl           = 0x48,
            ProgSouthStatus         = 0x49,
            ProgSouthDCNT           = 0x4A,
            ProgSouthData           = 0x4B,
            ProgSouthFladdr0        = 0x4C,
            ProgSouthFladdr1        = 0x4D,
            ProgSouthFladdr2        = 0x4E,
            GitHash                 = 0x50
        };

        // interlock status
        enum InterlockState
        {
            Interlock_DET_BOC_BCF    = 0x0,            // all interlock sources
            Interlock_DET_BOC        = 0x1,            // DET and BOC
            Interlock_DET_BCF        = 0x2,            // DET and BCF
            Interlock_DET            = 0x3,            // DET
            Interlock_BOC_BCF        = 0x4,
            Interlock_BOC            = 0x5,
            Interlock_BCF            = 0x6,
            Interlock_Nothing        = 0xAB            // only here the lasers are ON!
        };

        // JTAG Inputs
        enum JTAGInput
        {
            JTAG_BCF                = 0x00,
            JTAG_XVC                = 0x54,
            JTAG_External            = 0xAB
        };

        // Clock sources
        enum ClockSource
        {
            ClkSrc_Internal,
            ClkSrc_Unbuffered,
            ClkSrc_Buffered
        };

        // Microblaze primitives
        enum PrimList
        {
            PrimNOP                = 0x00,
            PrimDumpGPMem       = 0x03,
            PrimEnvSave         = 0x08,
            PrimEnvLoad         = 0x09,
            PrimSetIp           = 0x10,
            PrimGetIp           = 0x11,
            PrimSetGw           = 0x12,
            PrimGetGw           = 0x13,
            PrimSetSubnet       = 0x14,
            PrimGetSubnet       = 0x15,
            PrimRecoveryId      = 0x20,
            PrimFlashId         = 0x30,
            PrimFlashErase      = 0x31,
            PrimFlashRead       = 0x32,
            PrimFlashWrite      = 0x33,
            PrimCoarseLoad      = 0x40,
            PrimCoarseSave      = 0x41
        };

        // construct Bcf
        Bcf(Boc &boc) : boc(boc) { };

        // generic register interface
        uint8_t ReadRegister(uint16_t Addr);
        void WriteRegister(uint16_t Addr, uint8_t Value);
        size_t ReadRegister(uint16_t Addr, uint8_t *Value, size_t Len, bool Inc);
        size_t WriteRegister(uint16_t Addr, uint8_t *Value, size_t Len, bool Inc);

        // get some version information from the BCF
        uint8_t GetVersion();
        std::string GetBuildDate();
        std::string GetGitHash();
        std::string GetSerial();

        // read the BMF uptime
        uint32_t GetUptime();

        // SecureReg functionality
        void UnlockSecureReg();

        // Control the laser interlock
        void SetLaser(bool On);
        bool GetLaser();
        InterlockState GetInterlockState();
        unsigned int GetInterlockLossCnt();

        // BMF programming and reset routines
        bool ResetBmf(Bmf::Target target);
        bool ResetBmf();
        bool FlashBmf(Bmf::Target target, std::string filename);

        // JTAG control
        JTAGInput GetJTAGInput();
        void SetJTAGInput(JTAGInput input);

        // clock configuration
        unsigned int GetVmeFreq();
        bool GetVmeClkGood();
        void SetClkSource(ClockSource src);
        ClockSource GetClkSource();
        unsigned int GetIdelay();
        void SetIdelay(unsigned int value);

        // primitive processing
        void ProcessPrimitive(Bcf::PrimList primitive, uint8_t arg[4], uint8_t ret[4]);

        // IP configuration
        void SetIpConfig(uint8_t ip[4], uint8_t gw[4], uint8_t subnet[4]);
        void GetIpConfig(uint8_t ip[4], uint8_t gw[4], uint8_t subnet[4]);

        // BCF Flash memory reading
        bool FlashEraseSector(unsigned int baseaddr);
        bool FlashReadPage(unsigned int baseaddr, uint8_t buf[256]);
        bool FlashWritePage(unsigned int baseaddr, uint8_t buf[256]);
        bool FlashBcf(std::string filename, bool verify = true);

        // coarse delay saving to flash memory
        void SaveCoarse(uint32_t mask = 0xFFFFFFFFU);
        void LoadCoarse(uint32_t mask = 0xFFFFFFFFU);

        // reboot BCF
        void RebootBcf(bool Recovery = false);
        bool CheckRecovery();

    private:
        void ProgSetAddr(Bmf::Target target, uint32_t Addr);
        uint32_t ProgGetCRC(Bmf::Target target, unsigned int Len);
        void ProgWriteControl(Bmf::Target target, uint8_t value);
        uint8_t ProgReadStatus(Bmf::Target target);

        Boc &boc;            // reference Boc class for low-level communication
    };
};

#endif // BCF_H__
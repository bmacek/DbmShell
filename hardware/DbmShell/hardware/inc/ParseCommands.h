#ifndef PARSECOMMANDS_H__
#define PARSECOMMANDS_H__

#include <vector>

#define DEFAULT_PROMPT            "> "

namespace Boclib
{
    class ParseCommands
    {
        typedef int (*ParseCommandsCallback)(std::vector<std::string> &args);

    public:
        // constructor
        ParseCommands() : ParseCommands(DEFAULT_PROMPT) { };
        ParseCommands(const std::string Prompt) : m_prompt(Prompt), m_exit(false) { };

        // read a line
        void ReadLine();

        // check for exit flag
        bool DoExit();

        // get return value of the last command
        int GetReturnCode();

        // modify prompt
        void SetPrompt(const std::string Prompt);
        std::string GetPrompt();

        // add commands
        void AddCommand(const std::string Name, ParseCommandsCallback Callback, const std::string Description = "", const std::string HelpText = "");
        void ListCommands();

    private:
        struct Command {
            std::string Name;
            std::string Description;
            std::string HelpText;
            ParseCommandsCallback Callback;
        };

        void Parse();
        void Execute();
        static bool CompareCommands(const Command &rhs, const Command &lhs);

        std::string m_prompt;
        std::vector<Command> m_commands;
        std::vector<std::string> m_args;
        std::string m_input;
        int m_return;
        bool m_exit;
    };
}

#endif // PARSE_COMMANDS_H__
#ifndef OPTIONS_H__
#define OPTIONS_H__

#include <iostream>
#include <string>
#include <memory>

#include "boost/program_options.hpp"
#include "boost/any.hpp"

namespace po = boost::program_options;

class Options {
public:
    Options()
    {
        desc = std::make_shared<po::options_description>("Options");
        AddOption("help,h", "Print help and exit");
        AddOption("version,v", "Print version and exit");
        AddOption("host,H", host, "BOC Hostname or IP (default is 192.168.0.10)");
        AddOption("ip", ip, "BOC IP address (default is 192.168.0.10)");
    }

    //Options() = delete;
    //Options(Options&) = delete;
    //Options(Options&&) = delete;

    void AddOption(const std::string& cmd, const std::string& description)
    {
        auto c = cmd.c_str();
        auto d = description.c_str();
        desc->add_options()(c, d);
    }

    template <typename T>
    void AddOption(const std::string& cmd, T& val, const std::string& description)
    {
        auto c = cmd.c_str();
        auto d = description.c_str();
        desc->add_options()(c, po::value<T>(&val), d);
    }

    template <typename T>
    void AddOption(const std::string& cmd, T& val, T default_value, const std::string& description)
    {
        desc->add_options()(cmd.c_str(), po::value<T>(&val)->default_value(default_value), description.c_str());
    }

    template <typename T, typename U>
    void AddOption(const std::string& cmd, T& val, U default_value, const std::string& description)
    {
        //static_assert(std::is_convertible<U, T>::value, "Plase use types that can be converted into each other for" VAR_NAME(val) " and " VAR_NAME(default_value));
        desc->add_options()(cmd.c_str(), po::value<T>(&val)->default_value(default_value), description.c_str());
    }

    void Parse(int argc, char **argv)
    {
        try {
            po::store(po::parse_command_line(argc, argv, *desc), vm);
            po::notify(vm);
        }
        catch (...) {
            std::cout << "blah" << std::endl;
            std::cout << *desc << std::endl;
            exit(1);
        }

        if (vm.count("help")) {
            std::cout << *desc << std::endl;
            exit(0);
        }

        if (vm.count("version")) {
            //std::cout << version << std::endl;
            exit(0);
        }
    }

    std::string GetHost()
    {
        if (!vm.count("host") && !vm.count("ip")) {
            char* host_env = getenv("IBLBOC_HOST");
            if (host_env == nullptr) {
                char *ip_env = getenv("IBLBOC_IP");
                if(ip_env == nullptr) {
                    return "192.168.0.10";
                } else {
                    return ip_env;
                }
            }
            else {
                return host_env;
            }
        }
        else {
            if(vm.count("ip"))
                return ip;
            else
                return host;
        }
    }

private:
    std::shared_ptr<po::options_description> desc;
    po::variables_map vm;
    std::string host;
    std::string ip;
};

#endif // OPTIONS_H__

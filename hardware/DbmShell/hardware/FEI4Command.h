#ifndef INCLUDE_FEI4COMMAND
#define INCLUDE_FEI4COMMAND

#include "CatShell/core/CatObject.h"
#include "DbmShell/hardware/defines.h"

#include <vector>

namespace DbmShell{

class FEI4Command: public CatShell::CatObject
{
	CAT_OBJECT_DECLARE(FEI4Command, CatShell::CatObject, CAT_NAME_FEI4COMMAND, CAT_TYPE_FEI4COMMAND);

public:

	enum class type : std::uint8_t { None = 0, WrReg = 1, RdReg = 2, WrFe = 3, GlPulse = 4, Mode = 5, Reset = 6 };

	FEI4Command();
		/// Constructor: default.

	virtual ~FEI4Command();
		/// Destructor.

	//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

	virtual void* cat_get_part(CatShell::CatAddress& addr);
		///Returns the pointer to the desired part(element).
	//----------------------------------------------------------------------

	std::uint32_t get_bits(uint32_t start, uint32_t end) const;
		/// Returns the byte from start bit to end bit

	std::uint32_t get_first() const;
		/// Returns the place of first non-zero bit

	int size() const;
		/// Return the number of bytes within this command.

	std::uint8_t data_element(int i) const;	
		/// Returns the 'i'th byte of the command.

	std::uint8_t operator[](const int i) const;
		/// Returns the 'i'th byte of the command.

	void define_as_WrCommand(std::uint8_t chipID, std::uint8_t Address, std::uint16_t data);
		/// Define as write register command.

	void define_as_RdCommand(uint8_t chipID, uint8_t Address);
		/// Define as read register command.

	void define_as_WrFrontEnd(std::uint8_t chipID, std::uint8_t *data_c);
		/// Define as FE write to pixel registers.

	void define_as_GlobalPulse(std::uint8_t chipID, std::uint8_t width);
		/// Define as global pulse.

	void define_as_Mode(std::uint8_t chipID, std::uint8_t mode);
		/// Define as mode changing command.

	void define_as_Reset(std::uint8_t chipID);
		/// Define as mode changing command.


	type get_type() const;
		/// Returns type of the command.

private:

	void add_bits(uint8_t data, int length);
		/// Add command bits.
	
private:

	int _bit;
	type _type;
	std::vector<std::uint8_t> _data;

	// statics
	static std::uint8_t s_data_table [8][8];
	static std::uint8_t s_mask_vector[8];
	static std::uint8_t s_mask_vector_2[9];
};

inline StreamSize FEI4Command::cat_stream_get_size() const { return sizeof(std::uint8_t) + sizeof(std::uint32_t) + _data.size(); }
inline int FEI4Command::size() const { return _data.size(); }
inline std::uint8_t FEI4Command::operator[](const int i) const { return _data[i]; }
inline std::uint8_t FEI4Command::data_element(int i) const { return _data[i]; }
inline FEI4Command::type FEI4Command::get_type() const { return _type; }

} // namespace DbmShell

#endif

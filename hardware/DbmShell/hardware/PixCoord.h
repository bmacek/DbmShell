#ifndef INCLUDE_PIXCOORD
#define INCLUDE_PIXCOORD

#include "CatShell/core/CatObject.h"
#include "DbmShell/hardware/defines.h"



namespace DbmShell{

class PixCoord: public CatShell::CatObject
{
public:
	PixCoord();

	virtual ~PixCoord();

	CAT_OBJECT_DECLARE(PixCoord, CatShell::CatObject, CAT_NAME_PIXCOORD,CAT_TYPE_PIXCOORD);
	//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
	//----------------------------------------------------------------------
	virtual void* cat_get_part(CatShell::CatAddress& addr);
		///Returns the pointer to the desired part(element).

	void set(uint16_t row, uint8_t column);
	uint8_t column();
	uint16_t row();

private:
	// it should be private !! ask tomorow!!!!
	uint16_t _row;
	uint8_t _col;

};

inline StreamSize PixCoord::cat_stream_get_size() const { return sizeof (std::uint8_t)+ sizeof(std::uint16_t); }
inline void PixCoord::set(uint16_t row, uint8_t column) {_row = row; _col=column;}
inline uint8_t PixCoord::column() { return _col;}
inline uint16_t PixCoord::row() { return _row;}	
	
}
#endif

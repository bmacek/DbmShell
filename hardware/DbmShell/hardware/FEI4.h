#ifndef INCLUDE_FEI4
#define INCLUDE_FEI4

#include "CatShell/core/Algorithm.h"
#include "CatShell/core/Setting.h"
#include "DbmShell/hardware/defines.h"
#include "DbmShell/hardware/FEI4Command.h"


namespace DbmShell{

class FEI4
{
public:
	FEI4();
		/// Constructor: default.

	virtual ~FEI4();
		/// Destructor.

	void load_configuration(CatShell::Setting& setting);
		/// Load configuration of the chip from the settings.

	void default_configuration();
		/// FIll in the default configuration.

	void create_cmd_write(std::uint8_t reg, CatShell::CatPointer<FEI4Command>& cmd);
		/// Fills 'cmd' with the write command for predefined register value.

	void create_cmd_write(std::uint8_t reg, std::uint16_t value, CatShell::CatPointer<FEI4Command>& cmd);
		/// Fills 'cmd' with the desired command.

	void create_cmd_write(std::uint8_t reg, std::uint16_t value, std::uint16_t mask, CatShell::CatPointer<FEI4Command>& cmd);
		/// Fills 'cmd' with the desired command.

	void create_cmd_read(std::uint8_t reg, CatShell::CatPointer<FEI4Command>& cmd);
		/// Fills 'cmd' with the desired command.

	void create_cmd_mode(std::uint8_t mode, CatShell::CatPointer<FEI4Command>& cmd);
		/// Fills 'cmd' with the desired command.

	void create_cmd_reset(CatShell::CatPointer<FEI4Command>& cmd);
		/// Fills 'cmd' with the desired command.

	void create_cmd_pulse(std::uint8_t length, CatShell::CatPointer<FEI4Command>& cmd);
		/// Fills 'cmd' with the desired command.

	void create_cmd_write_fe(std::uint8_t* data, CatShell::CatPointer<FEI4Command>& cmd);
		/// Fills 'cmd' with the desired command.

private:

	std::uint16_t	_register[FEI4_NUMER_OF_REGISTERS+1];  // global registers
	std::uint8_t	_id;

	// mapping data
};

inline void FEI4::create_cmd_read(std::uint8_t reg, CatShell::CatPointer<FEI4Command>& cmd) { cmd->define_as_RdCommand(_id, reg); }
inline void FEI4::create_cmd_write(std::uint8_t reg, CatShell::CatPointer<FEI4Command>& cmd) { cmd->define_as_WrCommand(_id, reg, _register[reg]); }
inline void FEI4::create_cmd_mode(std::uint8_t mode, CatShell::CatPointer<FEI4Command>& cmd) { cmd->define_as_Mode(_id, mode); }
inline void FEI4::create_cmd_reset(CatShell::CatPointer<FEI4Command>& cmd) { cmd->define_as_Reset(_id); }
inline void FEI4::create_cmd_pulse(std::uint8_t length, CatShell::CatPointer<FEI4Command>& cmd) {cmd->define_as_GlobalPulse(_id, length); }
inline void FEI4::create_cmd_write_fe(std::uint8_t* data, CatShell::CatPointer<FEI4Command>& cmd) { cmd->define_as_WrFrontEnd(_id, data); }

}

#endif

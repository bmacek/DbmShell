#ifndef INCLUDE_FEI4MESSAGE
#define INCLUDE_FEI4MESSAGE

#include "CatShell/core/CatObject.h"
#include "DbmShell/hardware/defines.h"
#include "DbmShell/hardware/FEI4Command.h"

#include <vector>

namespace DbmShell{

class FEI4Message: public FEI4Command
{
	CAT_OBJECT_DECLARE(FEI4Message, FEI4Command, CAT_NAME_FEI4MESSAGE, CAT_TYPE_FEI4MESSAGE);

public:

	FEI4Message();
		/// Constructor: default.

	virtual ~FEI4Message();
		/// Destructor.

	//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

		virtual void* cat_get_part(CatShell::CatAddress& addr);
			///Returns the pointer to the desired part(element).
	//----------------------------------------------------------------------

	void set_address(const BocFpga& fpga, const std::uint8_t tx, const std::uint8_t rx);
		/// Set the destination address.

	BocFpga get_fpga() const;
		/// Get the destination fpga.

	std::uint8_t get_tx() const;
		/// Get the destination TX.

	std::uint8_t get_rx() const;
		/// Get the destination RX.
	
private:

	BocFpga      _fpga;
	std::uint8_t _tx;
	std::uint8_t _rx;
};

inline StreamSize FEI4Message::cat_stream_get_size() const { return FEI4Command::cat_stream_get_size() + 3*sizeof(std::uint8_t); }
inline void FEI4Message::set_address(const BocFpga& fpga, const std::uint8_t tx, const std::uint8_t rx) { _fpga = fpga; _tx = tx; _rx = rx; }
inline BocFpga FEI4Message::get_fpga() const { return _fpga; }
inline std::uint8_t FEI4Message::get_tx() const { return _tx; }
inline std::uint8_t FEI4Message::get_rx() const { return _rx; }

} // namespace DbmShell

#endif

#ifndef INCLUDE_DBMLUMIALGOCREATOR_INCLUDED
#define INCLUDE_DBMLUMIALGOCREATOR_INCLUDED

#include "CatShell/core/Process.h"

#include "DbmShell/hardware/defines.h"
#include "DbmShell/hardware/LumiAlgoLogic.h"

namespace DbmShell {

class LumiAlgoCreator : public CatShell::Process
{
        CAT_OBJECT_DECLARE(LumiAlgoCreator, CatShell::Process, CAT_NAME_LUMIALGOCREATOR, CAT_TYPE_LUMIALGOCREATOR);
public:

        LumiAlgoCreator();
                /// Constructor: default.

        virtual ~LumiAlgoCreator();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main();
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init();
                /// Called before the thread starts executing.

        virtual void work_deinit();
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatShell::CatPointer<CatShell::WorkThread> child);
                /// Called when a subprocess finished.
//----------------------------------------------------------------------

protected:

private:

        std::uint16_t menu();
                /// Dispays user menu.
private:

        CatShell::Algorithm::DataPort                                   _out_port;        // output port for lumi-logic
        std::map<std::string, CatShell::CatPointer<LumiAlgoLogic>>      _logics;
};

inline StreamSize LumiAlgoCreator::cat_stream_get_size() const { return CatShell::Process::cat_stream_get_size(); }
inline void LumiAlgoCreator::cat_stream_out(std::ostream& output) { CatShell::Process::cat_stream_out(output); }
inline void LumiAlgoCreator::cat_stream_in(std::istream& input) { CatShell::Process::cat_stream_in(input); }
inline void LumiAlgoCreator::cat_print(std::ostream& output, const std::string& padding) { CatShell::Process::cat_print(output, padding); }
inline void LumiAlgoCreator::work_subthread_finished(CatShell::CatPointer<CatShell::WorkThread> child) {}
inline void LumiAlgoCreator::cat_free() { _logics.clear(); }
                /// Free all internal references.

} // namespace DbmShell

#endif

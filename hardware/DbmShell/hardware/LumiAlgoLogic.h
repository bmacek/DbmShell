#ifndef INCLUDE_LUMIMAP32
#define INCLUDE_LUMIMAP32

#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"

#include "DbmShell/hardware/defines.h"

namespace DbmShell {

class LumiAlgoLogic : public CatShell::CatObject
{
        CAT_OBJECT_DECLARE(LumiAlgoLogic, CatShell::CatObject, CAT_NAME_LUMIALGOLOGIC, CAT_TYPE_LUMIALGOLOGIC);
public:
        enum class Operation { op_or, op_and, op_xor };

        LumiAlgoLogic();
                /// Constructor: default.

        virtual ~LumiAlgoLogic();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        void algorithm_fill_simple(std::uint16_t algorithm, std::uint16_t data_source);
                /// Fills particullar algorithm as it would only pass the logic from data-cource.

        void algorithm_enable(std::uint16_t algorithm, bool enable);
                /// Enables/disables particular algorithm, depending

        void algorithm_invert(std::uint16_t algorithm);
                /// Logical inversion of a given algorithm.

        void algorithm_combine(std::uint16_t algorithm, Operation op, CatShell::CatPointer<LumiAlgoLogic> src_1, std::uint16_t alg_1, CatShell::CatPointer<LumiAlgoLogic> src_2, std::uint16_t alg_2);
                /// Combines two algorithms into one with desired operation.

        void algorithm_copy(std::uint16_t algorithm, CatShell::CatPointer<LumiAlgoLogic> source, std::uint16_t source_algorithm);
                /// Copy algorithm from some source.

        bool algorithm_get_mask(std::uint16_t algorithm) const;
                /// Returns the mask for desired algorithm.

        std::uint16_t algorithm_get_mask() const;
                /// Returns entire mask.

        void algorithm_set_mask(std::uint16_t algorithm, bool mask);
                /// Sets the mask of the desired algorithm.

        std::uint16_t algorithm_get_logic_hit(std::uint16_t input) const;
                /// Returns the hit logic combination for cell describing 'input' combination.

        std::uint16_t algorithm_get_logic_live(std::uint16_t input) const;
                /// Returns the live logic combination for cell describing 'input' combination.

protected:

private:

        void format_flags(std::ostream& output, std::uint16_t flags, std::uint8_t width = 16, char yes = '1', char no = '0');
                /// Displays the numbe in binary format with custom characters.

private:

        typedef struct 
        {
                std::uint16_t   hit;
                std::uint16_t   live;
        } LogicCell_t;


        LogicCell_t     _logic[LUMI_ALGORITHM_COMBINATIONS]; // logic
        std::uint16_t   _mask;

};

inline StreamSize LumiAlgoLogic::cat_stream_get_size() const { return sizeof(std::uint16_t) + LUMI_ALGORITHM_COMBINATIONS * sizeof(LogicCell_t); }
inline void LumiAlgoLogic::cat_free() {}
inline std::uint16_t LumiAlgoLogic::algorithm_get_mask() const { return _mask; }
inline std::uint16_t LumiAlgoLogic::algorithm_get_logic_hit(std::uint16_t input) const { return _logic[input].hit; }
inline std::uint16_t LumiAlgoLogic::algorithm_get_logic_live(std::uint16_t input) const { return _logic[input].live; }


} // namespace DbmShell

#endif


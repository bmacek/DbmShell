#ifndef INCLUDE_PIXCOORDPARSER
#define INCLUDE_PIXCOORDPARSER

#include "CatShell/core/Algorithm.h"
#include "CatShell/core/Process.h"
#include "CatShell/core/WorkThread.h"
#include "DbmShell/hardware/defines.h"
#include "CatShell/core/Logger.h"
#include <iostream>
#include <fstream>

//using namespace CatShell;

namespace DbmShell{

class PixCoordParser: public CatShell::Process
{
public:
	PixCoordParser();

	virtual ~PixCoordParser();

	CAT_OBJECT_DECLARE(PixCoordParser, CatShell::Process, CAT_NAME_PIXCOORDPARSER,CAT_TYPE_PIXCOORDPARSER);

	//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
	//----------------------------------------------------------------------
	virtual void* cat_get_part(CatShell::CatAddress& addr);
		///Returns the pointer to the desired part(element).


	// ALGORITHM
	virtual void process_data(CatShell::Algorithm::DataPort port, CatShell::CatPointer<CatShell::CatObject>& object,bool flush, CatShell::Logger&logger);
		/// Process data.
	virtual void algorithm_init();
		/// Defines the process interface and initialises it.
	virtual void algorithm_deinit();
		/// Clears all the process interfaces.
	//virtual CatShell::CatPointer<CatShell::CatObject> produce_data(CatShell::Algorithm::DataPort port, bool& flush, CatShell::Logger&logger);
		/// Produces data on request.Second parameter that is returned if a flush flag.
		/// Object is taken from the first one that producess a result.


	
	//----------------------------------------------

	// PROCESS
	virtual void work_main();
		/// Function doing all the work. User should overload this function
		/// and do all the processing within it.
	virtual void work_init();
		/// Called before the thread starts executing.
	virtual void work_deinit();
		/// Called after the thread stops executing.
	virtual void work_subthread_finished(CatShell::CatPointer<CatShell::WorkThread> child);
		/// Called when a subprocess finished.

private:
	std::ifstream 	_myfile;
	std::string     _file_name;
	DataPort 	_pix_out;
};

inline StreamSize PixCoordParser::cat_stream_get_size() const { return Process::cat_stream_get_size(); }
inline void PixCoordParser::cat_free() {}
inline void PixCoordParser::work_subthread_finished(CatShell::CatPointer<CatShell::WorkThread> child) {}

}
#endif

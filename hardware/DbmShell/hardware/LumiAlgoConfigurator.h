#ifndef INCLUDE_DBMLUMIALGOCONFIGURATOR_INCLUDED
#define INCLUDE_DBMLUMIALGOCONFIGURATOR_INCLUDED

#include "CatShell/core/Process.h"
#include "CatShell/core/MemoryPool.h"

#include "DbmShell/hardware/defines.h"
#include "DbmShell/hardware/BocRegisterCommand.h"
#include "DbmShell/hardware/LumiAlgoLogic.h"

namespace DbmShell {

class LumiAlgoConfigurator : public CatShell::Algorithm
{
        CAT_OBJECT_DECLARE(LumiAlgoConfigurator, CatShell::Algorithm, CAT_NAME_LUMIALGOCONFIGURATOR, CAT_TYPE_LUMIALGOCONFIGURATOR);
public:

        LumiAlgoConfigurator();
                /// Constructor: default.

        virtual ~LumiAlgoConfigurator();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:

private:

        CatShell::MemoryPool<BocRegisterCommand>*  _pool; 

        CatShell::Algorithm::DataPort    _in_port_north;
        CatShell::Algorithm::DataPort    _in_port_south;        
        CatShell::Algorithm::DataPort    _out_port;
};

inline StreamSize LumiAlgoConfigurator::cat_stream_get_size() const { return CatShell::Algorithm::cat_stream_get_size(); }
inline void LumiAlgoConfigurator::cat_stream_out(std::ostream& output) { CatShell::Algorithm::cat_stream_out(output); }
inline void LumiAlgoConfigurator::cat_stream_in(std::istream& input) { CatShell::Algorithm::cat_stream_in(input); }
inline void LumiAlgoConfigurator::cat_print(std::ostream& output, const std::string& padding) { CatShell::Algorithm::cat_print(output, padding); }
inline void LumiAlgoConfigurator::cat_free() {}

} // namespace DbmShell

#endif

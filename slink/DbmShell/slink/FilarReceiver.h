#ifndef INCLUDE_FILARRECEIVER
#define INCLUDE_FILARRECEIVER

#include "CatShell/core/CatPointer.h"
#include "CatShell/core/Process.h"

#include "DbmShell/slink/defines.h"
#include "DbmShell/slink/FilarMemPage.h"

// TILAR/FILAR specifics
#include "rcc_error/rcc_error.h"
#include "ROSfilar/filar.h"


namespace DbmShell {

class FilarReceiver : public CatShell::Process
{
        CAT_OBJECT_DECLARE(FilarReceiver, CatShell::Process, CAT_NAME_FILARRECEIVER, CAT_TYPE_FILARRECEIVER);
public:

        FilarReceiver();
                /// Constructor: default.

        virtual ~FilarReceiver();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flash, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main();
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init();
                /// Called before the thread starts executing.

        virtual void work_deinit();
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatShell::CatPointer<CatShell::WorkThread> child);
                /// Called when a subprocess finished.
//----------------------------------------------------------------------

protected:

        virtual void temp_init();
                /// Called before the thread starts executing.

private:

        bool            _filar_card_ready;      // indicates if the filar card has initialized properly or not
        std::uint32_t   _filar_page_size;       // max. number of bytes per page

        Algorithm::DataPort    _out_port_raw_pages;       // output port for raw data received

        // channel info
        typedef struct
        {
                bool on;     
                std::queue<CatShell::CatPointer<FilarMemPage>>    data;
                std::uint32_t   fifo_capacity;          // number of FIFO spaces offered by the driver
                std::uint32_t   stat_received_pkg;      // number of received packages
                std::uint32_t   stat_error_pkg;         // number of error packages
                std::uint32_t   stat_received_bytes;    // number of received bytes
        } channel_t;

        channel_t       _channels[MAXROLS];
};

inline StreamSize FilarReceiver::cat_stream_get_size() const
{
        return 0;
}

inline void FilarReceiver::cat_stream_out(std::ostream& output)
{
}

inline void FilarReceiver::cat_stream_in(std::istream& input)
{
}

inline void FilarReceiver::process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flash, CatShell::Logger& logger)
{
        // does not receive any data
}

} // namespace DbmShell



#endif

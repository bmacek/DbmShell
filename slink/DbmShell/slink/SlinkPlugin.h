#ifndef INCLUDE_SLINKPLUGIN
#define INCLUDE_SLINKPLUGIN

#include "CatShell/core/Plugin.h"
#include "CatShell/core/Setting.h"

namespace DbmShell {

class SlinkPlugin : public CatShell::Plugin
{
public:

        SlinkPlugin();
                /// Constructor: default.

        virtual ~SlinkPlugin();
                /// Destructor.


//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual bool init();
                /// Prepares everything before plugin can be used. 

        virtual bool deinit();
                /// Cleans everything before plugin can be unloaded. 
//----------------------------------------------------------------------

protected:


private:

};

} // namespace DbmShell

#endif

#ifndef INCLUDE_FILARMEMPAGE
#define INCLUDE_FILARMEMPAGE

#include "CatShell/basics/CatMemPage.h"

#include "DbmShell/slink/defines.h"

#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"

namespace DbmShell {

class FilarMemPage : public CatShell::CatMemPage
        ///
        /// Memory page based on CMEM library, representing continuous phy. memory space, needed for Filar driver.
        ///
{
        CAT_OBJECT_DECLARE(FilarMemPage, CatShell::CatMemPage, CAT_NAME_FILARMEMPAGE, CAT_TYPE_FILARMEMPAGE);

public:

        FilarMemPage();
                /// Constructor: default.

        virtual ~FilarMemPage();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------


        u_long get_memory_physical();
                /// Returns the physical memory address.

        static bool object_init(FilarMemPage* obj);
                /// Does the specific memory initialization.

        static void object_deinit(FilarMemPage* obj);
                /// Does the specific memory deinitialization.

        static void set_filar_memory_page_size(std::uint8_t parameter, std::uint32_t size);
                /// Sets the size of the driver pages.

        static std::uint32_t get_filar_memory_page_size();
                /// Returns the size of the driver pages.

        static std::uint8_t get_filar_memory_page_parameter();
                /// Returns the parameter of the driver pages.

protected:

private: 

        int     _cmem_mem_handle;
        u_long  _cmem_mem_virtual;
        u_long  _cmem_mem_physical;

        // static
        static std::uint8_t    s_page_parameter;
        static std::uint32_t   s_page_size;

};

inline u_long FilarMemPage::get_memory_physical()
{
        return _cmem_mem_physical;
}

inline StreamSize FilarMemPage::cat_stream_get_size() const
{
        return CatShell::CatMemPage::cat_stream_get_size() + sizeof(std::uint32_t);
}

inline void FilarMemPage::cat_stream_out(std::ostream& output)
{
        std::uint32_t length = get_memory_length();

        // write the length
        output.write((char*)&length, sizeof(std::uint32_t));

        if(length > 0)
        {
                // write the data
                CatShell::CatMemPage::cat_stream_out(output);
        }
}

inline void FilarMemPage::cat_stream_in(std::istream& input)
{
        std::uint32_t length;

        // read the length
        input.read((char*)&length, sizeof(std::uint32_t));

        // declare the amount of data
        set_memory_length(length);

        if(length > 0)
        {
                // read data
                CatShell::CatMemPage::cat_stream_in(input);
        }
}

inline void FilarMemPage::set_filar_memory_page_size(std::uint8_t parameter, std::uint32_t size)
{
        s_page_parameter = parameter;
        s_page_size = size;
}

inline std::uint32_t FilarMemPage::get_filar_memory_page_size()
{
        return s_page_size;
}

inline std::uint8_t FilarMemPage::get_filar_memory_page_parameter()
{
        return s_page_parameter;
}


} // namespace DbmShell


#endif

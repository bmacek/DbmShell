#include "DbmShell/slink/FilarReceiver.h"

#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/PluginDeamon.h"


#include <iostream>
#include <chrono>
#include <thread>
#include <queue>

using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(FilarReceiver, CAT_NAME_FILARRECEIVER, CAT_TYPE_FILARRECEIVER);

FilarReceiver::FilarReceiver()
: _filar_card_ready(false)
{
}

FilarReceiver::~FilarReceiver()
{
}

void FilarReceiver::algorithm_init()
{
        Process::algorithm_init();


        // define the output port
        _out_port_raw_pages = declare_output_port("out_pages", CAT_NAME_FILARMEMPAGE);

        // flags
        _filar_card_ready = false;

        // reset the statistics
        for(uint8_t ch=0; ch<MAXROLS; ch++)
        {
                _channels[ch].stat_received_pkg = 0;
                _channels[ch].stat_error_pkg = 0;
                _channels[ch].stat_received_bytes = 0;
        }
}

void FilarReceiver::algorithm_deinit()
{
        Process::algorithm_deinit();

        for(uint8_t ch=0; ch<MAXROLS; ch++)
        {
                while(!_channels[ch].data.empty())
                       _channels[ch].data.pop();
        }
}

void FilarReceiver::work_main()
{
    temp_init();

        FILAR_ErrorCode_t code;
        FILAR_in_t  fifo_addrs;
        FILAR_out_t out_pages;
        std::string msg;

        // get memory pool for raw pages
        MemoryPool<FilarMemPage>* pool = PluginDeamon::get_pool<FilarMemPage>();

        // prepare the channels
        u_int n_free;
        for(uint8_t ch=0; ch<MAXROLS; ch++)
        {
                if(_channels[ch].on)
                {
                        DEBUG(logger(), "Thread: " << std::this_thread::get_id());//REMOVE
                        code = FILAR_InFree(ch, &n_free);
                        if(code != FILAR_SUCCESS)
                        {
                                // failed
                                switch(code)
                                {
                                case FILAR_NOTOPEN:
                                        msg = "The library has not been opened yet.";
                                        break;
                                case FILAR_NOHW:
                                        msg = "The channel selected with the parameter channel does not exist.";
                                        break;
                                default:
                                        msg = "Unknown error!";
                                        break;
                                }
                                // log
                                ERROR(logger(), "Failed to acuqire free space for FIFO on card " << (int)(ch/4) << " channel " << (int)(ch%4) << ". Channel will be disabled. Error: " << msg);

                                // turn off the channel
                                _channels[ch].on = false;
                                continue;
                        }

                        DEBUG(logger(), "Before initialization there are " << pool->get_elements_now() << " raw memory pages (" << pool->get_elements_free() << " free).")

                        //n_free = 1; // force it to 1 for single package mode
                        // remember the capacity
                        _channels[ch].fifo_capacity = n_free;

                        // fill the raw pages
                        fifo_addrs.nvalid = n_free;
                        fifo_addrs.channel = ch;
                        try
                        {
                                for(uint32_t i=0; i<n_free; ++i)
                                {
                                        // get page
                                        CatPointer<FilarMemPage> raw_page = pool->get_element();

                                        // use address
                                        fifo_addrs.pciaddr[i] = raw_page->get_memory_physical();

                                        DEBUG(logger(), "Inputting addr: " << i << " 0x" << hex << fifo_addrs.pciaddr[i] << dec)

                                        // remember in the queue
                                        _channels[ch].data.push(raw_page);
                                }
                        }
                        catch(std::bad_alloc& ex)
                        {

                                // ran out of space
                                ERROR(logger(), "Ran out of memory while filling FIFO. " << n_free << " raw pages were required. on card " << (int)(ch/4) << " channel " << (int)(ch%4) << ". Channel will be disabled.");

                                DEBUG(logger(), "On memory error there are " << pool->get_elements_now() << " raw memory pages (" << pool->get_elements_free() << " free).")

                                // turn off the channel
                                _channels[ch].on = false;
                                // empty the queue
                                while(!_channels[ch].data.empty())
                                {
                                        _channels[ch].data.pop();
                                }

                                DEBUG(logger(), "On memory error clear-up there are " << pool->get_elements_now() << " raw memory pages (" << pool->get_elements_free() << " free).")

                                continue;
                        }

                        DEBUG(logger(), "After initialization there are " << pool->get_elements_now() << " raw memory pages (" << pool->get_elements_free() << " free).")

                        DEBUG(logger(), "Thread: " << std::this_thread::get_id());//REMOVE
                        code = FILAR_PagesIn(&fifo_addrs);
                        if(code != FILAR_SUCCESS)
                        {
                                // failed
                                switch(code)
                                {
                                case FILAR_NOTOPEN:
                                        msg = "The library has not been opened yet.";
                                        break;
                                case FILAR_NOHW:
                                        msg = "The channel selected with the parameter channel does not exist.";
                                        break;
                                case FILAR_EFAULT:
                                        msg = "The driver could not execute the copy_to_user call.";
                                        break;
                                case FILAR_FIFOFULL:
                                        msg = "The IN FIFO of the respective channel became full. Not all PCI addresses have been written.";
                                        break;
                                default:
                                        msg = "Unknown error!";
                                        break;
                                }
                                // log
                                ERROR(logger(), "Failed to set memory addresses on card " << (int)(ch/4) << " channel " << (int)(ch%4) << ". Channel will be disabled. Error: " + msg);

                                // turn off the channel
                                _channels[ch].on = false;
                                // empty the queue
                                while(!_channels[ch].data.empty())
                                        _channels[ch].data.pop();
                                continue;
                        }

                        LOG(logger(), "FIFO on card " << (int)(ch/4) << " channel " << (int)(ch%4) << " will use " << (int)n_free << " raw pages.");
                }
        }

        bool finish = false;
        while(should_continue(false))
        {
                FILAR_Flush();
                // check if some data is available
                for(uint8_t ch=0; ch<MAXROLS; ch++)
                {
                        if(_channels[ch].on)
                        {
                                // channel that is being read-out
                                code = FILAR_PagesOut(ch, &out_pages);

                                if(code != FILAR_SUCCESS)
                                {
                                        // failed
                                        switch(code)
                                        {
                                        case FILAR_NOTOPEN:
                                                msg = "The library has not been opened yet.";
                                                break;
                                        case FILAR_NOHW:
                                                msg = "The channel selected with the parameter channel does not exist.";
                                                break;
                                        default:
                                                msg = "Unknown error!";
                                                break;
                                        }
                                        // log
                                        ERROR(logger(), "Failed to set memory addresses on card " << (int)(ch/4) << " channel " << (int)(ch%4) << ". Channel will be disabled. Error: " + msg);

                                        // turn off the channel
                                        _channels[ch].on = false;
                                        // empty the queue
                                        while(!_channels[ch].data.empty())
                                                _channels[ch].data.pop();
                                        continue;
                                }

                                if(out_pages.nvalid)
                                {
                                    DEBUG(logger(), "Thread: " << std::this_thread::get_id());//REMOVE
                                    DEBUG(logger(), "Received packages on card " << (int)(ch/4) << " channel " << (int)(ch%4) << ": " << out_pages.nvalid)
                                }

                                // check the results and process data
                                for(u_int i=0; i<out_pages.nvalid; ++i)
                                {
                                        DEBUG(logger(), "Get addr: " << i << " 0x" << hex << out_pages.pciaddr[i] << dec)

                                        // check that the proper page has been read
                                        if((_channels[ch].data.front()->get_memory_physical()) == out_pages.pciaddr[i])
                                        {
                                                // check the flags
                                                if(out_pages.fragstat[i] != 0)
                                                {
                                                        // error during package reception
                                                        msg = "Error during package reception. Package will be discarted. Errors: ";
                                                        if(out_pages.fragstat[i] & NO_SCW)
                                                                msg += "Start control word missing. ";
                                                        if(out_pages.fragstat[i] & NO_ECW)
                                                                msg += "End control word missing. ";
                                                        if(out_pages.fragstat[i] & BAD_SCW)
                                                                msg += "Unexpected start control word. ";
                                                        if(out_pages.fragstat[i] & BAD_ECW)
                                                                msg += "Unexpected end control word. ";
                                                        if(out_pages.fragstat[i] & SCW_DTE)
                                                                msg += "Data transmission error in SCW. ";
                                                        if(out_pages.fragstat[i] & SCW_CTE)
                                                                msg += "Control word transmission error in SCW. ";
                                                        if(out_pages.fragstat[i] & ECW_DTE)
                                                                msg += "Data transmission error in ECW. ";
                                                        if(out_pages.fragstat[i] & ECW_CTE)
                                                                msg += "Control word transmission error in ECW. ";

                                                        ERROR(logger(), "Card " << (int)(ch/4) << " channel " << (int)(ch%4) << ": " << msg);

                                                        // statistics
                                                        _channels[ch].stat_error_pkg++;
                                                        DEBUG(logger(), "**** error: " << _channels[ch].stat_error_pkg)

                                                        // prepare the raw page
                                                        _channels[ch].data.front()->set_memory_length(4*out_pages.fragsize[i]); // multiplication by 4, for word->byte
                                                        _channels[ch].data.front()->set_PrintSize(CatMemPage::PrintSize::byte4);                                                       // publish
                                                        publish_data(_out_port_raw_pages, _channels[ch].data.front(), logger());

                                                        _channels[ch].data.pop();
                                                        // go to next packet
                                                        continue;
                                                }
                                                else
                                                {
                                                        // prepare the raw page
                                                        _channels[ch].data.front()->set_memory_length(4*out_pages.fragsize[i]); // multiplication by 4, for word->byte
                                                        _channels[ch].data.front()->set_PrintSize(CatMemPage::PrintSize::byte4);

                                                        // statistics
                                                        _channels[ch].stat_received_pkg++;
                                                        _channels[ch].stat_received_bytes += 4*out_pages.fragsize[i];
                                                        DEBUG(logger(), "**** ok: " << _channels[ch].stat_received_pkg)
                                                        DEBUG(logger(), "**** bytes: " << _channels[ch].stat_received_bytes)

                                                        // publish
                                                        publish_data(_out_port_raw_pages, _channels[ch].data.front(), logger());

                                                        // remove it from the list
                                                        _channels[ch].data.pop();
                                                }
                                        }
                                        else
                                        {
                                                ERROR(logger(), "Raw pages were not read out in sequence. Channel being turned off.");
                                                ERROR(logger(), "ERROR package pci addr: 0x" << hex << out_pages.pciaddr[i] << dec);
                                                ERROR(logger(), "ERROR package fragstat: 0x" << hex << out_pages.fragstat[i] << dec);
                                                ERROR(logger(), "ERROR package fragsize: " << out_pages.fragsize[i]);

                                                // display the statistics
                                                START_LOG_ERROR(logger())
                                                LINE_LOG_ERROR(logger(), "Raw pages were not read out in sequence on card " << (int)(ch/4) << " channel " << (int)(ch%4) << ". " << _channels[ch].data.size() << " raw pages should be in FIFO. Expecting 0x" << hex << out_pages.pciaddr[i] << dec << "(" << i << ")")
                                                while(!_channels[ch].data.empty())
                                                {
                                                        logger().stream() << logger().padding() << "  0x" << hex << (_channels[ch].data.front()->get_memory_physical()) << dec;
                                                        if((_channels[ch].data.front()->get_memory_physical()) == out_pages.pciaddr[i])
                                                        {
                                                                logger().stream() << "  <---";
                                                        }
                                                        logger().stream() << endl;
                                                        _channels[ch].data.pop();
                                                }
                                                END_LOG(logger())

                                                // turn off the channel
                                                _channels[ch].on = false;
                                                // empty the queue
                                                while(!_channels[ch].data.empty())
                                                        _channels[ch].data.pop();
                                                finish = true;
                                                break;
                                        }
                                }
                                if(finish)
                                    break;


                                // input the new raw pages
                                fifo_addrs.nvalid = _channels[ch].fifo_capacity-_channels[ch].data.size();
                                fifo_addrs.channel = ch;
                                if(fifo_addrs.nvalid > 0)
                                {
                                        uint32_t i;
                                        try
                                        {
                                                // prepare the structure
                                                for(i=0; i<fifo_addrs.nvalid; ++i)
                                                {
                                                        // get page
                                                        CatPointer<FilarMemPage> raw_page = pool->get_element();
                                                        // use address
                                                        fifo_addrs.pciaddr[i] = raw_page->get_memory_physical();

                                                        DEBUG(logger(), "Inputting addr: " << i << " 0x" << hex << fifo_addrs.pciaddr[i] << dec)
                                                        // remember in the queue
                                                        _channels[ch].data.push(raw_page);
                                                }
                                        }
                                        catch(std::bad_alloc& ex)
                                        {
                                                ERROR(logger(), "Ran out of raw memory objects. Objects inserted into FIFO on card " << (int)(ch/4) << " channel " << (int)(ch%4) << ": " << i)

                                                fifo_addrs.nvalid = i;
                                        }

                                        // if something to send, send it
                                        if(fifo_addrs.nvalid>0)
                                        {
                                                DEBUG(logger(), "Thread: " << std::this_thread::get_id());//REMOVE
                                                code = FILAR_PagesIn(&fifo_addrs);
                                                if(code != FILAR_SUCCESS)
                                                {
                                                        // failed
                                                        switch(code)
                                                        {
                                                        case FILAR_NOTOPEN:
                                                                msg = "The library has not been opened yet.";
                                                                break;
                                                        case FILAR_NOHW:
                                                                msg = "The channel selected with the parameter channel does not exist.";
                                                                break;
                                                        case FILAR_EFAULT:
                                                                msg = "The driver could not execute the copy_to_user call.";
                                                                break;
                                                        case FILAR_FIFOFULL:
                                                                msg = "The IN FIFO of the respective channel became full. Not all PCI addresses have been written.";
                                                                break;
                                                        default:
                                                                msg = "Unknown error!";
                                                                break;
                                                        }
                                                        // log
                                                        ERROR(logger(), "Failed to set memory addresses on card " << (int)(ch/4) << " channel " << (int)(ch%4) << ". Channel will be disabled. Error: " + msg);

                                                        // turn off the channel
                                                        _channels[ch].on = false;
                                                        // empty the queue
                                                        while(!_channels[ch].data.empty())
                                                                _channels[ch].data.pop();
                                                        continue;
                                                }

                                                DEBUG(logger(), "Send " << fifo_addrs.nvalid << " new raw pages to the FIFO on card " << (int)(ch/4) << " channel " << (int)(ch%4) << ": " << i)
                                        }
                                }

                        }
                }
        }

        char line[200];

        // display the statistics
        START_LOG_LOG(logger())
        LINE_LOG_LOG(logger(), "Filar receiving finished. Statistics:")
        logger().stream() << logger().padding() << "-----------------------------------------------------------------" << endl
                          << logger().padding() << "Card & channel       | Error pkg.   | Ok pkg.      | Bytes       " << endl;
        for(uint8_t ch=0; ch<MAXROLS; ch++)
        {
                sprintf(line, "Card %3u channel %3u | %12u | %12u | %12u", (ch/4), (ch%4), _channels[ch].stat_error_pkg, _channels[ch].stat_received_pkg, _channels[ch].stat_received_bytes);
                logger().stream() << logger().padding() << line << endl;
        }
        logger().stream() << logger().padding() << "-----------------------------------------------------------------" << endl;
        END_LOG(logger())

        // reset the cards, to drop references to memory locations within FIFOs
        LOG(logger(), "Resetting the hw.")
        for(uint8_t ch=0; ch<MAXROLS; ch++)
        {
                if(_channels[ch].on)
                {
                        // reset the channel
                        LOG(logger(), "Resetting card with channel " << (int)ch << "...")
                        code == FILAR_Reset(ch);
                        if(code != FILAR_SUCCESS)
                        {
                                // failed
                                switch(code)
                                {
                                case FILAR_NOTOPEN:
                                        msg = "The library has not been opened yet.";
                                        break;
                                case FILAR_NOHW:
                                        msg = "The channel selected with the parameter channel does not exist.";
                                        break;
                                case FILAR_EFAULT:
                                        msg = "The driver could not execute the copy_to_user call.";
                                        break;
                                default:
                                        msg = "Unknown error!";
                                        break;
                                }

                                // log
                                ERROR(logger(), "Failed resetting card with channel " << (int)ch << ". FilarReceiver will not be usable. Error: " + msg);

                                // exit
                                _filar_card_ready = false;
                        }
                }
        }

        LOG(logger(), "Dropping raw pages.")
        for(uint8_t ch=0; ch<MAXROLS; ch++)
        {
                while(!_channels[ch].data.empty())
                       _channels[ch].data.pop();
        }

        // declare the card unusable
        _filar_card_ready = false;
}

void FilarReceiver::work_init()
{

}

void FilarReceiver::temp_init()
{
        std::string msg;
        FILAR_ErrorCode_t code;
        FILAR_info_t hw_info;
        FILAR_config_t hw_config;

        INFO(logger(), "FilarReceiver preparing...")

        // open the FILAR library
        DEBUG(logger(), "Thread: " << std::this_thread::get_id());//REMOVE
        code = FILAR_Open();
        if(code != FILAR_SUCCESS)
        {
                // failed
                switch(code)
                {
                case FILAR_ERROR_FAIL:
                        msg = "The rcc_error library did not open.";
                        break;
                case FILAR_FILE:
                        msg = "The library did not manage to open the file /dev/filar.";
                        break;
                case FILAR_EFAULT:
                        msg = "The driver failed in the copy_to_user() call.";
                        break;
                case FILAR_MMAP:
                        msg = "The driver failed to map some shared memory to user space.";
                        break;
                case FILAR_USED:
                        msg = "An other application is already using the FILAR.";
                        break;
                default:
                        msg = "Unknown error!";
                        break;
                }

                // log
                ERROR(logger(), "Failed opening FILAR library. FilarReceiver will not be usable. Error: " + msg);

                // exit
                _filar_card_ready = false;
                return;
        }
        LOG(logger(), "Filar library initialized.")

        // inspect the HW
        DEBUG(logger(), "Thread: " << std::this_thread::get_id());//REMOVE
        code == FILAR_Info(&hw_info);
        if(code != FILAR_SUCCESS)
        {
                // failed
                switch(code)
                {
                case FILAR_NOTOPEN:
                        msg = "The library has not been opened yet.";
                        break;
                case FILAR_EFAULT:
                        msg = "The driver could not execute the copy_to_user call/";
                        break;
                default:
                        msg = "Unknown error!";
                        break;
                }

                // log
                ERROR(logger(), "Failed get FILAR HW info. FilarReceiver will not be usable. Error: " + msg);

                // exit
                _filar_card_ready = false;
                return;

        }

        START_LOG_LOG(logger())
        LINE_LOG_LOG(logger(), "HW info for FILAR cards on the machine before reset:")
        logger().stream() << logger().padding() << "Library supports at most " << MAXROLS << " links" << endl
                        << logger().padding() << "Total number of channels: " << hw_info.nchannels << endl
                        << logger().padding() <<  "Channels available : ";
                        for(uint8_t i=0; i<MAXROLS; i++)
                                logger().stream() << (hw_info.channels[i]==1 ? "#" : ".");
                        logger().stream() << endl;
        logger().stream() << logger().padding() <<  "Channels enabled   : ";
                        for(uint8_t i=0; i<MAXROLS; i++)
                                logger().stream() << (hw_info.enabled[i]==1 ? "#" : ".");
                        logger().stream() << endl;
        END_LOG(logger())

        // reset the cards
        LOG(logger(), "Resetting the hw.")
        for(uint8_t i=0; i<MAXROLS; i++)
        {
                if(hw_info.channels[i]==1)
                {
                        // reset the channel
                        LOG(logger(), "Resetting card with channel " << (int)i << "...")
                        code == FILAR_Reset(i);
                        if(code != FILAR_SUCCESS)
                        {
                                // failed
                                switch(code)
                                {
                                case FILAR_NOTOPEN:
                                        msg = "The library has not been opened yet.";
                                        break;
                                case FILAR_NOHW:
                                        msg = "The channel selected with the parameter channel does not exist.";
                                        break;
                                case FILAR_EFAULT:
                                        msg = "The driver could not execute the copy_to_user call/";
                                        break;
                                default:
                                        msg = "Unknown error!";
                                        break;
                                }

                                // log
                                ERROR(logger(), "Failed resetting card with channel " << (int)i << ". FilarReceiver will not be usable. Error: " + msg);

                                // exit
                                _filar_card_ready = false;
                                return;
                        }
                }
        }

        // reset the channels
        for(uint8_t i=0; i<MAXROLS; i++)
        {
                if(hw_info.channels[i]==1)
                {
                        // reset the channel
                        LOG(logger(), "Resetting channel " << (int)i << "...")
                        code == FILAR_LinkReset(i);
                        if(code != FILAR_SUCCESS)
                        {
                                // failed
                                switch(code)
                                {
                                case FILAR_NOTOPEN:
                                        msg = "The library has not been opened yet.";
                                        break;
                                case FILAR_NOHW:
                                        msg = "The channel selected with the parameter channel does not exist.";
                                        break;
                                case FILAR_STUCK:
                                        msg = "The respective link did not come up again after the reset.";
                                        break;
                                case FILAR_EFAULT:
                                        msg = "The driver could not execute the copy_to_user call.";
                                        break;
                                default:
                                        msg = "Unknown error!";
                                        break;
                                }

                                // log
                                ERROR(logger(), "Failed resetting the channel " << (int)i << ". FilarReceiver will not be usable. Error: " + msg);

                                // exit
                                _filar_card_ready = false;
                                return;
                        }
                }
        }

        // inspect the HW
        code == FILAR_Info(&hw_info);
        if(code != FILAR_SUCCESS)
        {
                // failed
                switch(code)
                {
                case FILAR_NOTOPEN:
                        msg = "The library has not been opened yet.";
                        break;
                case FILAR_EFAULT:
                        msg = "The driver could not execute the copy_to_user call.";
                        break;
                default:
                        msg = "Unknown error!";
                        break;
                }

                // log
                ERROR(logger(), "Failed get FILAR HW info. FilarReceiver will not be usable. Error: " + msg);

                // exit
                _filar_card_ready = false;
                return;

        }

        START_LOG_LOG(logger())
        LINE_LOG_LOG(logger(), "HW info for FILAR cards on the machine before initialization:")
        logger().stream() << logger().padding() << "Library supports at most " << MAXROLS << " links" << endl
                        << logger().padding() << "Total number of channels: " << hw_info.nchannels << endl
                        << logger().padding() <<  "Channels available : ";
                        for(uint8_t i=0; i<MAXROLS; i++)
                                logger().stream() << (hw_info.channels[i]==1 ? "#" : ".");
                        logger().stream() << endl;
        logger().stream() << logger().padding() <<  "Channels enabled   : ";
                        for(uint8_t i=0; i<MAXROLS; i++)
                                logger().stream() << (hw_info.enabled[i]==1 ? "#" : ".");
                        logger().stream() << endl;
        END_LOG(logger())


        // turn on the channels
        LOG(logger(), "Initilaizing the hardware.")

        // check the settings are there
        if(!algo_settings().has_element("card_list"))
                algo_settings().add_element(new Setting("plugins"));

        // check the settings and enable corresponding channels
        for(uint8_t i=0; i<MAXROLS; i++)
                hw_config.enable[i] = 0; // disable by default
        for(auto& cr_setting: algo_settings()["card_list"]) // loop through the cards
        {
                if(cr_setting.name() == "card")
                {
                        // get card number
                        std::uint8_t card_number = cr_setting.get_setting<std::uint8_t>("card_number", 0);

                        // check the settings are there
                        if(!cr_setting.has_element("channel_list"))
                                cr_setting.add_element(new Setting("channel_list"));

                        for(auto& ch_setting: cr_setting["channel_list"]) // loop through the lists
                        {
                                if(ch_setting.name() == "channel")
                                {
                                        // get card number
                                        std::uint8_t channel_number = ch_setting.get_setting<std::uint8_t>("channel_number", 0);

                                        hw_config.enable[card_number*4+channel_number] = 1;
                                }
                        }
                }
        }

        // get the page size parameter from the class itself - it was configured through the memory pool
        std::uint8_t page_size = FilarMemPage::get_filar_memory_page_parameter();
        _filar_page_size = FilarMemPage::get_filar_memory_page_size();

        LOG(logger(), "Page size parameter = " << (int)page_size << " coresponding to pages of " << _filar_page_size << " bytes for FILAR pages.");

        hw_config.psize = page_size;
        hw_config.bswap = 0;
        hw_config.wswap = 0;
        hw_config.interrupt = 1;
        hw_config.scw = 0xB0F00000;
        hw_config.ecw = 0xE0F00000;
        hw_config.ccw = 0;

        DEBUG(logger(), "Thread: " << std::this_thread::get_id());//REMOVE
        code == FILAR_Init(&hw_config);
        if(code != FILAR_SUCCESS)
        {
                // failed
                switch(code)
                {
                case FILAR_NOTOPEN:
                        msg = "The library has not been opened yet.";
                        break;
                case FILAR_NOHW:
                        msg = "One of the channels which were set to 1 in enable[] does not exist.";
                        break;
                default:
                        msg = "Unknown error!";
                        break;
                }

                // log
                ERROR(logger(), "Failed initializing the HW. FilarReceiver will not be usable. Error: " + msg);

                // exit
                _filar_card_ready = false;
                return;
        }

        // inspect the HW
        code == FILAR_Info(&hw_info);
        if(code != FILAR_SUCCESS)
        {
                // failed
                switch(code)
                {
                case FILAR_NOTOPEN:
                        msg = "The library has not been opened yet.";
                        break;
                case FILAR_EFAULT:
                        msg = "The driver could not execute the copy_to_user call/";
                        break;
                default:
                        msg = "Unknown error!";
                        break;
                }

                // log
                ERROR(logger(), "Failed get FILAR HW info. FilarReceiver will not be usable. Error: " + msg);

                // exit
                _filar_card_ready = false;
                return;
        }

        // prepare the channels
        for(uint8_t i=0; i<MAXROLS; i++)
                _channels[i].on = (hw_info.enabled[i]==1);

        START_LOG_LOG(logger())
        LINE_LOG_LOG(logger(), "HW info for FILAR cards on the machine after initialization:")
        logger().stream() << logger().padding() << "Library supports at most " << MAXROLS << " links" << endl
                        << logger().padding() << "Total number of channels: " << hw_info.nchannels << endl
                        << logger().padding() <<  "Channels available : ";
                        for(uint8_t i=0; i<MAXROLS; i++)
                                logger().stream() << (hw_info.channels[i]==1 ? "#" : ".");
                        logger().stream() << endl;
        logger().stream() << logger().padding() <<  "Channels enabled   : ";
                        for(uint8_t i=0; i<MAXROLS; i++)
                                logger().stream() << (hw_info.enabled[i]==1 ? "#" : ".");
                        logger().stream() << endl;
        logger().stream() << logger().padding() <<  "Settings enabled   : ";
                        for(uint8_t i=0; i<MAXROLS; i++)
                                logger().stream() << (hw_config.enable[i]==1 ? "#" : ".");
                        logger().stream() << endl;
        END_LOG(logger())

        // ckecking the link statuses
        LOG(logger(), "Checking log status of enabled channels.")
        for(uint8_t i=0; i<MAXROLS; i++)
        {
                if(hw_config.enable[i] == 1)
                {
                        u_int status;
                        code == FILAR_LinkStatus(i, &status);
                        if(code != FILAR_SUCCESS)
                        {
                                // failed
                                switch(code)
                                {
                                case FILAR_NOTOPEN:
                                        msg = "The library has not been opened yet.";
                                        break;
                                case FILAR_NOHW:
                                        msg = "The channel selected with the parameter channel does not exist.";
                                        break;
                                case FILAR_EFAULT:
                                        msg = "The driver could not execute the copy_to_user call.";
                                        break;
                                default:
                                        msg = "Unknown error!";
                                        break;
                                }

                                // log
                                ERROR(logger(), "Failed retrieving the link status for link  " << (int)i << ". FilarReceiver will not be usable. Error: " + msg);

                                // exit
                                _filar_card_ready = false;
                                return;
                        }
                        else
                        {
                                LOG(logger(), "Link status for card " << (int)(i/4) << " link " << (int)(i%4) << ": " << (status==1 ? "UP" : "DOWN"))
                        }
                }
        }

        // declare ready
        _filar_card_ready = true;
}

void FilarReceiver::work_deinit()
{
        std::string msg;
        FILAR_ErrorCode_t code;

        INFO(logger(), "FilarReceiver cleaning...")

        // close the FILAR library
        code = FILAR_Close();
        if(code != FILAR_SUCCESS)
        {
                // failed
                switch(code)
                {
                case FILAR_NOTOPEN:
                        msg = "The library has not been opened yet.";
                        break;
                case FILAR_MUNMAP:
                        msg = "The driver failed to un-map some shared memory from user space.";
                        break;
                default:
                        msg = "Unknown error!";
                        break;
                }

                // log
                ERROR(logger(), "Failed closing FILAR library. Error: " + msg);
        }
        else
        {
                LOG(logger(), "Filar library deinitialized.")
        }


        // declare shutdown
        _filar_card_ready = false;
}

void FilarReceiver::work_subthread_finished(CatPointer<WorkThread> child)
{
	// no sub-treads so nothing to do
}

} // namespace DbmShell

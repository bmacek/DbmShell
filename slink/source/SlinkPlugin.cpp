#include "DbmShell/slink/SlinkPlugin.h"
#include "DbmShell/slink/FilarReceiver.h"
#include "DbmShell/slink/FilarMemPage.h"

#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"
#include "cmem_rcc/cmem_rcc_drv.h"

using namespace std;
using namespace CatShell;

namespace DbmShell {


SlinkPlugin::SlinkPlugin()
{
}

SlinkPlugin::~SlinkPlugin()
{
}

bool SlinkPlugin::init()
{
	if(!Plugin::init())
		return false;

	// set the filar memory size
	std::uint32_t filar_page_size;
	std::string setting_name;

	setting_name = std::string("classes/") + FilarMemPage::s_cat_name() + "/page_size";
    std::uint8_t page_size = settings().get_setting<std::uint8_t>(setting_name, 7);
    switch(page_size)
    {
    case 0: filar_page_size = 256;     break;
    case 1: filar_page_size = 1024;    break;
    case 2: filar_page_size = 2048;    break;
    case 3: filar_page_size = 4096;    break;
    case 4: filar_page_size = 16384;   break;
    case 5: filar_page_size = 65536;   break;
    case 6: filar_page_size = 262144;  break;
    case 7: filar_page_size = 4194296; break;
    default:
            // log
            ERROR(logger(), "Unusable 'page_size' parameter: " << (int)page_size << ". Can be 0 = 256, 1 = 1024, 2 = 2048, 3 = 4096, 4 = 16384, 5 = 65536, 6 = 262144, 7 = 4194296. FilarReceiver will not be usable.");
            // exit
            return false;
    }

    LOG(logger(), "Page size parameter = " << (int)page_size << " coresponding to pages of " << filar_page_size << " bytes for FILAR pages.");
	FilarMemPage::set_filar_memory_page_size(page_size, filar_page_size);

	// open the CMEM interface
	CMEM_Error_code_t result;

	result = CMEM_Open();
	if(result)
	{
		ERROR(logger(), "Could not open CMEM_RCC!")
		return false;
	}

	MemoryPoolBase* pool = get_memory_pool(FilarMemPage::s_cat_name());
	// change the setting of the memory pool (set the max. objects)
	LOG(logger(), "Changed the max. number of elements for '" << FilarMemPage::s_cat_name() << "' to " << (MAX_BUFFS-1))
	setting_name = std::string("classes/") + FilarMemPage::s_cat_name() + "/max_elements";
	* ((ValueSetting<std::uint32_t>*)settings().get_setting_node(setting_name, nullptr)) = (MAX_BUFFS-1);
	// register the init functions
	((MemoryPool<FilarMemPage>*)pool)->set_init_deinit(&FilarMemPage::object_init, &FilarMemPage::object_deinit);

	return true;
}

bool SlinkPlugin::deinit()
{
	if(!Plugin::deinit())
		return false;

	// check that the memory pool is empty and derecister the init functions
	MemoryPoolBase* pool = get_memory_pool(FilarMemPage::s_cat_name());
	pool->drop_unused(); // with this it should be empty
	if(pool->get_elements_now() != 0)
	{
		ERROR(logger(), "There are still " << pool->get_elements_now() << " elements in the FilarMemPage pool!")
		return false;
	}
	((MemoryPool<FilarMemPage>*)pool)->set_init_deinit(nullptr, nullptr);

	// close the CMEM insterface
	CMEM_Error_code_t result;

	result = CMEM_Close();
	if(result)
	{
		ERROR(logger(), "Could not open CMEM_RCC!")
		return false;
	}

	return true;
}

EXPORT_PLUGIN(SlinkPlugin, DbmSlinkPlugin)
START_EXPORT_CLASSES(DbmSlinkPlugin)
EXPORT_CLASS(FilarReceiver, CAT_NAME_FILARRECEIVER, DbmSlinkPlugin)
EXPORT_CLASS(FilarMemPage, CAT_NAME_FILARMEMPAGE, DbmSlinkPlugin)
END_EXPORT_CLASSES(DbmSlinkPlugin)

} // namespace DbmShell
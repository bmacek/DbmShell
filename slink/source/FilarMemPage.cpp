#include "DbmShell/slink/FilarMemPage.h"

using namespace std;
using namespace CatShell;

namespace DbmShell {

std::uint8_t  FilarMemPage::s_page_parameter = 0;
std::uint32_t FilarMemPage::s_page_size = 0;

CAT_OBJECT_IMPLEMENT(FilarMemPage, CAT_NAME_FILARMEMPAGE, CAT_TYPE_FILARMEMPAGE);

FilarMemPage::FilarMemPage()
{
}

FilarMemPage::~FilarMemPage()
{
}

void FilarMemPage::cat_print(std::ostream& output, const std::string& padding)
{
        CatMemPage::cat_print(output, padding);
}

bool FilarMemPage::object_init(FilarMemPage* obj)
{
        CMEM_Error_code_t result;

        // allocate the memory
        result = CMEM_SegmentAllocate(s_page_size, (char*)"FILAR_BUFFER", &(obj->_cmem_mem_handle));
        if(result)
                return false;
        // get the virtual address
        result = CMEM_SegmentVirtualAddress(obj->_cmem_mem_handle, &(obj->_cmem_mem_virtual));
        if(result)
                return false;
        // get the physical addres
        result = CMEM_SegmentPhysicalAddress(obj->_cmem_mem_handle, &(obj->_cmem_mem_physical));
        if(result)
                return false;

        obj->set_memory((std::uint8_t*)(obj->_cmem_mem_virtual), s_page_size, 0);

        //cout << "Assigned new mem. section: " << obj->_cmem_mem_handle << "\t" << obj->_cmem_mem_virtual << "\t" << obj->_cmem_mem_physical << endl;

        return true;
}

void FilarMemPage::object_deinit(FilarMemPage* obj)
{
        CMEM_Error_code_t result;
 
        /// has it already been deinit
        //if(obj->_cmem_mem_handle == 0 && obj->_cmem_mem_virtual == 0 && obj->_cmem_mem_physical == 0)
        //        return;

        //cout << "Removing mem. section: " << obj->_cmem_mem_handle << "\t" << obj->_cmem_mem_virtual << "\t" << obj->_cmem_mem_physical << endl;

        result = CMEM_SegmentFree(obj->_cmem_mem_handle);
        obj->set_memory(nullptr, 0, 0);
        obj->_cmem_mem_handle = 0;
        obj->_cmem_mem_virtual = 0;
        obj->_cmem_mem_physical = 0;
}

} // namespace DbmShell

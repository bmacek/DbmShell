cmake_minimum_required(VERSION 2.8)

project(DbmSlink CXX)

set(CPP_FILES source/FilarReceiver.cpp source/SlinkPlugin.cpp source/FilarMemPage.cpp)

include_directories(${PROJECT_SOURCE_DIR})

if(EXISTS "$ENV{TDAQ_INST_PATH}")
		include_directories($ENV{TDAQ_INST_PATH}/include)
		LINK_DIRECTORIES(${LINK_DIRECTORIES} $ENV{TDAQ_INST_PATH}/$ENV{CMTCONFIG}/lib)
else()
        message("TDAQ should be setup.")
endif(EXISTS "$ENV{TDAQ_INST_PATH}")

add_library(${PROJECT_NAME}Plugin SHARED ${CPP_FILES})
target_link_libraries(${PROJECT_NAME}Plugin CatShellCore CatBasicsPlugin dl filar cmem_rcc)

set(${PROJECT_NAME}_INCLUDE_DIRS ${PROJECT_SOURCE_DIR} CACHE INTERNAL "${PROJECT_NAME}: Include Directories" FORCE)

install (TARGETS ${PROJECT_NAME}Plugin DESTINATION lib)
#install (DIRECTORY ${PROJECT_SOURCE_DIR}/DbmShell DESTINATION include)

#ifndef INCLUDE_DBMBOCLINK_INCLUDED
#define INCLUDE_DBMBOCLINK_INCLUDED

#include "CatShell/core/Algorithm.h"
#include "CatShell/core/MemoryPool.h"

#include "DbmShell/hardware/BocRegisterCommand.h"
#include "DbmShell/hardware/FEI4Package.h"

#include "DbmShell/boclib/defines.h"

#include "Boc.h"
#include "Bmf.h"
#include "Bcf.h"

namespace DbmShell {

class BocLink : public CatShell::Algorithm
{
        CAT_OBJECT_DECLARE(BocLink, CatShell::Algorithm, CAT_NAME_BOCLINK, CAT_TYPE_BOCLINK);
public:

        enum class bmf_fpga : std::uint8_t { north = 0, south = 1};


        BocLink();
                /// Constructor: default.

        virtual ~BocLink();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:

private:

        void tx_fifo_transmit_off(bmf_fpga fpga, std::uint8_t channel);
                /// Turns on TX FIFO transmit.

        void tx_fifo_transmit_on(bmf_fpga fpga, std::uint8_t channel);
                /// Turns off TX FIFO transmit.

        void tx_fifo_wait_empty(bmf_fpga fpga, std::uint8_t channel);
                /// Waits for the TX FIFO to transmit all of the data.

        void rx_fifo_clear(bmf_fpga fpga, std::uint8_t channel);
                /// Discards all the data in the RX FIFO.

        void rx_fifo_receive_on(bmf_fpga fpga, std::uint8_t channel);
                /// Turns on the RX FIFO which starts recording. 

        void rx_fifo_receive_off(bmf_fpga fpga, std::uint8_t channel);
                /// Ends the recording of the data in the monitoring FIFO.

        void rx_fifo_read_package(bmf_fpga fpga, std::uint8_t channel, CatShell::CatPointer<FEI4Package>& pkg, CatShell::Logger& logger);
                /// Read the package from the RX FIFO.

private:

        Boclib::Boc*    _boc; // BOC connection
        Boclib::Bcf*    _bcf; // bcf FPGA
        Boclib::Bmf*    _bmf[2]; // north FPGA

        CatShell::Algorithm::DataPort  _in_port_cmd;
        CatShell::Algorithm::DataPort  _in_port_fe;
        CatShell::Algorithm::DataPort  _in_port_fe_broadcast;
        CatShell::Algorithm::DataPort  _out_port_read;
        CatShell::Algorithm::DataPort  _out_port_fe_read;

        CatShell::MemoryPool<BocRegisterCommand>*  _pool; 
        CatShell::MemoryPool<FEI4Package>*  _pool_fe4pkg; 

        // register memory
        std::uint8_t    _tx_reg[2][FEI4_PER_BOC/2][32];
        std::uint8_t    _rx_reg[2][FEI4_PER_BOC/2][32];
};

inline StreamSize BocLink::cat_stream_get_size() const { return CatShell::Algorithm::cat_stream_get_size(); }
inline void BocLink::cat_stream_out(std::ostream& output) { CatShell::Algorithm::cat_stream_out(output); }
inline void BocLink::cat_stream_in(std::istream& input) { CatShell::Algorithm::cat_stream_in(input); }
inline void BocLink::cat_print(std::ostream& output, const std::string& padding) { CatShell::Algorithm::cat_print(output, padding); }
inline void BocLink::cat_free() {}

} // namespace DbmShell

#endif

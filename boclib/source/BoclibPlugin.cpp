#include "CatShell/core/Plugin.h"
#include "DbmShell/boclib/BocLink.h"

using namespace std;

namespace DbmShell {

EXPORT_PLUGIN(CatShell::Plugin, DbmBoclibPlugin)
START_EXPORT_CLASSES(DbmBoclibPlugin)
EXPORT_CLASS(BocLink, CAT_NAME_BOCLINK, DbmBoclibPlugin)
END_EXPORT_CLASSES(DbmBoclibPlugin)

} // namespace DbmShell
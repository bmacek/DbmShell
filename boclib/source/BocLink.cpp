#include "DbmShell/boclib/BocLink.h"

#include "DbmShell/hardware/defines.h"
#include "DbmShell/hardware/FEI4Message.h"

#include "CatShell/core/PluginDeamon.h"

using namespace std;
using namespace CatShell;
using namespace Boclib;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(BocLink, CAT_NAME_BOCLINK, CAT_TYPE_BOCLINK);

BocLink::BocLink()
{
}

BocLink::~BocLink()
{
}

void BocLink::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
	// processing input data
	if(object.isSomething() && port == _in_port_cmd)
	{
		CatPointer<BocRegisterCommand> cmd = object.unsafeCast<BocRegisterCommand>();

		// FPGA
		Bmf* fpga;

		if(cmd->get_fpga() == BocFpga::North)
			fpga = _bmf[(std::uint8_t)bmf_fpga::north];
		else if(cmd->get_fpga() == BocFpga::South)
			fpga = _bmf[(std::uint8_t)bmf_fpga::south];
		else if(cmd->get_fpga() == BocFpga::Bcf)
		{
			// command type
			if(cmd->get_command_type() == BocCommand::Write)
			{
				_bcf->WriteRegister(cmd->get_register(), cmd->getData());

				Algorithm::process_data(port, object, flush, logger);
				return;
			}
			else if(cmd->get_command_type() == BocCommand::Read)
			{
				// reading register
				std::uint16_t value;
				value = _bcf->ReadRegister(cmd->get_register());

				// get the response command
				CatPointer<BocRegisterCommand> rsp = _pool->get_element();
				rsp->set_command(cmd->get_command_type(), cmd->get_fpga(), cmd->get_bank(), cmd->get_register(), value);

				// publish
				publish_data(_out_port_read, rsp, logger);

				Algorithm::process_data(port, object, flush, logger);
				return;
			}
			else
			{
				// unknown command type
				ERROR(logger, "Unknown command type. Skipping it.");
				Algorithm::process_data(port, object, flush, logger);
				return;				
			}
		}
		else
		{
			ERROR(logger, "Unknown target FPGA. Skipping command.");
			Algorithm::process_data(port, object, flush, logger);
			return;
		}

		// address
		std::uint16_t reg;
		if(cmd->get_bank() == BmfRegisterBank::General)
			reg = Bmf::RefOffs_General;
		else if(cmd->get_bank() == BmfRegisterBank::Rx)
			reg = Bmf::RegOffs_Rx;
		else if(cmd->get_bank() == BmfRegisterBank::Tx)
			reg = Bmf::RegOffs_Tx;
		else if(cmd->get_bank() == BmfRegisterBank::Dbm)
			reg = Bmf::RefOffs_Dbm;		
		else
		{
			ERROR(logger, "Error: unrecognised register bank. Skipping command.");
			Algorithm::process_data(port, object, flush, logger);
			return;
		}
		reg |= (cmd->get_register() & 0x03ff); 

		// command type
		if(cmd->get_command_type() == BocCommand::Write)
		{
			// writing register
			fpga->WriteRegister(reg, cmd->getData());
		}
		else if(cmd->get_command_type() == BocCommand::Read)
		{
			// reading register
			std::uint16_t value;
			value = fpga->ReadRegister(reg);

			// get the response command
			CatPointer<BocRegisterCommand> rsp = _pool->get_element();
			rsp->set_command(cmd->get_command_type(), cmd->get_fpga(), cmd->get_bank(), cmd->get_register(), value);

			// publish
			publish_data(_out_port_read, rsp, logger);
		}
		else
		{
			// unknown command type
			ERROR(logger, "Unknown command type. Skipping it.");
			Algorithm::process_data(port, object, flush, logger);
			return;
		}
	}
	else if(object.isSomething() && (port == _in_port_fe_broadcast))
	{
		CatPointer<FEI4Command> cmd = object.unsafeCast<FEI4Command>();

		// one of the front end commands
		std::uint8_t channel = 16;	
		std::uint16_t reg;
		reg = Bmf::RegOffs_Tx + BOC_REGISTERS_PER_TX*channel + BOC_TX_REGISTER_FIFO_DATA; // write to fifo

		if(cmd->get_type() == FEI4Command::type::RdReg)
		{
			// broadcast of read command not supported
			ERROR(logger, "FE read command broadcast not supported. Skipping it.");
		}
		else
		{
			// FPGA
			bmf_fpga fpga = bmf_fpga::north;

			// write the data to FIFO
			for(int i=0; i<cmd->size(); ++i)
			{
				_bmf[(std::uint8_t)bmf_fpga::north]->WriteRegister(reg, cmd->data_element(i));
				_bmf[(std::uint8_t)bmf_fpga::south]->WriteRegister(reg, cmd->data_element(i));
				//printf("--> 0x%04x 0x%02x\n", reg & 0x3ff, cmd->data_element(i));
			}

			// only write

			// get data to the FE
			for(int i=0; i<(FEI4_PER_BOC/2); ++i)
			{
				tx_fifo_transmit_on(bmf_fpga::north, i);
				tx_fifo_transmit_on(bmf_fpga::south, i);
			}
			for(int i=0; i<(FEI4_PER_BOC/2); ++i)
			{
				tx_fifo_wait_empty(bmf_fpga::north, i);
				tx_fifo_transmit_off(bmf_fpga::north, i);				
				tx_fifo_wait_empty(bmf_fpga::south, i);
				tx_fifo_transmit_off(bmf_fpga::south, i);				
			}
		}		
	}
	else if(object.isSomething() && (port == _in_port_fe))
	{
		CatPointer<FEI4Message> cmd = object.unsafeCast<FEI4Message>();

		// one of the front end commands
		bmf_fpga     fpga;
		if(cmd->get_fpga() == BocFpga::North)
			fpga = bmf_fpga::north;
		else if(cmd->get_fpga() == BocFpga::South)
			fpga = bmf_fpga::south;
		else
		{
			ERROR(logger, "Unknown FPGA. Skipping it.");
			Algorithm::process_data(port, object, flush, logger);
			return;
		}
		std::uint8_t tx   = cmd->get_tx();
		std::uint8_t rx   = cmd->get_rx();

		std::uint16_t reg;
		reg = Bmf::RegOffs_Tx + BOC_REGISTERS_PER_TX*tx + BOC_TX_REGISTER_FIFO_DATA; // write to fifo

		// write the data to FIFO
		for(int i=0; i<cmd->size(); ++i)
		{
			_bmf[(std::uint8_t)fpga]->WriteRegister(reg, cmd->data_element(i));
			//printf("--> 0x%04x 0x%02x\n", reg & 0x3ff, cmd->data_element(i));
		}

		if(cmd->get_type() == FEI4Command::type::RdReg)
		{
			// issued command was reading the register so collect the response 

			// clear the RX FIFO
			rx_fifo_clear(fpga, rx);

			// prepare RX FIFO to get the data
			rx_fifo_receive_on(fpga, rx);

			// get data to the FE
			tx_fifo_transmit_on(fpga, tx);
			tx_fifo_wait_empty(fpga, tx);
			tx_fifo_transmit_off(fpga, tx);

			// read response package
			CatShell::CatPointer<FEI4Package> pkg = _pool_fe4pkg->get_element();
			rx_fifo_read_package(fpga, rx, pkg, logger);

			// turn off RX FIFO to get the data
			rx_fifo_receive_off(fpga, rx);

			// publish
			publish_data(_out_port_fe_read + rx, pkg, logger);
		}
		else
		{
			// only write

			// get data to the FE
			tx_fifo_transmit_on(fpga, tx);
			tx_fifo_wait_empty(fpga, tx);
			tx_fifo_transmit_off(fpga, tx);
		}
	}

	Algorithm::process_data(port, object, flush, logger);
}

void BocLink::rx_fifo_clear(bmf_fpga fpga, std::uint8_t channel)
{
	std::uint16_t reg_low, reg_high, reg_status;
	std::uint8_t read_high, read_low;

	reg_low = Bmf::RegOffs_Rx + BOC_REGISTERS_PER_RX*channel + BOC_RX_REGISTER_FIFO_DATA_LOW;// 
	reg_high = Bmf::RegOffs_Rx + BOC_REGISTERS_PER_RX*channel + BOC_RX_REGISTER_FIFO_DATA_HIGH;// 
	reg_status = Bmf::RegOffs_Rx + BOC_REGISTERS_PER_RX*channel + BOC_RX_REGISTER_STATUS;// 

	while(!(_bmf[(std::uint8_t)fpga]->ReadRegister(reg_status) & 0x20))// while empty
	{
		read_high = _bmf[(std::uint8_t)fpga]->ReadRegister(reg_high);
		read_low = _bmf[(std::uint8_t)fpga]->ReadRegister(reg_low);
	}
}

void BocLink::rx_fifo_read_package(bmf_fpga fpga, std::uint8_t channel, CatShell::CatPointer<FEI4Package>& pkg, CatShell::Logger& logger)
{
	std::uint16_t reg_low, reg_high, reg_status;
	std::uint8_t read_high, read_low;

	reg_low = Bmf::RegOffs_Rx + BOC_REGISTERS_PER_RX*channel + BOC_RX_REGISTER_FIFO_DATA_LOW;// 
	reg_high = Bmf::RegOffs_Rx + BOC_REGISTERS_PER_RX*channel + BOC_RX_REGISTER_FIFO_DATA_HIGH;// 
	reg_status = Bmf::RegOffs_Rx + BOC_REGISTERS_PER_RX*channel + BOC_RX_REGISTER_STATUS;// 
	
	// read until reaching SOF
	do
	{
		while(_bmf[(std::uint8_t)fpga]->ReadRegister(reg_status) & 0x20)// while empty
		{
		}
		read_high = _bmf[(std::uint8_t)fpga]->ReadRegister(reg_high);
		read_low = _bmf[(std::uint8_t)fpga]->ReadRegister(reg_low);
		//printf("B: %02x %02x\n", read_high, read_low);

		if(read_high & 0x7E)
		{
			// error during reading
			ERROR(logger, "RX FIFO read with error flag 0x" << hex << read_high << " and data 0x" << hex << read_low);
		}
	}
	while(!((!((read_high & 0x7E)))&& (read_high & 0x01) && (read_low == FE_SOF))); // SOF
	pkg->clear();

	// read data until EOF
	do
	{
		while(_bmf[(std::uint8_t)fpga]->ReadRegister(reg_status) & 0x20)// while empty
		{
		}
		read_high = _bmf[(std::uint8_t)fpga]->ReadRegister(reg_high);
		read_low = _bmf[(std::uint8_t)fpga]->ReadRegister(reg_low);
		//printf("P: %02x %02x\n", read_high, read_low);

		if(read_high & 0x7E)
		{
			// error during reading
			ERROR(logger, "RX FIFO read with error flag 0x" << hex << read_high << " and data 0x" << hex << read_low);
		}
		else if(read_high & 0x01)
		{
			if(read_low == FE_EOF) // EOF
				break;
			else if(read_low == FE_SOF)
			{
				// new package ?!
				ERROR(logger, "Second SOF encountered. Dropping data until now! Recreating package.");
				pkg->clear();
			}
		}
		else
		{
			// data word
			pkg->add_data(read_low);
		}
	}
	while(true);
}

void BocLink::rx_fifo_receive_on(bmf_fpga fpga, std::uint8_t channel)
{
	std::uint16_t reg = Bmf::RegOffs_Rx + BOC_REGISTERS_PER_RX*channel + BOC_RX_REGISTER_CONTROL;

	// remebmer prev. state
	_rx_reg[(std::uint8_t)fpga][channel][BOC_RX_REGISTER_CONTROL] = _bmf[(std::uint8_t)fpga]->ReadRegister(reg);

	// write new state
	std::uint8_t val = _rx_reg[(std::uint8_t)fpga][channel][BOC_RX_REGISTER_CONTROL];
	val |= 0x01 /*enable rx*/  | 0x04 /*FIFO enable*/;
	val &= 0xf7 /*monitor 8/10 decoded*/; 
	val &= 0xef /*idles off*/;
	val &= 0x1f /*source 0*/;

	_bmf[(std::uint8_t)fpga]->WriteRegister(reg, val);
}

void BocLink::rx_fifo_receive_off(bmf_fpga fpga, std::uint8_t channel)
{
	// return the prev. state of register
	std::uint16_t reg = Bmf::RegOffs_Rx + BOC_REGISTERS_PER_RX*channel + BOC_RX_REGISTER_CONTROL;
	_bmf[(std::uint8_t)fpga]->WriteRegister(reg, _rx_reg[(std::uint8_t)fpga][channel][BOC_RX_REGISTER_CONTROL]);
}

void BocLink::tx_fifo_transmit_on(bmf_fpga fpga, std::uint8_t channel)
{
	std::uint16_t reg = Bmf::RegOffs_Tx + BOC_REGISTERS_PER_TX*channel + BOC_TX_REGISTER_CONTROL_1;

	// remebmer prev. state
	_tx_reg[(std::uint8_t)fpga][channel][BOC_TX_REGISTER_CONTROL_1] = _bmf[(std::uint8_t)fpga]->ReadRegister(reg);

	// write new state
	std::uint8_t val = _tx_reg[(std::uint8_t)fpga][channel][BOC_TX_REGISTER_CONTROL_1];
	val |= 0x01 /*enable tx*/  | 0x04 /*FIFO transmit*/ | 0x40 /*serial FIFO source*/;
	val &= 0xef /*BPM on*/; 
	val &= 0xfd /*8/10 encoding off*/;

	_bmf[(std::uint8_t)fpga]->WriteRegister(reg, val);
}

void BocLink::tx_fifo_transmit_off(bmf_fpga fpga, std::uint8_t channel)
{
	// return the prev. state of register
	std::uint16_t reg = Bmf::RegOffs_Tx + BOC_REGISTERS_PER_TX*channel + BOC_TX_REGISTER_CONTROL_1;
	_bmf[(std::uint8_t)fpga]->WriteRegister(reg, _tx_reg[(std::uint8_t)fpga][channel][BOC_TX_REGISTER_CONTROL_1]);
}

void BocLink::tx_fifo_wait_empty(bmf_fpga fpga, std::uint8_t channel)
{
	std::uint16_t reg = Bmf::RegOffs_Tx + BOC_REGISTERS_PER_TX*channel + BOC_TX_REGISTER_STATUS;

	while(!(_bmf[(std::uint8_t)fpga]->ReadRegister(reg) & 0x01 /*FIFO empty*/))
	{
	}
}

void BocLink::algorithm_init()
{
	Algorithm::algorithm_init();

	_boc = nullptr;

	// open the connection to BOC
	_boc = new Boc(algo_settings().get_setting<std::string>("ip", "192.168.0.1")); // TODO: get it from settings

	// connect both FPGAs
	_bcf = new Bcf(*_boc);
	_bmf[(std::uint8_t)bmf_fpga::north] = new Bmf(*_boc, Bmf::Target::North);
	_bmf[(std::uint8_t)bmf_fpga::south] = new Bmf(*_boc, Bmf::Target::South);

	// get the memory pool
    _pool = PluginDeamon::get_pool<BocRegisterCommand>();
    _pool_fe4pkg = PluginDeamon::get_pool<FEI4Package>();

    // declare the input ports
    _in_port_cmd = declare_input_port("in_command", CAT_NAME_BOCREGISTERCOMMAND);
	_in_port_fe = declare_input_port("in_fe", CAT_NAME_FEI4MESSAGE);
	_in_port_fe_broadcast = declare_input_port("in_fe_broadcast", CAT_NAME_FEI4COMMAND);

    // declare the output ports
    _out_port_read = declare_output_port("out_value", CAT_NAME_BOCREGISTERCOMMAND);
    _out_port_fe_read = declare_output_port("out_fe_value", CAT_NAME_FEI4PACKAGE, FEI4_PER_BOC);
}

void BocLink::algorithm_deinit()
{
	// remove FPGAs
	delete _bmf[(std::uint8_t)bmf_fpga::north];
	delete _bmf[(std::uint8_t)bmf_fpga::south];
	delete _bcf;
	_bcf = nullptr;
	_bmf[(std::uint8_t)bmf_fpga::north] = _bmf[(std::uint8_t)bmf_fpga::south] = nullptr;

	// remove the conneciton
	delete _boc;
	_boc = nullptr;

	Algorithm::algorithm_deinit();
}

} // namespace DbmShell

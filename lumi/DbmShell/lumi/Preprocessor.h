#ifndef INCLUDE_DBMPREPROCESSOR
#define INCLUDE_DBMPREPROCESSOR

#include "CatShell/core/TimePoint.h"
#include "CatShell/core/Algorithm.h"
#include "CatShell/core/MemoryPool.h"

#include "DbmShell/lumi/defines.h"
#include "DbmShell/lumi/RawH.h"

namespace DbmShell {

class DbmPreprocessor : public CatShell::Algorithm
{
        CAT_OBJECT_DECLARE(DbmPreprocessor, CatShell::Algorithm, CAT_NAME_DBMPREPROCESSOR, CAT_TYPE_DBMPREPROCESSOR);
public:

        DbmPreprocessor();
                /// Constructor: default.

        virtual ~DbmPreprocessor();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

//----------------------------------------------------------------------

protected:

private:

        typedef struct
        {
                std::uint32_t   start;
                std::uint32_t   end;
                std::uint16_t   readout_number;
                CatShell::TimePoint     time;
        } Interval_t;

        Interval_t      _sync_data[DBM_MAX_SYNC_SOURCES];

        CatShell::MemoryPoolBase*        _pool; // memory pool RawH objects

        CatShell::Algorithm::DataPort    _in_port_raw_pages;    // input port for raw data received
        CatShell::Algorithm::DataPort    _out_port_rawh;        // output port for rawh data
};

inline StreamSize DbmPreprocessor::cat_stream_get_size() const
{
        return CatShell::Algorithm::cat_stream_get_size();
}

inline void DbmPreprocessor::cat_stream_out(std::ostream& output)
{
        CatShell::Algorithm::cat_stream_out(output);
}

inline void DbmPreprocessor::cat_stream_in(std::istream& input)
{
        CatShell::Algorithm::cat_stream_in(input);
}

inline void DbmPreprocessor::cat_print(std::ostream& output, const std::string& padding)
{
        CatShell::Algorithm::cat_print(output, padding);
}

} // namespace DbmShell

#endif

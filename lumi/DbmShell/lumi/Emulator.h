#ifndef INCLUDE_DBMEMULATOR
#define INCLUDE_DBMEMULATOR

#include "CatShell/core/Process.h"

#include "DbmShell/lumi/defines.h"

namespace DbmShell {

class DbmEmulator : public CatShell::Process
{
        CAT_OBJECT_DECLARE(DbmEmulator, CatShell::Process, CAT_NAME_DBMEMULATOR, CAT_TYPE_DBMEMULATOR);
public:

        DbmEmulator();
                /// Constructor: default.

        virtual ~DbmEmulator();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main();
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init();
                /// Called before the thread starts executing.

        virtual void work_deinit();
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatShell::CatPointer<CatShell::WorkThread> child);
                /// Called when a subprocess finished.
//----------------------------------------------------------------------

protected:

private:

        CatShell::Algorithm::DataPort    _out_port_raw_pages;       // output port for raw data received
};

inline StreamSize DbmEmulator::cat_stream_get_size() const
{
        return CatShell::Process::cat_stream_get_size();
}

inline void DbmEmulator::cat_stream_out(std::ostream& output)
{
        CatShell::Process::cat_stream_out(output);
}

inline void DbmEmulator::cat_stream_in(std::istream& input)
{
        CatShell::Process::cat_stream_in(input);
}

inline void DbmEmulator::cat_print(std::ostream& output, const std::string& padding)
{
        CatShell::Process::cat_print(output, padding);
}

} // namespace DbmShell

#endif

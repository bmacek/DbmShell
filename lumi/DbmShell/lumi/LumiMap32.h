#ifndef INCLUDE_LUMIMAP32
#define INCLUDE_LUMIMAP32

#include "CatShell/core/TimePoint.h"

#include "DbmShell/lumi/defines.h"
#include "DbmShell/lumi/RawH.h"
#include "DbmShell/lumi/MapIdentity.h"

namespace DbmShell {

class LumiMap32 : public CatShell::CatObject
{
        CAT_OBJECT_DECLARE(LumiMap32, CatShell::CatObject, CAT_NAME_LUMIMAP32, CAT_TYPE_LUMIMAP32);
public:

        LumiMap32();
                /// Constructor: default.

        virtual ~LumiMap32();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

        virtual void* cat_get_part(CatShell::CatAddress& addr);
                /// Returns the pointer to the desired part.
//----------------------------------------------------------------------

        void construct_from_rawh(CatShell::CatPointer<RawH>& before, CatShell::CatPointer<RawH>& after, std::uint16_t bcid_offset);
                /// Calculates map from start and end values of counters. This function performs no checks
                /// on the consistency between both RawHs. The values are taken from 'after' RawH.
                /// 'bcid_offset' causes data from RawH map BCID=0, to be mapped to BCID='bcid_offset'
                /// of the LumiMap32.

protected:

private:

        void refresh_accumulation();
                /// Refresh the accumulated statistics.
private:

        MapIdentity _identity;

        std::uint32_t   _error_count;
        std::uint32_t   _period_start;
        std::uint32_t   _period_end;

        CatShell::TimePoint     _time_start;
        CatShell::TimePoint     _time_end;

        std::uint32_t   _counts_hit[ORBIT_LENGTH];
        std::uint32_t   _counts_live[ORBIT_LENGTH];

        std::uint64_t   _accumulation_hits; // accumulated number of hits
        std::uint16_t   _accumulation_count; // number of BCIDs with some hits
        std::uint16_t   _accumulation_dead; // accumulated number of dead
        bool            _accumulation_valid;
};

inline void LumiMap32::cat_free() {}
inline StreamSize LumiMap32::cat_stream_get_size() const { return _identity.cat_stream_get_size() + 3*sizeof(std::uint32_t) + ORBIT_LENGTH*2*sizeof(uint32_t) + _time_start.get_stream_length() + _time_end.get_stream_length(); }

} // namespace DbmShell

#endif

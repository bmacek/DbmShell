#ifndef INCLUDE_RAWH
#define INCLUDE_RAWH

#include <ctime>

#include "CatShell/core/TimePoint.h"
#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"
#include "CatShell/core/PluginDeamon.h"

#include "CatShell/basics/CatMemPage.h"

#include "DbmShell/lumi/defines.h"
#include "DbmShell/lumi/MapIdentity.h"

namespace DbmShell {

class RawH : public CatShell::CatObject
{
        CAT_OBJECT_DECLARE(RawH, CatShell::CatObject, CAT_NAME_RAWH, CAT_TYPE_RAWH);
public:

        RawH();
                /// Constructor: default.

        virtual ~RawH();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        //
        // functions for setting the maps
        //
        void define_header(bool reset, BocFpga fpga, BocStream stream, std::uint8_t map_number);
                /// Defines the map header.

        void define_period(std::uint16_t overflow_number, std::uint16_t readout_number, std::uint32_t previous_end_orbit, std::uint32_t end_orbit);
                /// Defines the period and status counters.

        void define_counters(std::uint16_t bcid, std::uint16_t hit_count, std::uint16_t live_count);
                /// Defines the counters for desired BCID.

	void add_count(std::uint16_t bcid, bool add_hit, bool add_live);
		/// Increases 'live' or 'hit' counter for one for a given BCID.

        //
        // function for reading the maps
        //
        void get_version_date(std::uint16_t& year, std::uint8_t& month, std::uint8_t& day, std::uint8_t& hour, std::uint8_t& minute, std::uint8_t& second);
                /// Return the date of the version freeze of the data source.

        bool was_lumi_reset();
                /// Returns the lumi was reset before this readout.

        std::uint32_t data_source();
                /// Returns the source of the data.

        BocFpga fpga_source();
                /// Returns from which FPGA the data originates.

        std::uint8_t streamID();
                /// Returns the stream from which the map arrived.

        std::uint8_t map_number();
                /// Returns the serial number of the map.

        std::uint16_t overflow_number();
                /// Get the number of overflows.

        std::uint16_t readout_number();
                /// Get the consequential number of the readout.

        std::uint32_t readout_start();
                /// Get the orbit before the interval.

        std::uint32_t readout_end();
                /// Get the last orbit of the interval.

        const CatShell::TimePoint& timestamp() const;
                /// Returns time marking the end of accumulation period.

        void set_time(const CatShell::TimePoint& time);
                /// Sets the time associated with this reading.

        void get_counters_for_bcid(std::uint16_t bcid, std::uint16_t& hit_counter, std::uint16_t& live_counter);
                /// Returns the counters for desired bcid.

        void fill_identity(MapIdentity& identity);
                /// Identity of the map is set into 'identity'.

        //
        // setting the map (do not use if you don't know what you are doing)
        //
        void set_content(CatShell::CatPointer<CatShell::CatMemPage> data);
                /// Sets the content raw data page.

        //
        // static functions
        //
        static std::uint16_t get_counter_increase_16(std::uint16_t cnt_before, std::uint16_t cnt_after);
        static std::uint32_t get_counter_increase_32(std::uint32_t cnt_before, std::uint32_t cnt_after);

        static std::uint32_t get_memory_lenght();
                /// Returns the length of the memory needed to hold single RawH map.

protected:

private:

        void put_raw_32bit(std::uint32_t index, std::uint32_t data);
                /// Function for setting the raw data.

        std::uint32_t get_raw_32bit(std::uint32_t index);
                /// Function for retrieving from raw data.

        void put_raw_16bit(std::uint16_t index, std::uint16_t data);
                /// Function for setting the raw data.

        std::uint16_t get_raw_16bit(std::uint16_t index);
                /// Function for retrieving from raw data.


private:

        CatShell::CatPointer<CatShell::CatMemPage>  _raw_memory_page;
        std::uint8_t*            _raw_data;
        // time-stamp
        CatShell::TimePoint      _time;

};

inline StreamSize RawH::cat_stream_get_size() const
{
        if(_raw_memory_page.isSomething())
        {
                return _raw_memory_page->cat_stream_get_size() + _time.get_stream_length();
        }
        else
                return 0;
}

inline void RawH::cat_stream_out(std::ostream& output)
{
        _time.write_to_stream(output);
        CatShell::PluginDeamon::stream_object_out(output, _raw_memory_page);
}

inline void RawH::cat_stream_in(std::istream& input)
{
        _time.read_from_stream(input);
        _raw_memory_page = CatShell::PluginDeamon::stream_object_in(input);
        _raw_data = _raw_memory_page->get_memory();
}

inline void RawH::put_raw_32bit(std::uint32_t index, std::uint32_t data) {*((std::uint32_t*)(_raw_data+index)) = data;}

inline std::uint32_t RawH::get_raw_32bit(std::uint32_t index) {return *((std::uint32_t*)(_raw_data+index));}

inline void RawH::put_raw_16bit(std::uint16_t index, std::uint16_t data) {*((std::uint16_t*)(_raw_data+index)) = data;}

inline std::uint16_t RawH::get_raw_16bit(std::uint16_t index) {return *((std::uint16_t*)(_raw_data+index));}


inline std::uint8_t RawH::map_number() {return (std::uint8_t)(get_raw_32bit(12) & 0xFF);}

inline std::uint16_t RawH::overflow_number() {return (std::uint16_t)(get_raw_32bit(16) >> 16);}

inline std::uint16_t RawH::readout_number() {return (std::uint16_t)(get_raw_32bit(16));}

inline std::uint32_t RawH::readout_start() {return get_raw_32bit(20);}

inline std::uint32_t RawH::readout_end() {return get_raw_32bit(24);}

inline void RawH::set_content(CatShell::CatPointer<CatShell::CatMemPage> data) {_raw_memory_page = data; _raw_data = _raw_memory_page->get_memory();}

inline void RawH::cat_free() {_raw_memory_page.make_nullptr();}

inline void RawH::get_counters_for_bcid(std::uint16_t bcid, std::uint16_t& hit_counter, std::uint16_t& live_counter)
{
        std::uint32_t tmp = get_raw_32bit(28 + 4*bcid);
        hit_counter = (std::uint16_t)(tmp >> 16);
        live_counter = (std::uint16_t)(tmp);
}

inline const CatShell::TimePoint& RawH::timestamp() const { return _time; }
inline void RawH::set_time(const CatShell::TimePoint& time) { _time = time; }

//
// static functions
//

inline std::uint16_t RawH::get_counter_increase_16(std::uint16_t cnt_before, std::uint16_t cnt_after) { return (cnt_before <= cnt_after) ? (cnt_after-cnt_before) : ((~(cnt_after-cnt_before))+1); }
inline std::uint32_t RawH::get_counter_increase_32(std::uint32_t cnt_before, std::uint32_t cnt_after) { return (cnt_before <= cnt_after) ? (cnt_after-cnt_before) : ((~(cnt_after-cnt_before))+1); }
inline std::uint32_t RawH::get_memory_lenght() { return (7+ORBIT_LENGTH)*sizeof(std::uint32_t); }

} // namespace DbmShell

#endif

#ifndef INCLUDE_RAWTOMAP
#define INCLUDE_RAWTOMAP

#include "CatShell/core/Algorithm.h"

#include "DbmShell/lumi/defines.h"
#include "DbmShell/lumi/MapIdentity.h"
#include "DbmShell/lumi/RawH.h"
#include "DbmShell/lumi/LumiMap32.h"

namespace DbmShell {

class RawToMap : public CatShell::Algorithm
{
        CAT_OBJECT_DECLARE(RawToMap, CatShell::Algorithm, CAT_NAME_RAWTOMAP, CAT_TYPE_RAWTOMAP);
public:

        RawToMap();
                /// Constructor: default.

        virtual ~RawToMap();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

//----------------------------------------------------------------------

protected:

private:

        std::uint16_t _bcid_offset; // BCID offset

        CatShell::MemoryPool<LumiMap32>*  _pool; // memory pool LumiMap32 objects

        CatShell::Algorithm::DataPort    _in_port_rawh;    // input port for raw data maps
        CatShell::Algorithm::DataPort    _out_port_lumimap32;        // output port for lumi maps

        std::map<MapIdentity, CatShell::CatPointer<RawH>> _history;
};

inline StreamSize RawToMap::cat_stream_get_size() const
{
        return CatShell::Algorithm::cat_stream_get_size();
}

inline void RawToMap::cat_stream_out(std::ostream& output)
{
        CatShell::Algorithm::cat_stream_out(output);
}

inline void RawToMap::cat_stream_in(std::istream& input)
{
        CatShell::Algorithm::cat_stream_in(input);
}

inline void RawToMap::cat_print(std::ostream& output, const std::string& padding)
{
        CatShell::Algorithm::cat_print(output, padding);
}

} // namespace DbmShell

#endif

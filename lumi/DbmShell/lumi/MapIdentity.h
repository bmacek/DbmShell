#ifndef INCLUDE_MAPIDENTITY
#define INCLUDE_MAPIDENTITY

#include <iostream>

#include "CatShell/core/defines.h"
#include "DbmShell/hardware/defines.h"
#include "DbmShell/lumi/defines.h"
#include "CatShell/core/CatAddress.h"

namespace DbmShell {

class MapIdentity
{
public:

        MapIdentity();
                /// Constructor: default.


        virtual ~MapIdentity();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------

        // comparison operators for sorting
        bool operator<(const MapIdentity& id) const;
                /// Compares two identities.


        std::uint8_t data_source();
                /// Returns the source of the data.

        BocFpga fpga_source() const;
                /// Returns from which FPGA the data originates.

        std::uint8_t stream_id();
                /// Returns the stream from which the map arrived.

        std::uint8_t map_number();
                /// Returns the serial number of the map.

        void set(std::uint8_t build_year, std::uint8_t build_month, std::uint8_t build_day, std::uint8_t build_hour, std::uint8_t build_minute, std::uint8_t build_second, std::uint8_t source_byte, std::uint8_t serial);
                /// Sets the time-stamp. 'source_byte' must be composed as specified in the
                /// hardware manual.

        void* cat_get_part(CatShell::CatAddress& addr);
                /// Returns the pointer to the desired part.

protected:

private:

        MapIdentity(std::uint8_t build_year, std::uint8_t build_month, std::uint8_t build_day, std::uint8_t build_hour, std::uint8_t build_minute, std::uint8_t build_second, std::uint8_t source_byte, std::uint8_t serial);
                /// Constructor: default.
private:

        std::uint64_t   _fw_version;
        std::uint8_t    _source;
        std::uint8_t    _stream;
        std::uint8_t    _fpga;
        std::uint8_t    _map;

};

inline std::uint8_t MapIdentity::data_source() { return _source; }
inline BocFpga MapIdentity::fpga_source() const { return _fpga==1 ? BocFpga::South : BocFpga::North; }
inline std::uint8_t MapIdentity::stream_id() { return _stream; }
inline std::uint8_t MapIdentity::map_number() { return _map; }
inline StreamSize MapIdentity::cat_stream_get_size() const { return sizeof(std::uint64_t); }

} // namespace DbmShell

#endif

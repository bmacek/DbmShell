#include "DbmShell/lumi/MapIdentity.h"

using namespace std;
using namespace CatShell;

namespace DbmShell {

MapIdentity::MapIdentity()
{
}

MapIdentity::MapIdentity(std::uint8_t build_year, std::uint8_t build_month, std::uint8_t build_day, std::uint8_t build_hour, std::uint8_t build_minute, std::uint8_t build_second, std::uint8_t source_byte, std::uint8_t serial)
{
        _fw_version = (((std::uint64_t)build_year) << 40) | (((std::uint64_t)build_month) << 32) | (((std::uint64_t)build_day) << 24) | (((std::uint64_t)build_hour) << 16) | (((std::uint64_t)build_minute) << 8) | (((std::uint64_t)build_second) << 0);
        _source = ((source_byte >> 3) & 0x0F);
        _stream = ((source_byte >> 0) & 0x03);
        if(source_byte & 0x04)
                _fpga = 1;//BocFpga::South;
        else
                _fpga = 0;//BocFpga::North;
        _map = serial;
}

MapIdentity::~MapIdentity()
{
}

void MapIdentity::set(std::uint8_t build_year, std::uint8_t build_month, std::uint8_t build_day, std::uint8_t build_hour, std::uint8_t build_minute, std::uint8_t build_second, std::uint8_t source_byte, std::uint8_t serial)
{
        _fw_version = (((std::uint64_t)build_year) << 40) | (((std::uint64_t)build_month) << 32) | (((std::uint64_t)build_day) << 24) | (((std::uint64_t)build_hour) << 16) | (((std::uint64_t)build_minute) << 8) | (((std::uint64_t)build_second) << 0);
        _source = ((source_byte >> 3) & 0x0F);
        _stream = ((source_byte >> 0) & 0x03);
        if(source_byte & 0x04)
                _fpga = 1;//BocFpga::South;
        else
                _fpga = 0;//BocFpga::North;
        _map = serial;
}

void MapIdentity::cat_stream_out(std::ostream& output)
{
        std::uint64_t tmp;

        tmp = _fw_version << 16;
        tmp |= ((std::uint64_t)_source) << 11;
        if(_fpga == 1)//BocFpga::South
                tmp |= 0x0400;
        tmp |= ((std::uint64_t)_stream) << 8;
        tmp |= ((std::uint64_t)_map);

        output.write((char*)&tmp, sizeof(std::uint64_t));
}

void MapIdentity::cat_stream_in(std::istream& input)
{
        std::uint64_t tmp;

        input.read((char*)&tmp, sizeof(std::uint64_t));

        _fw_version = tmp >> 16;
        _source = (uint8_t)((tmp >> 11) & 0x0F);
        if(tmp & 0x0400)
                _fpga = 1;//BocFpga::South;
        else
                _fpga = 0;//BocFpga::North;
        _stream = (uint8_t)((tmp >> 8) & 0x03);
        _map = (uint8_t)tmp;
}

void MapIdentity::cat_print(std::ostream& output, const std::string& padding)
{
        char text[100];
        std::uint32_t tmp32;

        sprintf(text, "Source version date: %04u-%02u-%02u %02u:%02u:%02u", (std::uint32_t)(2000+((_fw_version >> 40) & 0xFF)), (std::uint16_t)((_fw_version >> 32) & 0xFF), (std::uint16_t)((_fw_version >> 24) & 0xFF), (std::uint16_t)((_fw_version >> 16) & 0xFF), (std::uint16_t)((_fw_version >> 8) & 0xFF), (std::uint16_t)((_fw_version >> 0) & 0xFF));
        output << padding << text << endl;
        tmp32 = data_source();
		    if(tmp32 == (DBM_LUMI_H_DATA_SOURCE_ERROR >> DBM_LUMI_H_DATA_SOURCE_SHIFT))
			     output << padding << "Data source        : error" << endl;
		    else if(tmp32 == (DBM_LUMI_H_DATA_SOURCE_EMULATOR >> DBM_LUMI_H_DATA_SOURCE_SHIFT))
			     output << padding << "Data source        : emulator" << endl;
		    else
			     output << padding << "Data source        : " << tmp32 << endl;
        BocFpga fpga = fpga_source();
        if(fpga == BocFpga::North)
                output << padding << "FPGA source        : north" << endl;
        else
                output << padding << "FPGA source        : south" << endl;
        output << padding << "Stream ID          : " << (int)stream_id() << endl;
        output << padding << "Map                : " << (int)map_number() << endl;
}

bool MapIdentity::operator<(const MapIdentity& id) const
{
        if(_map < id._map)
                return true;
        else if(_map > id._map)
                return false;
        else
        {
                if(_stream < id._stream)
                        return true;
                else if(_stream > id._stream)
                        return false;
                else
                {
                        if(_fpga != id._fpga)
                        {
                                if(fpga_source()  == BocFpga::North)
                                        return true;
                                else
                                        return false;
                        }
                        else
                        {
                                if(_source < id._source)
                                        return true;
                                else if(_source > id._source)
                                        return false;
                                else
                                {
                                        if(_fw_version < id._fw_version)
                                                return true;
                                        else if(_fw_version > id._fw_version)
                                                return false;
                                        else
                                                return false;
                                }
                        }
                }
        }
}

void* MapIdentity::cat_get_part(CatAddress& addr)
{
  if(addr == "id_fw")
	{
        return &_fw_version;
	}
	else if(addr == "id_source")
	{
        return &_source;
	}
	else if(addr == "id_stream")
	{
        return &_stream;
	}
	else if(addr == "id_map")
	{
	      return &_map;
	}
	else if(addr == "id_fpga")
	{
	      return &_fpga;
	}
	return nullptr;
}

} // namespace DbmShell

#include "DbmShell/lumi/LumiMap32.h"

using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(LumiMap32, CAT_NAME_LUMIMAP32, CAT_TYPE_LUMIMAP32);

LumiMap32::LumiMap32()
{
	_accumulation_valid = false;
}

LumiMap32::~LumiMap32()
{
}

void LumiMap32::cat_stream_out(std::ostream& output)
{
	_identity.cat_stream_out(output);
	output.write((char*)&_error_count, sizeof(std::uint32_t));
	output.write((char*)&_period_start, sizeof(std::uint32_t));
	output.write((char*)&_period_end, sizeof(std::uint32_t));

	_time_start.write_to_stream(output);
	_time_end.write_to_stream(output);

    for(std::uint16_t bcid=0; bcid<ORBIT_LENGTH; ++bcid)
    {
		output.write((char*)&_counts_hit[bcid], sizeof(std::uint32_t));
		output.write((char*)&_counts_live[bcid], sizeof(std::uint32_t));
    }
}

void LumiMap32::cat_stream_in(std::istream& input)
{
	_identity.cat_stream_in(input);
	input.read((char*)&_error_count, sizeof(std::uint32_t));
	input.read((char*)&_period_start, sizeof(std::uint32_t));
	input.read((char*)&_period_end, sizeof(std::uint32_t));

	_time_start.read_from_stream(input);
	_time_end.read_from_stream(input);

  for(std::uint16_t bcid=0; bcid<ORBIT_LENGTH; ++bcid)
  {
		input.read((char*)&_counts_hit[bcid], sizeof(std::uint32_t));
		input.read((char*)&_counts_live[bcid], sizeof(std::uint32_t));
  }
	_accumulation_valid = false;
}

void LumiMap32::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];

	CatObject::cat_print(output, padding);
	output << "Lumi map 32-bit for " << ORBIT_LENGTH << " BCIDs." << endl;
	_identity.cat_print(output, padding);

	output << padding << "Error count        : " << _error_count << endl;
	output << padding << "Start orbit        : " << _period_start << endl;
	output << padding << "End orbit          : " << _period_end << endl;
	_time_start.convert_to_UTC_asci(text);
	output << padding << "Start time (UTC)   : " << text << endl;
	_time_end.convert_to_UTC_asci(text);
	output << padding << "End time (UTC)     : " << text << endl;
	output << padding << "Counters           : ";

	std::uint16_t i=0;
	for(std::uint16_t r=0; i<ORBIT_LENGTH; ++r)
	{
		sprintf(text, "%5u: ", i);
		output << endl << padding << text;
		for(; i<5*r+5 && i<ORBIT_LENGTH;i++)
		{
			sprintf(text, "[%10u,%10u],", _counts_hit[i], _counts_live[i]);
			output << text;
		}
	}
}

void LumiMap32::construct_from_rawh(CatPointer<RawH>& before, CatPointer<RawH>& after, std::uint16_t bcid_offset)
{
    // set the identity
    after->fill_identity(_identity);

    // number of errors
    _error_count = RawH::get_counter_increase_16(before->overflow_number(), after->overflow_number());

    // period
    _period_start = before->readout_end();
    _period_end = after->readout_end();

    // time
    _time_start = before->timestamp();
    _time_end = after->timestamp();

    // counters
    std::uint16_t h1, l1, h2, l2, bcid_corr, result, orbits;
	orbits = (std::uint16_t)(_period_end - _period_start);
    for(std::uint16_t bcid=0; bcid<ORBIT_LENGTH; ++bcid)
    {
            before->get_counters_for_bcid(bcid, h1, l1);
            after->get_counters_for_bcid(bcid, h2, l2);

		bcid_corr = (bcid+bcid_offset)%ORBIT_LENGTH;

		result = h2-h1;
		if(result <= orbits)
		{
	            _counts_hit[bcid_corr] = result;
		}
		else
		{
                    _counts_hit[bcid_corr] = ((~result) + 1);
		}
		result = l2-l1;
		if(result <= orbits)
		{
                    _counts_live[bcid_corr] = result;
		}
		else
		{
		    _counts_live[bcid_corr] = ((~result) + 1);
		}

//		_counts_hit[bcid_corr] = (RawH::get_counter_increase_16(h1, h2));
//		_counts_live[bcid_corr] = (RawH::get_counter_increase_16(l1, l2));
    }

    _accumulation_valid = false;
}

void* LumiMap32::cat_get_part(CatShell::CatAddress& addr)
{
	if(addr == "live")
	{
        if(addr.has_index())
        {
                if(addr.get_index() >= ORBIT_LENGTH)
                        return nullptr;
                else
                		return &(_counts_hit[addr.get_index()]);
        }
        else
                return nullptr;
	}
	else if(addr == "hit")
	{
        if(addr.has_index())
        {
                if(addr.get_index() >= ORBIT_LENGTH)
                        return nullptr;
                else
                		return &(_counts_live[addr.get_index()]);
        }
        else
                return nullptr;
	}
	else if(addr == "hits")
	{
        return _counts_live;
	}
	else if(addr == "lives")
	{
	      return _counts_hit;
	}
	else if(addr == "error")
	{
	      return &_error_count;
	}
	else if(addr == "start")
	{
	      return &_period_start;
	}
	else if(addr == "end")
	{
	      return &_period_end;
	}
	else if(addr == "accumulated_hits")
	{
		refresh_accumulation();
	  return &_accumulation_hits;
	}
	else if(addr == "accumulated_bcids")
	{
		refresh_accumulation();
	  return &_accumulation_count;
	}
	else if(addr == "accumulated_dead")
	{
		refresh_accumulation();
	  return &_accumulation_dead;
	}
	else
		return _identity.cat_get_part(addr);
}

void LumiMap32::refresh_accumulation()
{
	if(!_accumulation_valid)
	{
		_accumulation_hits = 0;
		_accumulation_count = 0;
		_accumulation_dead = 0;
		for(std::uint16_t i=0; i<ORBIT_LENGTH; ++i)
		{
			_accumulation_hits += _counts_live[i];
			_accumulation_dead += ((_period_end-_period_start)-_counts_hit[i]);
			if(_counts_live[i])
				_accumulation_count++;
		}
		_accumulation_valid = true;
	}
}

} // namespace DbmShell

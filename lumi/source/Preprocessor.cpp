#include "DbmShell/lumi/Preprocessor.h"

#include "CatShell/core/PluginDeamon.h"

using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(DbmPreprocessor, CAT_NAME_DBMPREPROCESSOR, CAT_TYPE_DBMPREPROCESSOR);

DbmPreprocessor::DbmPreprocessor()
{
}

DbmPreprocessor::~DbmPreprocessor()
{
}

void DbmPreprocessor::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
	// processing input data

	// get new RawH
	if(_pool && object.isSomething())
	{
		CatPointer<CatMemPage> data = object.unsafeCast<CatMemPage>();
		// check that this is H-stream
		if(data->get_raw_32bit(0) == DBM_SUBSTREAM_MARKER_H)
		{
			// H-stream pachage
			CatPointer<RawH> ptr = _pool->get_base_element().unsafeCast<RawH>();
			ptr->set_content(data);

			// see which sync group it is
			std::uint32_t sync_index;

			sync_index = 2*ptr->data_source() + (ptr->fpga_source() == BocFpga::North ? 1:0);

			// check the timestamp
			if(_sync_data[sync_index].time)
			{
				if(_sync_data[sync_index].start == ptr->readout_start() && _sync_data[sync_index].end == ptr->readout_end() && _sync_data[sync_index].readout_number == ptr->readout_number())
				{
					// same sync as before
					ptr->set_time(_sync_data[sync_index].time);
				}
				else
				{
					// sync changed
					_sync_data[sync_index].time.get_now();
					_sync_data[sync_index].start = ptr->readout_start();
					_sync_data[sync_index].end = ptr->readout_end();

					// check the increase of the readout number
					std::uint16_t x = RawH::get_counter_increase_16(_sync_data[sync_index].readout_number, ptr->readout_number());
					if(x != 1)
					{
						ERROR(logger, "Readout number sequence unexpected. Readout number increased by " << x << ".")
					}

					_sync_data[sync_index].readout_number = ptr->readout_number();
					ptr->set_time(_sync_data[sync_index].time);
				}
			}
			else
			{
				// no sync data so far
				_sync_data[sync_index].time.get_now();
				_sync_data[sync_index].start = ptr->readout_start();
				_sync_data[sync_index].end = ptr->readout_end();
				_sync_data[sync_index].readout_number = ptr->readout_number();
				ptr->set_time(_sync_data[sync_index].time);
			}

			// publish RawH
			publish_data(_out_port_rawh+sync_index, ptr, logger);
		}
		else
		{
			// unknown DBM package
			ERROR(logger, "Package marked as sub-stream 0x" << hex << data->get_raw_32bit(0) << dec << ": no logic for this substream! Data dropped..")
		}
	}

	Algorithm::process_data(port, object, flush, logger);
}

void DbmPreprocessor::algorithm_init()
{
	char text[20];
	Algorithm::algorithm_init();

	// get the memory pool
	_pool = PluginDeamon::get_memory_pool(CAT_TYPE_RAWH);

	// define the input port
	_in_port_raw_pages = declare_input_port("in_pages", CAT_NAME_CATMEMPAGE);
	// define the output ports
	_out_port_rawh = declare_output_port("out_rawh", CAT_NAME_RAWH, DBM_MAX_SYNC_SOURCES);

	// clear sync data 
	for(std::uint16_t i=0; i<DBM_MAX_SYNC_SOURCES; ++i)
	{
		_sync_data[i].start = 0;
		_sync_data[i].end = 0;
		_sync_data[i].time.set_value(0, 0);
	}
}

void DbmPreprocessor::algorithm_deinit()
{
	Algorithm::algorithm_deinit();
}

} // namespace DbmShell

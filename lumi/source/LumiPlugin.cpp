#include "CatShell/core/Plugin.h"
#include "DbmShell/lumi/RawH.h"
#include "DbmShell/lumi/Emulator.h"
#include "DbmShell/lumi/Preprocessor.h"
#include "DbmShell/lumi/LumiMap32.h"
#include "DbmShell/lumi/RawToMap.h"

using namespace std;

namespace DbmShell {

EXPORT_PLUGIN(CatShell::Plugin, DbmLumiPlugin)
START_EXPORT_CLASSES(DbmLumiPlugin)
EXPORT_CLASS(RawH, CAT_NAME_RAWH, DbmLumiPlugin)
EXPORT_CLASS(DbmPreprocessor, CAT_NAME_DBMPREPROCESSOR, DbmLumiPlugin)
EXPORT_CLASS(DbmEmulator, CAT_NAME_DBMEMULATOR, DbmLumiPlugin)
EXPORT_CLASS(LumiMap32, CAT_NAME_LUMIMAP32, DbmLumiPlugin)
EXPORT_CLASS(RawToMap, CAT_NAME_RAWTOMAP, DbmLumiPlugin)
END_EXPORT_CLASSES(DbmLumiPlugin)

} // namespace DbmShell

#include "DbmShell/lumi/RawH.h"

using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(RawH, CAT_NAME_RAWH, CAT_TYPE_RAWH);

RawH::RawH()
: _raw_memory_page(nullptr)
{
}

RawH::~RawH()
{
}

void RawH::define_header(bool reset, BocFpga fpga, BocStream stream, std::uint8_t map_number)
{
	std::uint32_t value = 0;
	//
	put_raw_32bit(0, DBM_SUBSTREAM_MARKER_H);	// sub-stream marker
	put_raw_32bit(4, (DBM_H_STREAM_HEADER_LENGTH + ORBIT_LENGTH*4)); // packet length
	value = 0; 
	value |= ((std::uint32_t)(DBM_LUMI_COMPILE_YEAR-2000)) << 24;
	value |= ((std::uint32_t)(DBM_LUMI_COMPILE_MONTH)) << 16;
	value |= ((std::uint32_t)(DBM_LUMI_COMPILE_DAY)) << 8;
	value |= ((std::uint32_t)(DBM_LUMI_COMPILE_HOUR));
	put_raw_32bit(8, value); // version ID 0
	value = 0; 
	value |= ((std::uint32_t)(DBM_LUMI_COMPILE_MINUTE)) << 24;
	value |= ((std::uint32_t)(DBM_LUMI_COMPILE_SECOND)) << 16;
	if(reset)
		value |= DBM_LUMI_H_RESET_FLAG;
	value |= DBM_LUMI_H_DATA_SOURCE_EMULATOR;
	if(fpga == BocFpga::South)
		value |= DBM_LUMI_H_FPGA_SOUTH;
	if(stream == BocStream::stream_0)
		value |= DBM_LUMI_H_STREAM_LUMI0;
	else if(stream == BocStream::stream_1)
		value |= DBM_LUMI_H_STREAM_LUMI1;
	value |= ((std::uint32_t)map_number);
	put_raw_32bit(12, value); // version ID 1, flags
}

void RawH::define_period(std::uint16_t overflow_number, std::uint16_t readout_number, std::uint32_t previous_end_orbit, std::uint32_t end_orbit)
{
	std::uint32_t value = 0;
	//
	value = (((std::uint32_t)overflow_number) << 16) | readout_number;
	put_raw_32bit(16, value);	// status counters
	put_raw_32bit(20, previous_end_orbit);	// beggining
	put_raw_32bit(24, end_orbit);	// end
}

void RawH::define_counters(std::uint16_t bcid, std::uint16_t hit_count, std::uint16_t live_count)
{
	std::uint32_t value = 0;
	//
	value = (((std::uint32_t)hit_count) << 16) | live_count;
	put_raw_32bit(28+4*bcid, value);	// counter
}

void RawH::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];
	std::uint32_t	tmp32;
	std::uint16_t	tmp16[2];
	std::uint8_t	tmp8[5];

	CatObject::cat_print(output, padding);
	output << "Raw-H fragment." << endl;
	get_version_date(tmp16[0], tmp8[0], tmp8[1], tmp8[2], tmp8[3], tmp8[4]);
	sprintf(text, "Source version date: %4u-%2u-%2u %2u:%2u:%2u", tmp16[0], tmp8[0], tmp8[1], tmp8[2], tmp8[3], tmp8[4]);
	output << padding << text << endl;
	output << padding << "Reset             : " << (was_lumi_reset() ? "Y" : "N") << endl;
	tmp32 = data_source();
	if(tmp32 == (DBM_LUMI_H_DATA_SOURCE_ERROR >> DBM_LUMI_H_DATA_SOURCE_SHIFT))
		output << padding << "Data source       : error" << endl;
	else if(tmp32 == (DBM_LUMI_H_DATA_SOURCE_EMULATOR >> DBM_LUMI_H_DATA_SOURCE_SHIFT))
		output << padding << "Data source       : emulator" << endl;
	else
		output << padding << "Data source       : " << tmp32 << endl;
	BocFpga fpga = fpga_source();
	if(fpga == BocFpga::North)
		output << padding << "FPGA source       : north" << endl;
	else
		output << padding << "FPGA source       : south" << endl;
	output << padding << "Stream ID         : " << (int)streamID() << endl;
	output << padding << "Map               : " << (int)map_number() << endl;
	output << padding << "Overflow number   : " << overflow_number() << endl;
	output << padding << "Readout number    : " << readout_number() << endl;
	output << padding << "Start orbit       : " << readout_start() << endl;
	output << padding << "End orbit         : " << readout_end() << endl;
	_time.convert_to_UTC_asci(text);
	output << padding << "Timestamp (UTC)   : " << text << endl;
	output << padding << "Data              : ";

	std::uint16_t i=0;
	for(std::uint16_t r=0; i<ORBIT_LENGTH; ++r)
	{
		sprintf(text, "%5u: ", i);
		output << endl << padding << text;
		for(; i<10*r+10 && i<ORBIT_LENGTH;i++)
		{
			get_counters_for_bcid(i, tmp16[0], tmp16[1]);
			sprintf(text, "[%5u,%5u],", tmp16[0], tmp16[1]);
			output << text;
		}
	}
}

void RawH::get_version_date(std::uint16_t& year, std::uint8_t& month, std::uint8_t& day, std::uint8_t& hour, std::uint8_t& minute, std::uint8_t& second)
{
	std::uint32_t value;
	//
	value = get_raw_32bit(8);
	year = (std::uint16_t)((value >> 24) + 2000);
	month = (std::uint8_t)((value >> 16) & 0xFF);
	day = (std::uint8_t)((value >> 8) & 0xFF);
	hour = (std::uint8_t)((value) & 0xFF);
	//
	value = get_raw_32bit(12);
	minute = (std::uint8_t)((value >> 24) & 0xFF);
	second = (std::uint8_t)((value >> 16) & 0xFF);
}

void RawH::fill_identity(MapIdentity& identity)
{
	std::uint32_t value, value_II;
	//
	value = get_raw_32bit(8);
	value_II = get_raw_32bit(12);
	identity.set((std::uint8_t)(value >> 24), (std::uint8_t)(value >> 16), (std::uint8_t)(value >> 8), (std::uint8_t)(value), (std::uint8_t)(value_II >> 24), (std::uint8_t)(value_II >> 16), (std::uint8_t)(value_II >> 8), (std::uint8_t)(value_II));
}

bool RawH::was_lumi_reset()
{
	std::uint32_t value;
	//
	value = get_raw_32bit(12);
	return (value & DBM_LUMI_H_RESET_FLAG) == DBM_LUMI_H_RESET_FLAG;
}

std::uint32_t RawH::data_source()
{
	std::uint32_t value;
	//
	value = get_raw_32bit(12) >> DBM_LUMI_H_DATA_SOURCE_SHIFT;
	return (value & DBM_LUMI_H_DATA_SOURCE_MASK_SHIFTED);
}

BocFpga RawH::fpga_source()
{
	std::uint32_t value;
	//
	value = get_raw_32bit(12);
	if ((value & DBM_LUMI_H_FPGA_SOUTH) == DBM_LUMI_H_FPGA_SOUTH)
		return BocFpga::South;
	else
		return BocFpga::North;
}

std::uint8_t RawH::streamID()
{
	std::uint32_t value;
	//
	value = get_raw_32bit(12);
	value = value & DBM_LUMI_H_STREAM_MASK;
	if(value == DBM_LUMI_H_STREAM_LUMI0)
		return 0;
	else if(value == DBM_LUMI_H_STREAM_LUMI1)
		return 1;
	else
		return 0xFF;
}

void RawH::add_count(std::uint16_t bcid, bool add_hit, bool add_live)
{
	if(add_hit)
	{
        	*((std::uint16_t*)(_raw_data+28+4*bcid)) += 1;
	}
	if(add_live)
	{
        	*((std::uint16_t*)(_raw_data+28+4*bcid+2)) += 1;
	}
}


} // namespace DbmShell

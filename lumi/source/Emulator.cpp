#include "DbmShell/lumi/Emulator.h"

using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(DbmEmulator, CAT_NAME_DBMEMULATOR, CAT_TYPE_DBMEMULATOR);

DbmEmulator::DbmEmulator()
{
}

DbmEmulator::~DbmEmulator()
{
}

void DbmEmulator::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
	// processing input data

	// no input data is used
}

void DbmEmulator::algorithm_init()
{
	Process::algorithm_init();

	// define the output port
	_out_port_raw_pages = declare_output_port("out_pages", CAT_NAME_CATMEMORY);
}

void DbmEmulator::algorithm_deinit()
{
	Process::algorithm_deinit();
}

void DbmEmulator::work_main()
{

}

void DbmEmulator::work_init()
{

}

void DbmEmulator::work_deinit()
{

}

void DbmEmulator::work_subthread_finished(CatPointer<WorkThread> child)
{
	// no subthreads for now
}

} // namespace DbmShell

#include "DbmShell/lumi/RawToMap.h"

#include "DbmShell/lumi/RawH.h"

using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(RawToMap, CAT_NAME_RAWTOMAP, CAT_TYPE_RAWTOMAP);

RawToMap::RawToMap()
{
}

RawToMap::~RawToMap()
{
	_history.clear();
}

void RawToMap::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
	// processing input data

	// get new RawH
	if(_pool && object.isSomething())
	{
		MapIdentity id;

		CatPointer<RawH> data = object.unsafeCast<RawH>();
		data->fill_identity(id);

		// find previous RawH map for this algo
		auto result = _history.emplace(id, data);
		if(!result.second)
		{
			// was not inserted, so there is a previous map
			std::uint32_t x = RawH::get_counter_increase_32(result.first->second->readout_end(), data->readout_end());
			if(x > 0xFFFF)
			{
				// overflow !!!
				ERROR(logger, "Readouts too far apart. Overflow happened (" << x << ")!")
				result.first->second = data;
			}
			else
			{
				// get new lumi map
				CatPointer<LumiMap32> lumi_map = _pool->get_element();
				// construct the map
				lumi_map->construct_from_rawh(result.first->second, data, _bcid_offset);
				result.first->second = data;

				// publish the map
				publish_data(_out_port_lumimap32, lumi_map, logger);
			}
		}
	}

	if(flush)
		_history.clear();

	Algorithm::process_data(port, object, flush, logger);
}

void RawToMap::algorithm_init()
{
	Algorithm::algorithm_init();

	// get the memory pool
    _pool = PluginDeamon::get_pool<LumiMap32>();

	// define the input port
	_in_port_rawh = declare_input_port("in_raw", CAT_NAME_RAWH);
	// define the output port
	_out_port_lumimap32 = declare_output_port("out_map", CAT_NAME_LUMIMAP32);

	// get settings
	_bcid_offset = algo_settings().get_setting<std::uint16_t>("offset", 0);
}

void RawToMap::algorithm_deinit()
{
	_history.clear();

	Algorithm::algorithm_deinit();
}

} // namespace DbmShell

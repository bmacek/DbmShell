#ifndef INCLUDE_DBMSIMTRACK
#define INCLUDE_DBMSIMTRACK

#include "CatShell/core/CatObject.h"
#include "DbmShell/simulation/defines.h"

namespace DbmShell{

class DbmSimTrack: public CatShell::CatObject
{
public:
	DbmSimTrack();

	virtual ~DbmSimTrack();

	CAT_OBJECT_DECLARE(DbmSimTrack, CatShell::CatObject, CAT_NAME_DBMSIMTRACK, CAT_TYPE_DBMSIMTRACK);

	//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
       	virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
	//----------------------------------------------------------------------
	virtual void* cat_get_part(CatShell::CatAddress& addr);
		///Returns the pointer to the desired part(element).

	void set_track(uint8_t col, uint16_t row, double energy, uint8_t t);
		/// Set the parameters of the track.

	uint8_t get_col() { return _part_coord_x; }
	uint16_t get_row() { return _part_coord_y; }

private:

	uint8_t		_part_coord_x;//column
	uint16_t	_part_coord_y;//row
	double		_part_energy;// in Mev
	uint8_t		_part_time;// ns after BC
};

inline StreamSize DbmSimTrack::cat_stream_get_size() const { return sizeof(double) + sizeof(uint16_t) + 2*sizeof(uint8_t); }
inline void DbmSimTrack::set_track(uint8_t col, uint16_t row, double energy, uint8_t t)
{
	_part_coord_x = col;
	_part_coord_y = row;
	_part_energy = energy;
	_part_time = t;
}

}
#endif

#ifndef INCLUDE_DBMSIMFE
#define INCLUDE_DBMSIMFE

#include "CatShell/core/Algorithm.h"
#include "DbmShell/simulation/defines.h"
#include "CatShell/core/Logger.h"
#include "CatShell/core/WorkThread.h"
#include "CatShell/core/CatBool.h"

//using namespace CatShell;

namespace DbmShell{

class DbmSimFe: public CatShell::Algorithm
{
public:
	DbmSimFe();

	virtual ~DbmSimFe();

	CAT_OBJECT_DECLARE(DbmSimFe, CatShell::Algorithm, CAT_NAME_DBMSIMFE, CAT_TYPE_DBMSIMFE);

	//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
	//----------------------------------------------------------------------
	virtual void* cat_get_part(CatShell::CatAddress& addr);
		///Returns the pointer to the desired part(element).


	// ALGORITHM
	virtual void process_data(CatShell::Algorithm::DataPort port, CatShell::CatPointer<CatShell::CatObject>& object,bool flush, CatShell::Logger&logger);
		/// Process data.
	virtual void algorithm_init();
		/// Defines the process interface and initialises it.
	virtual void algorithm_deinit();
		/// Clears all the process interfaces.
	//----------------------------------------------
	


private:

	// memory pool
	CatShell::MemoryPool<CatShell::CatBool>*  _pool;
	
	
	bool _gl_pulse;
	bool _len;
	
	typedef struct {
		bool _enable;
		bool _hitor;
		uint8_t _tdac;
		uint8_t _fdac;
		bool _inj_large;
		bool _inj_small;
	} pixelInfo;

	pixelInfo _pixels[80][336];
	bool      _fifo[672];

	uint8_t _d_col;
	uint8_t _p_sel;

	// ports
	DataPort	_in_port_object;
	DataPort	_in_port_command;
	DataPort	_hitor_out;

};

inline StreamSize DbmSimFe::cat_stream_get_size() const { return 0; }	

}
#endif

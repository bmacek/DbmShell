#ifndef INCLUDE_DBMSIMTRACKGEN
#define INCLUDE_DBMSIMTRACKGEN

#include "CatShell/core/Algorithm.h"
#include "DbmShell/simulation/defines.h"
#include "DbmShell/simulation/DbmSimTrack.h"
#include "CatShell/core/Logger.h"
#include "CatShell/core/WorkThread.h"
#include "CatShell/core/CatCollection.h"
#include "CatShell/core/CatObject.h"
#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/CatUint32.h"
#include <iostream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <string>
#include <bitset>
#include <vector>
#include <stdio.h>
#include <set>
#include <iomanip>
#include <bitset>
#include <random>

//using namespace CatShell;

namespace DbmShell{

class DbmSimTrackGen: public CatShell::Algorithm
{
public:
	DbmSimTrackGen();

	virtual ~DbmSimTrackGen();

	CAT_OBJECT_DECLARE(DbmSimTrackGen, CatShell::Algorithm, CAT_NAME_DBMSIMTRACKGEN,CAT_TYPE_DBMSIMTRACKGEN);

	//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
	//----------------------------------------------------------------------
	virtual void* cat_get_part(CatShell::CatAddress& addr);
		///Returns the pointer to the desired part(element).


	// ALGORITHM
	virtual void process_data(CatShell::Algorithm::DataPort port, CatShell::CatPointer<CatShell::CatObject>& object,bool flush, CatShell::Logger&logger);
		/// Process data.
	virtual void algorithm_init();
		/// Defines the process interface and initialises it.
	virtual void algorithm_deinit();
		/// Clears all the process interfaces.
	//----------------------------------------------

private:

	uint32_t tracks_for_pps(uint32_t pp);
		/// Returns the number of tracks given the number of pp collisions. 

	int random(int a, int b);
		/// Gives a random variable between a and b.

	int N_simulatedTracks(int i);
		/// Random number of tracks for single pp.

	int N_Tracks();

private:

	DataPort	_in_port_object;
	DataPort	_pix_out;

	// random distributions
	std::default_random_engine				_generator;
	std::discrete_distribution<uint32_t>*	_distribution_single_pp;	
	std::uniform_real_distribution<double>*	_distribution_flat;

	// memory pools
	CatShell::MemoryPool<DbmSimTrack>* _pool_tracks;
	CatShell::MemoryPool<CatShell::CatCollection>* _pool_collection;
	
	std::uint8_t _mode;

};
inline StreamSize DbmSimTrackGen::cat_stream_get_size() const { return 0; }
inline int DbmSimTrackGen::random(int a, int b) { return ((*_distribution_flat)(_generator))*(b-a)+a; }

}
#endif

#ifndef INCLUDE_BOCFW_INCLUDED
#define INCLUDE_BOCFW_INCLUDED

#include "CatShell/core/Algorithm.h"
#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/CatBool.h"
#include "CatShell/core/CatUint32.h"
#include "CatShell/core/CatBool.h"

#include "CatShell/basics/CatMemory.h"

#include "DbmShell/hardware/defines.h"

#include "DbmShell/lumi/RawH.h"

#include "DbmShell/simulation/defines.h"

#include "DbmShell/hardware/BocRegisterCommand.h"

#include <iostream>
#include <fstream>
#include <math.h>

namespace DbmShell {

class DbmSimBoc : public CatShell::Algorithm
{
        CAT_OBJECT_DECLARE(DbmSimBoc, CatShell::Algorithm, CAT_NAME_DBMSIMBOC, CAT_TYPE_DBMSIMBOC);
public:

        DbmSimBoc();
                /// Constructor: default.

        virtual ~DbmSimBoc();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:

private:

        // ports
        CatShell::Algorithm::DataPort    _in_port_hitor;
	CatShell::Algorithm::DataPort    _out_port_rawh;
	CatShell::Algorithm::DataPort    _in_port_command;

        // memory pools
	CatShell::MemoryPool<RawH>*                _pool_rawh;
	CatShell::MemoryPool<CatShell::CatMemory>* _pool_mem;

        // timing variables
        std::uint16_t _bcid;
        std::uint32_t _period;
        std::uint32_t _counter;
        std::uint32_t _end_orbit;
        std::uint32_t _previous_end_orbit;
        std::uint16_t _readout_number;

	//commands
	std::uint16_t _address;
	std::uint16_t _address_l;
	std::uint16_t _data;
	std::uint16_t _mask;
	bool _hit_live_selector;

        // lumi maps to be published
        CatShell::CatPointer<RawH> _rawh[LUMI_ALGORITHM_MAPS]; 
        CatShell::CatPointer<CatShell::CatMemory> _rawmem[LUMI_ALGORITHM_MAPS]; 

        // cache variables
        bool _b_prej[LUMI_ALGORITHM_INPUTS];
        bool _check[LUMI_ALGORITHM_INPUTS]; // flag that the infor from particular gate arrived
        bool _add_hit[ORBIT_LENGTH];
        bool _add_live[ORBIT_LENGTH];

        // look-up tables
        bool _lut_hit[LUMI_ALGORITHM_COMBINATIONS][LUMI_ALGORITHM_MAPS];
        bool _lut_live[LUMI_ALGORITHM_COMBINATIONS][LUMI_ALGORITHM_MAPS];
};

inline StreamSize DbmSimBoc::cat_stream_get_size() const { return CatShell::Algorithm::cat_stream_get_size(); }
inline void DbmSimBoc::cat_stream_out(std::ostream& output) { CatShell::Algorithm::cat_stream_out(output); }
inline void DbmSimBoc::cat_stream_in(std::istream& input) { CatShell::Algorithm::cat_stream_in(input); }
inline void DbmSimBoc::cat_print(std::ostream& output, const std::string& padding) { CatShell::Algorithm::cat_print(output, padding); }
inline void DbmSimBoc::cat_free() {}

} // namespace DbmShell

#endif

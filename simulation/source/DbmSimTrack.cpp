#include "DbmShell/simulation/DbmSimTrack.h"

using namespace std;
using namespace CatShell;

namespace DbmShell{

CAT_OBJECT_IMPLEMENT(DbmSimTrack, CAT_NAME_DBMSIMTRACK, CAT_TYPE_DBMSIMTRACK);

DbmSimTrack::DbmSimTrack()
{
}

DbmSimTrack::~DbmSimTrack()
{
}
	
void DbmSimTrack::cat_stream_out(std::ostream& output)
{
	output.write((char*)&_part_energy, sizeof(double));
	output.write((char*)&_part_coord_x, sizeof(uint8_t));
	output.write((char*)&_part_coord_y, sizeof(uint16_t));
	output.write((char*)&_part_time, sizeof(uint8_t));
}

void DbmSimTrack::cat_stream_in(std::istream& input)
{
	input.read((char*)&_part_energy, sizeof(double));
	input.read((char*)&_part_coord_x, sizeof(uint8_t));
	input.read((char*)&_part_coord_y, sizeof(uint16_t));
	input.read((char*)&_part_time, sizeof(uint8_t));
}

void DbmSimTrack::cat_print(std::ostream& output, const std::string& padding)
{
	CatObject::cat_print(output, padding);
	output << "x = " << int(_part_coord_x)	<< ", y = " << _part_coord_y << ", E = " << _part_energy << ", t = " << int(_part_time);
}

void DbmSimTrack::cat_free() 
{
}

void* DbmSimTrack::cat_get_part(CatAddress& addr)
{
	if(addr == "column")
		return &_part_coord_x;
	else if(addr == "row")
		return &_part_coord_y;
	else if(addr == "energy")
		return &_part_energy;
	else if(addr == "time")
		return &_part_time;
	else
		return nullptr;
}

}

#include "DbmShell/simulation/DbmSimTrackGen.h"
#include "CatShell/core/CatUint32.h"
#include "CatShell/core/PluginDeamon.h"
#include <cstdlib>
#include <iostream>
#include <random>
#include <iterator>
#include <ctime>
#include <type_traits>
#include <cassert>

using namespace CatShell;
using namespace std;

namespace DbmShell{

CAT_OBJECT_IMPLEMENT(DbmSimTrackGen, CAT_NAME_DBMSIMTRACKGEN,CAT_TYPE_DBMSIMTRACKGEN);

DbmSimTrackGen::DbmSimTrackGen()
{
}

DbmSimTrackGen::~DbmSimTrackGen()
{
}

void DbmSimTrackGen::cat_stream_out(std::ostream& output)
{      
}

void DbmSimTrackGen::cat_stream_in(std::istream& input)
{
}

void DbmSimTrackGen::cat_print(std::ostream& output, const std::string& padding)
{
}

void* DbmSimTrackGen::cat_get_part(CatAddress& addr)
{
	return nullptr;
}

void DbmSimTrackGen::cat_free() 
{
}

void DbmSimTrackGen::algorithm_init()
{
	Algorithm::algorithm_init();
	_in_port_object = declare_input_port("in_pp", CAT_NAME_CATUINT32);
	_pix_out = declare_output_port("out_tracks", CAT_NAME_CATCOLLECTION);

	// init the pools
	_pool_tracks = PluginDeamon::get_pool<DbmSimTrack>();
	_pool_collection = PluginDeamon::get_pool<CatCollection>();

	// operation mode
	_mode = algo_settings().get_setting<std::uint8_t>("mode", 1);

	// seed
	_generator.seed(std::time(0));
	// create a flat distribution
	_distribution_flat = new std::uniform_real_distribution<double>(0.0,1.0);
	// create a distribution of tracks for single pp (read it from setting file)
	std::vector<double> weights;
    for(auto& s: algo_settings()["distribution"])
    {
            if(s.name() == "bin")
            {
                weights.push_back(s.get_setting<double>("", 1));
            }
    }
	_distribution_single_pp = new std::discrete_distribution<uint32_t>(weights.begin(), weights.end());
}

void DbmSimTrackGen::algorithm_deinit()
{
	delete _distribution_single_pp;
	_distribution_single_pp = nullptr;
	delete _distribution_flat;
	_distribution_flat = nullptr;

	Algorithm::algorithm_deinit();
}

void DbmSimTrackGen::process_data(CatShell::Algorithm::DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger&logger)
{
    if(port == _in_port_object && object.isSomething())
    {
		uint32_t number_of_pp;
		CatPointer<DbmSimTrack> track;
		CatPointer<CatCollection> list_of_tracks;


		// get the number of pp collisions
		CatPointer<CatUint32> p = object.unsafeCast<CatUint32>();
		number_of_pp = p->value();
		//number_of_pp = 1;

		// create list
		list_of_tracks = _pool_collection->get_element();

		// modes
		if(_mode == 0)
		{	
			// number of tracks equal to number of pp collisions
			if (object.isSomething())
			{

				for(uint32_t i=0; i<number_of_pp; ++i)
				{
					track = _pool_tracks->get_element();
					track->set_track(random(0,80), random(0,336), random(0,50), random(0,25));
					list_of_tracks->push_back(track);
				}
				publish_data(_pix_out, list_of_tracks, logger);
			}
		}	
		else if(_mode == 1)
		{
			// distribution according to ATHENA sim
			uint32_t number_of_tracks = tracks_for_pps(number_of_pp);
			for(uint32_t i=0; i<number_of_tracks; ++i)
			{
				track = _pool_tracks->get_element();
				track->set_track(random(0,80), random(0,336), random(0,50), random(0,25));
				list_of_tracks->push_back(track);
			}
			publish_data(_pix_out, list_of_tracks, logger);
		}
	}
	Algorithm::process_data(port, object, flush, logger);
}

uint32_t DbmSimTrackGen::tracks_for_pps(uint32_t pp)
{
	uint32_t n = 0;
	for(uint32_t i=0; i<pp; ++i)
	{
		n += (*_distribution_single_pp)(_generator);
	}
	return n;
}

}


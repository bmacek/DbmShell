#include "CatShell/core/PluginDeamon.h"
#include "DbmShell/hardware/FEI4Message.h"
#include "DbmShell/simulation/DbmSimFe.h"
#include "DbmShell/simulation/DbmSimTrackGen.h"
#include "DbmShell/simulation/DbmSimTrack.h"

using namespace CatShell;
using namespace std;

namespace DbmShell{

CAT_OBJECT_IMPLEMENT(DbmSimFe, CAT_NAME_DBMSIMFE,CAT_TYPE_DBMSIMFE);

DbmSimFe::DbmSimFe()
{
}

DbmSimFe::~DbmSimFe()
{
}

void DbmSimFe::cat_stream_out(std::ostream& output)
{
}

void DbmSimFe::cat_stream_in(std::istream& input)
{
}

void DbmSimFe::cat_print(std::ostream& output, const std::string& padding)
{
}

void* DbmSimFe::cat_get_part(CatAddress& addr)
{
	return nullptr;
}

void DbmSimFe::cat_free() 
{
}

void DbmSimFe::algorithm_init()
{
	Algorithm::algorithm_init();
	_in_port_object = declare_input_port("tracks", CAT_NAME_CATCOLLECTION);
	_in_port_command = declare_input_port("commands", CAT_NAME_FEI4MESSAGE);
	_hitor_out = declare_output_port("hitor", CAT_NAME_CATBOOL);

	cout << _in_port_object << " xxxx " << _in_port_command << endl;

	// init pool
	_pool = PluginDeamon::get_pool<CatBool>();

	_len = false;
	_gl_pulse = false;
	
	
	for(uint32_t i=0; i<80; i++)
	{
		for(uint32_t j=0; j<336; j++)
		{
			_pixels[i][j]._enable = false;
			_pixels[i][j]._tdac = 0;
			_pixels[i][j]._inj_small = false;
			_pixels[i][j]._inj_large = false;
			_pixels[i][j]._hitor = false;
			_pixels[i][j]._fdac = 0;	
		}
	}
	

}

void DbmSimFe::algorithm_deinit()
{

	Algorithm::algorithm_deinit();
}

void DbmSimFe::process_data(CatShell::Algorithm::DataPort port, CatShell::CatPointer<CatShell::CatObject>& object,bool flush, CatShell::Logger&logger)
{
	CatPointer<CatBool> hitor;	

	if(port == _in_port_object && object.isSomething())
	{	
			
		CatPointer<CatCollection> tracks = object.unsafeCast<CatCollection>();
		hitor = _pool->get_element();

		//število delcev
		vector<int>::size_type np = tracks->size();
		uint32_t x_coord, y_coord;
		
		*hitor = false;
		for(uint32_t i=0; i<np; i++)
		{
		
			CatPointer<DbmSimTrack> t = tracks->at(i).unsafeCast<DbmSimTrack>();
			
			//koordinate
			x_coord = t->get_col();
			y_coord = t->get_row();
			
	
				if(_pixels[x_coord][y_coord]._enable == true && _pixels[x_coord][y_coord]._hitor == true)
				{
					*hitor=true;	
				}
		}
		publish_data(_hitor_out, hitor, logger);
	}
	else if(port == _in_port_command && object.isSomething())
	{
		CatPointer<FEI4Message> cmd = object.unsafeCast<FEI4Message>();

		
		uint32_t start = cmd->get_first()-1;
		//printf("%d\n",start);
		
		//Slow
		if(
		cmd->get_bits(start,start+4) == 22 &&
		cmd->get_bits(start+5,start+8) == 8)
		{	
			//WrRegister
			if(cmd->get_bits(start+9,start+12) == 2)
			{
				//d_col
				if(cmd->get_bits(start+17,start+22) == 22)
				{
					_d_col = cmd->get_bits(start+31,start+36);cmd->cat_print(std::cout, "......");
				}
				//p_sel
				else if(cmd->get_bits(start+17,start+22) == 13)
				{
					_p_sel = 99;
					for(uint32_t i=0; i<13; i++)
					{
						if(cmd->get_bits(start+25+i,start+25+i) != 0){_p_sel = i; break;}
					}//cmd->cat_print(std::cout, "......");
				}
				//LEN
				else if(cmd->get_bits(start+17,start+22) == 27)
				{
					if(cmd->get_bits(start+36,start+36) == 1){_len = true;}
					if(cmd->get_bits(start+36,start+36) == 0){_len = false;}
				}
			
			}
			



		
			//WrFrontEnd
			if(cmd->get_bits(start+9,start+12) == 4)
			{
				for(uint32_t i=0; i<672; ++i)
				{
					if(cmd->get_bits(start+23+i,start+23+i) == 0){_fifo[i] = false;}
					else {_fifo[i] = true;}
				}
			}
			//GlobalPulse
			if(cmd->get_bits(start+9,start+12) == 9 && cmd->get_bits(start+17,start+22) != 0 && _len == true)
			{
				for(uint32_t j=0; j<336; j++)
				{
					if(_p_sel == 8){_pixels[_d_col*2+1][335-j]._hitor = !_fifo[671-j];}
					else if(_p_sel == 7){_pixels[_d_col*2+1][335-j]._inj_large = _fifo[671-j];}
					else if(_p_sel == 6){_pixels[_d_col*2+1][335-j]._inj_small = _fifo[671-j];}
					else if(_p_sel == 0){_pixels[_d_col*2+1][335-j]._enable = _fifo[671-j];}
					else if(_p_sel == 1){
						if(_fifo[671-j] == true){
							_pixels[_d_col*2+1][335-j]._tdac = _pixels[_d_col*2+1][335-j]._tdac | 0x01;
						}
						else if(_fifo[671-j] == false){
							_pixels[_d_col*2+1][335-j]._tdac = _pixels[_d_col*2+1][335-j]._tdac & 0xfe;
						}
					}
					else if(_p_sel == 2){
						if(_fifo[671-j] == true){
							_pixels[_d_col*2+1][335-j]._tdac = _pixels[_d_col*2+1][335-j]._tdac | 0x02;
						}
						else if(_fifo[671-j] == false){
							_pixels[_d_col*2+1][335-j]._tdac = _pixels[_d_col*2+1][335-j]._tdac & 0xfd;
						}
						
					}
					else if(_p_sel == 3){
						if(_fifo[671-j] == true){
							_pixels[_d_col*2+1][335-j]._tdac = _pixels[_d_col*2+1][335-j]._tdac | 0x04;
						}
						else if(_fifo[671-j] == false){
							_pixels[_d_col*2+1][335-j]._tdac = _pixels[_d_col*2+1][335-j]._tdac & 0xfb;
						}
						
					}
					else if(_p_sel == 4){
						if(_fifo[671-j] == true){
							_pixels[_d_col*2+1][335-j]._tdac = _pixels[_d_col*2+1][335-j]._tdac | 0x08;
						}
						else if(_fifo[671-j] == false){
							_pixels[_d_col*2+1][335-j]._tdac = _pixels[_d_col*2+1][335-j]._tdac & 0xf7;
						}
						
					}
					else if(_p_sel == 5){
						if(_fifo[671-j] == true){
							_pixels[_d_col*2+1][335-j]._tdac = _pixels[_d_col*2+1][335-j]._tdac | 0x10;
						}
						else if(_fifo[671-j] == false){
							_pixels[_d_col*2+1][335-j]._tdac = _pixels[_d_col*2+1][335-j]._tdac & 0xef;
						}
						
					}
					else if(_p_sel == 9){
						if(_fifo[671-j] == true){
							_pixels[_d_col*2+1][335-j]._fdac = _pixels[_d_col*2+1][335-j]._fdac | 0x01;
						}
						else if(_fifo[671-j] == false){
							_pixels[_d_col*2+1][335-j]._fdac = _pixels[_d_col*2+1][335-j]._fdac & 0xfe;
						}
					}
					else if(_p_sel == 10){
						if(_fifo[671-j] == true){
							_pixels[_d_col*2+1][335-j]._fdac = _pixels[_d_col*2+1][335-j]._fdac | 0x02;
						}
						else if(_fifo[671-j] == false){
							_pixels[_d_col*2+1][335-j]._fdac = _pixels[_d_col*2+1][335-j]._fdac & 0xfd;
						}
					}
					else if(_p_sel == 11){
						if(_fifo[671-j] == true){
							_pixels[_d_col*2+1][335-j]._fdac = _pixels[_d_col*2+1][335-j]._fdac | 0x04;
						}
						else if(_fifo[671-j] == false){
							_pixels[_d_col*2+1][335-j]._fdac = _pixels[_d_col*2+1][335-j]._fdac & 0xfb;
						}
					}
					else if(_p_sel == 12){
						if(_fifo[671-j] == true){
							_pixels[_d_col*2+1][335-j]._fdac = _pixels[_d_col*2+1][335-j]._fdac | 0x08;
						}
						else if(_fifo[671-j] == false){
							_pixels[_d_col*2+1][335-j]._fdac = _pixels[_d_col*2+1][335-j]._fdac & 0xf7;
						}
					}
				}
				for(uint32_t j=0; j<336; j++)
				{
					if(_p_sel == 8){_pixels[_d_col*2][j]._hitor = !_fifo[j];}
					else if(_p_sel == 7){_pixels[_d_col*2][j]._inj_large = _fifo[j];}
					else if(_p_sel == 6){_pixels[_d_col*2][j]._inj_small = _fifo[j];}
					else if(_p_sel == 0){_pixels[_d_col*2][j]._enable = _fifo[j];}
					else if(_p_sel == 1){
						if(_fifo[j] == true){
							_pixels[_d_col*2][j]._tdac = _pixels[_d_col*2][j]._tdac | 0x01;
						}
						else if(_fifo[j] == false){
							_pixels[_d_col*2][j]._tdac = _pixels[_d_col*2][j]._tdac & 0xfe;
						}
					}
					else if(_p_sel == 2){
						if(_fifo[j] == true){
							_pixels[_d_col*2][j]._tdac = _pixels[_d_col*2][j]._tdac | 0x02;
						}
						else if(_fifo[j] == false){
							_pixels[_d_col*2][j]._tdac = _pixels[_d_col*2][j]._tdac & 0xfd;
						}
						
					}
					else if(_p_sel == 3){
						if(_fifo[j] == true){
							_pixels[_d_col*2][j]._tdac = _pixels[_d_col*2][j]._tdac | 0x04;
						}
						else if(_fifo[j] == false){
							_pixels[_d_col*2][j]._tdac = _pixels[_d_col*2][j]._tdac & 0xfb;
						}
						
					}
					else if(_p_sel == 4){
						if(_fifo[j] == true){
							_pixels[_d_col*2][j]._tdac = _pixels[_d_col*2][j]._tdac | 0x08;
						}
						else if(_fifo[j] == false){
							_pixels[_d_col*2][j]._tdac = _pixels[_d_col*2][j]._tdac & 0xf7;
						}
						
					}
					else if(_p_sel == 5){
						if(_fifo[j] == true){
							_pixels[_d_col*2][j]._tdac = _pixels[_d_col*2][j]._tdac | 0x10;
						}
						else if(_fifo[j] == false){
							_pixels[_d_col*2][j]._tdac = _pixels[_d_col*2][j]._tdac & 0xef;
						}
						
					}
					else if(_p_sel == 9){
						if(_fifo[j] == true){
							_pixels[_d_col*2][j]._fdac = _pixels[_d_col*2][j]._fdac | 0x01;
						}
						else if(_fifo[j] == false){
							_pixels[_d_col*2][j]._fdac = _pixels[_d_col*2][j]._fdac & 0xfe;
						}
					}
					else if(_p_sel == 10){
						if(_fifo[j] == true){
							_pixels[_d_col*2][j]._fdac = _pixels[_d_col*2][j]._fdac | 0x02;
						}
						else if(_fifo[j] == false){
							_pixels[_d_col*2][j]._fdac = _pixels[_d_col*2][j]._fdac & 0xfd;
						}
					}
					else if(_p_sel == 11){
						if(_fifo[j] == true){
							_pixels[_d_col*2][j]._fdac = _pixels[_d_col*2][j]._fdac | 0x04;
						}
						else if(_fifo[j] == false){
							_pixels[_d_col*2][j]._fdac = _pixels[_d_col*2][j]._fdac & 0xfb;
						}
					}
					else if(_p_sel == 12){
						if(_fifo[j] == true){
							_pixels[_d_col*2][j]._fdac = _pixels[_d_col*2][j]._fdac | 0x08;
						}
						else if(_fifo[j] == false){
							_pixels[_d_col*2][j]._fdac = _pixels[_d_col*2][j]._fdac & 0xf7;
						}
					}
				}
			}
		}
	}
	
	Algorithm::process_data(port, object, flush, logger);
}

}

#include "DbmShell/simulation/DbmSimBoc.h"
#include <math.h>

using namespace std;
using namespace CatShell;

namespace DbmShell {

CAT_OBJECT_IMPLEMENT(DbmSimBoc, CAT_NAME_DBMSIMBOC, CAT_TYPE_DBMSIMBOC);

DbmSimBoc::DbmSimBoc()
{
}

DbmSimBoc::~DbmSimBoc()
{
}

void DbmSimBoc::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
	
	if((port >= _in_port_hitor) && (port < _in_port_hitor+LUMI_ALGORITHM_INPUTS) && object.isSomething())
	{
		// calculate which input this is
		uint32_t signal_number = port-_in_port_hitor;
	
		CatPointer<CatBool>  b = object.cast<CatBool>();

		if(b->value() == true)
		{
			if(_b_prej[signal_number] == false)
			{
				//zelena 1
				_add_hit[signal_number]=true;
				_add_live[signal_number]=true;
			}
			else
			{
				//rdeča
				_add_hit[signal_number]=false;
				_add_live[signal_number]=false;
			}
		}
		else
		{
			if(_b_prej[signal_number] == false)
			{
				//zelena 0
				_add_live[signal_number]=true;
				_add_hit[signal_number]=false;
			}
			else
			{
				//rdeča
				_add_hit[signal_number]=false;
				_add_live[signal_number]=false;
			}
		}
		
		_check[signal_number] = true;
		_b_prej[signal_number] = b->value(); // remember this value for the next time

		//check if info from all inputs already arrived
		bool all_info = true;
		for(std::uint32_t i=0; i<LUMI_ALGORITHM_INPUTS; i++)
		{
			if(!_check[i])
			{
				all_info = false;
				break;
			}
		}
			
		if(all_info)
		{	
			// get hit and live LUT addresses
			uint32_t address_h=0;
			uint32_t address_l=0;

			for(uint32_t i=0; i<LUMI_ALGORITHM_INPUTS; i++)
			{
				if(_add_hit[i])
					address_h += (1 << i);
			}
			for(uint32_t i=0; i<LUMI_ALGORITHM_INPUTS; i++)
			{
				if(_add_live[i])
					address_l += (1 << i);
			}
			
			// do the statistics
			for(uint32_t i=0;i<LUMI_ALGORITHM_MAPS;i++)
			{
				// add the counts if needed
				_rawh[i]->add_count(_bcid, _lut_hit[address_h][i] & _lut_live[address_l][i], _lut_live[address_l][i]);
				// reset the flag
				_check[i]=false;
			}
			
			// increase the timing
			_bcid++;
			_counter++;

			// check if at the end of the orbit
			if(_bcid == ORBIT_LENGTH)
			{			
				_end_orbit++;
				_bcid = 0;

				// check if we can publish
				if(_counter == _period*ORBIT_LENGTH)
				{
					// do the publishing
					_counter = 0;
					_readout_number++;

					for(std::uint32_t i=0; i<LUMI_ALGORITHM_MAPS; i++)
					{					
						// define the period
						_rawh[i]->define_period(0, _readout_number, _previous_end_orbit, _end_orbit);
						// get map from RawH and make a duplicate
						CatShell::CatPointer<CatShell::CatMemory> mem;
						mem = _pool_mem->get_element();
						mem->set_memory_capacity(RawH::get_memory_lenght());
						mem->set_memory_length(RawH::get_memory_lenght());
						memcpy(mem->get_memory(), _rawmem[i]->get_memory(), RawH::get_memory_lenght());
						// publish the data
						publish_data(_out_port_rawh+i, mem, logger);
					}

					_previous_end_orbit = _end_orbit;
				}
			}
		}
	}

	else if(port == _in_port_command && object.isSomething())
	{
		CatPointer<BocRegisterCommand> brc = object.unsafeCast<BocRegisterCommand>();
		BmfRegisterBank bank = brc->get_bank();
		if(brc->get_bank() == BmfRegisterBank::Dbm)
		{
			//get mask
			if(brc->get_register() == 7) _mask = (uint16_t)brc->getData() << 8;
			if(brc->get_register() == 6) _mask += (uint16_t)brc->getData();

			//get period
			if(brc->get_register() == 3) _period = (uint16_t)brc->getData() << 8;
			if(brc->get_register() == 2) _period += (uint16_t)brc->getData();
			
			//get address
			if(brc->get_register() == 5)
			{
				_address_l = (uint16_t)brc->getData(); 
				
				uint16_t h_l_s = _address_l & 128;
				if(h_l_s == 128) _hit_live_selector = true;
				if(h_l_s == 0) _hit_live_selector = false;
				
				_address_l = (_address_l & 127);
			}
			
			if(brc->get_register() == 4) _address = _address_l + (uint16_t)brc->getData();

			//get data
			if(brc->get_register() == 9) _data = (uint16_t)brc->getData() << 8;
			if(brc->get_register() == 8)
			{
				_data += (uint16_t)brc->getData();
				//set lut
				for(uint32_t i=0; i<LUMI_ALGORITHM_MAPS; i++)
				{
					if((_mask >> i) & 1 == 1)
					{
						if(_hit_live_selector == true) _lut_hit[_address][LUMI_ALGORITHM_MAPS-1-i] = (_data >> i) & 1;
						if(_hit_live_selector == false) _lut_live[_address][LUMI_ALGORITHM_MAPS-1-i] = (_data >> i) & 1;
					}
				}
			}
		}
	}

	Algorithm::process_data(port, object, flush, logger);
}

void DbmSimBoc::algorithm_init()
{
	Algorithm::algorithm_init();

	_in_port_hitor = declare_input_port("in_hitor", CAT_NAME_CATBOOL, LUMI_ALGORITHM_INPUTS);
	_out_port_rawh = declare_output_port("out_mempage", CAT_NAME_CATMEMPAGE, LUMI_ALGORITHM_MAPS);
	_in_port_command = declare_input_port("commands", CAT_NAME_BOCREGISTERCOMMAND);

	_pool_rawh = PluginDeamon::get_pool<RawH>();
	_pool_mem = PluginDeamon::get_pool<CatMemory>();

	CatPointer<CatMemory> mem;
	for(std::uint32_t i=0; i<LUMI_ALGORITHM_MAPS; i++)
	{
		// define the output raw map
		_rawh[i] = _pool_rawh->get_element();
		_rawmem[i] = _pool_mem->get_element();
		_rawmem[i]->set_memory_capacity(RawH::get_memory_lenght());
		_rawmem[i]->set_memory_length(RawH::get_memory_lenght());
		_rawh[i]->set_content(_rawmem[i]);
		_rawh[i]->define_header(false, BocFpga::North, BocStream::stream_0, i);
	
		_check[i] = false;
	}

	// reset the current timing
	_bcid = 0;
	_counter = 0;
	_readout_number = 0;
	_end_orbit = 0;
	_previous_end_orbit = 0;

	// settings
	_period = algo_settings().get_setting<std::uint32_t>("period", 1);

	// init the look-up tables
	for(uint32_t i=0; i<LUMI_ALGORITHM_COMBINATIONS; ++i)
	{
		for(uint32_t j=0; j<LUMI_ALGORITHM_MAPS; ++j)
		{
			_lut_hit[i][j] = false;
			_lut_live[i][j] = false;
		}
	}
//	// WARNING: testing setup: independent single input maps
//	for(uint32_t j=0; j<LUMI_ALGORITHM_MAPS; ++j)
//	{
//		_lut_hit[(1<<j)][j] = true;
//		_lut_live[(1<<j)][j] = true;
//	}
}
void DbmSimBoc::algorithm_deinit()
{
	// clear all RawH maps
	for(std::uint32_t i=0; i<LUMI_ALGORITHM_MAPS; i++)
	{
		// define the output raw map
		_rawh[i].make_nullptr();
		_rawmem[i].make_nullptr();
	}	
	//
	Algorithm::algorithm_deinit();
}
	
} // namespace DbmShell

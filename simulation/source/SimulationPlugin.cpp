#include "CatShell/core/Plugin.h"
#include "DbmShell/simulation/DbmSimTrackGen.h"
#include "DbmShell/simulation/DbmSimTrack.h"
#include "DbmShell/simulation/DbmSimFe.h"
#include "DbmShell/simulation/DbmSimBoc.h"

using namespace std;

namespace DbmShell{

EXPORT_PLUGIN(CatShell::Plugin,DbmSimulationPlugin)
START_EXPORT_CLASSES(DbmSimulationPlugin)
EXPORT_CLASS(DbmSimTrackGen, CAT_NAME_DBMSIMTRACKGEN, DbmSimulationPlugin)
EXPORT_CLASS(DbmSimTrack, CAT_NAME_DBMSIMTRACK, DbmSimulationPlugin)
EXPORT_CLASS(DbmSimFe, CAT_NAME_DBMSIMFE, DbmSimulationPlugin)
EXPORT_CLASS(DbmSimBoc, CAT_NAME_DBMSIMBOC, DbmSimulationPlugin)
END_EXPORT_CLASSES(DbmSimulationPlugin)

} // namespace DbmShell
